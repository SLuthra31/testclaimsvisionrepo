package utilities;

public class Configuration {
	
	
//public static String baseUrl="http://cvqa.pcisvision.com";
	public static String baseUrl="http://192.168.81.102:666";

	static String userPath="C:\\Project-Claimsvision\\Claimsvision-Project\\";


	//Driver Locations
	static String chromeDriverLocation = userPath+"drivers\\chromedriver.exe";
			
    static String ieDriverLocation = userPath+"drivers\\IEDriverServer.exe";
		
    public static final String reportPath=userPath+"report\\report.html";

	// fail screen locations
	public static final String failSceenLocation = userPath+"failScreenLocation\\";

	// path for TestData.xls file
	public static final String testDataFileLocation = userPath+"testDataLocation\\testData.xlsx";
}
