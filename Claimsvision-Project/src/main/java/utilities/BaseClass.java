package utilities;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class BaseClass {

	WebDriver driver;

	// base class constructor to initialize the WebDriver instance variable
	public BaseClass(WebDriver driver) {
		this.driver = driver;
	}

	// Description : this method used to clear the cookies of browser
	public void ClearBrowserCache() {
		driver.manage().deleteAllCookies();

	}

	// Description: This method used to scroll down the whole web page

	public void BrowserScrollDown() {
		final JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");
	}

	// Description: This method used to scrollUp the whole web page

	public void BrowserScrollup() {
		final JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, -250);");
	}

	// Description: This method return by default selected value of drop-down
	// Parameter : Locator(Variable of objectRepository.property file that
	// Contain webPage element locator i.e xpath, css selector etc)
	public String getSelectedValueOfDropDown(WebElement locator) {

		Select select = null;
		select = new Select(locator);
		return select.getFirstSelectedOption().getText();

	}

	// Description: This select options of drop down by the name of option
	// Parameter1 : Locator(Variable of objectRepository.property file that
	// Contain webPage element locator i.e xpath, css selector etc)
	// Parameter2 : Option name that we want to get select
	public void selectByVisibleText(WebElement locator, String visibleText) throws IOException, InterruptedException {
		Select select = null;
		select = new Select(locator);
		select.selectByVisibleText(visibleText);
	}
	// Description: This select options of drop down by the name of option

	// Description: This select options of drop down by the index of opetion
	// Parameter1 : Locator(Variable of objectRepository.property file that
	// Contain webPage element locator i.e xpath, css selector etc)
	// Parameter2 : index of option that we want to get select
	public void selectByIndex(WebElement locator, int index) throws IOException, InterruptedException {
		Select select = null;
		select = new Select(locator);
		select.selectByIndex(index);
	}

	// Description: This method used to switch to the alert and and it return
	// the alert type object
	public Alert switchToAlert() {
		final Alert alert = driver.switchTo().alert();

		return alert;
	}

	// Description: This method used to select check box
	// Parameter1 : Locator(Variable of objectRepository.property file that
	// Contain webPage element locator i.e xpath, css selector etc)

	public void select(WebElement locator) {

		if (!locator.isSelected()) {
			locator.click();
		}
	}

	// Description: This method used to deSelect check box
	// Parameter1 : Locator(Variable of objectRepository.property file that
	// Contain webPage element locator i.e xpath, css selector etc)

	public void deSelect(WebElement locator) {

		if (locator.isSelected()) {
			locator.click();
		}
	}

	// Description: This method used to clear the input text box field
	// Parameter1 : Locator(Variable of objectRepository.property file that
	// Contain webPage element locator i.e xpath, css selector etc)

	public void clearField(WebElement locator) {

		locator.clear();

	}

	// Description: This method used to get the attribute value of webElement
	// Parameter1 : Attribute (type of attribute that we want to get i.e.
	// 'value' , 'href' etc)
	// Parameter2 : Locator(Variable of objectRepository.property file that
	// contatin webPage element locator i.e xpath, css selector etc)

	public String getAttribute(String attribute, WebElement locator) {

		String text = null;

		text = locator.getAttribute(attribute);

		return text;
	}

	// Description: This method used to switch to the next tab
	public void switchControlToTab() {

		for (final String window : driver.getWindowHandles()) {

			driver.switchTo().window(window);

		}

	}

	// Description: It return the set of window handles
	public Set<String> getWinowshandles() {
		final Set<String> temp = new HashSet<String>();

		final Set<String> windows = driver.getWindowHandles();

		for (final String window : windows) {

			temp.add(window);

		}

		return temp;
	}

	// switching between windows
	/*
	 * public String switchwindow(String object, String data){ try {
	 *
	 * String winHandleBefore = driver.getWindowHandle();
	 *
	 * for(String winHandle : driver.getWindowHandles()){
	 * driver.switchTo().window(winHandle); } }catch(Exception e){ return
	 * Constants.KEYWORD_FAIL+ "Unable to Switch Window" + e.getMessage(); }
	 * return Constants.KEYWORD_PASS; }
	 *
	 * public String switchwindowback(String object, String data){ try { String
	 * winHandleBefore = driver.getWindowHandle(); driver.close(); //Switch back
	 * to original browser (first window)
	 * driver.switchTo().window(winHandleBefore); //continue with original
	 * browser (first window) }catch(Exception e){ return
	 * Constants.KEYWORD_FAIL+ "Unable to Switch to main window" +
	 * e.getMessage(); } return Constants.KEYWORD_PASS; }
	 */

	public void switchWindow() {

		final Set<String> allWindows = driver.getWindowHandles();
		for (final String curWindow : allWindows) {
			driver.switchTo().window(curWindow);
		}

	}

	// Description:It move the focus from one element to another element
	// automatically

	public void moveToElement(WebElement locator) {

		final Actions action = new Actions(driver);

		action.moveToElement(locator).build().perform();

	}

	// Description: to fetch value from input field
	public String getValueFromInputField(WebElement locator) {

		String temp = null;
		temp = locator.getAttribute("value");
		return temp;
	}

	// Description: to check field is disabled or not
	public boolean checkFieldIsDisabled(WebElement locator) {

		Boolean temp = false;
		temp = locator.isEnabled();
		return temp;
	}

	// Description: to fetch all options from drop down
	public int getCountOfAllOptionsFromDropDowm(WebElement locator) {
		Select select = null;
		select = new Select(locator);
		final List<WebElement> countofoptions = select.getOptions();
		return countofoptions.size();
	}

	// Description : use to check element in the list
	// Parameter1 : value, value that need to verify in the list
	// Parameter2 : list, list in which value need to verify

	public boolean isListContain(String value, List<String> list) {

		boolean ispresent = true;
		if (list.isEmpty()) {

			ispresent = false;

		}

		else {
			for (String ele : list) {
				ele = ele.toLowerCase();

				if (!ele.contains(value)) {
					ispresent = false;

				}

			}
		}
		return ispresent;
	}

	// Description : To get all options from the drop down
	public List<WebElement> getAllOptionsFromDropDown(WebElement locator) {
		Select select = null;
		select = new Select(locator);
		return select.getOptions();

	}

	// Description : To select date from calendar widget
	public String selectDateFromCalender(WebElement locator, String Date) throws IOException {
		final int datetemp = 0;
		final WebElement dateWidget = locator;

		final List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));
		for (final WebElement row : rows) {
			// List<WebElement>
			// columns=dateWidget.findElements(By.tagName("td"));
			final List<WebElement> columns = row.findElements(By.tagName("td"));
			for (final WebElement cell : columns) {

				if (cell.getText().equals(Date)) {

					cell.click();
					break;
				}

			}

			if (datetemp == 1) {
				break;
			}
		}
		final String dateTempString = Integer.toString(datetemp);
		return dateTempString;
	}

	// Description : To click
	public void click(WebElement locator) {
		locator.click();
	}

	// Description : To select first value from lookup
	public void selectValueFromLookup(WebElement search, WebElement firstElement) {

		click(search);
		click(firstElement);
	}

	public static int clickRandomWebElement(WebElement w1, WebElement w2) {
		final ArrayList<WebElement> webElements = new ArrayList<WebElement>();
		webElements.add(w1);
		webElements.add(w2);
		final Random random = new Random();
		final int count = random.nextInt(2);
		while (!webElements.get(count).isSelected()) {
			webElements.get(count).click();

		}
		return count;
	}

	public static String generateEffectiveDate() {

		final Random random = new Random();
		final String month = Integer.toString(random.nextInt(12 - 1) + 1);
		final String day = Integer.toString(random.nextInt(30 - 10) + 10);
		final String year = Integer.toString(random.nextInt(2017 - 1900) + 1900);
		final String date = month + "/" + day + "/" + year;

		return date;
	}

	public static String generateDateOfBirth() {

		final Random random = new Random();
		final String month = Integer.toString(random.nextInt(12 - 1) + 1);
		final String day = Integer.toString(random.nextInt(30 - 10) + 10);
		final String year = Integer.toString(random.nextInt(2017 - 1950) + 1950);
		final String date = month + "/" + day + "/" + year;

		return date;
	}

	@SuppressWarnings("deprecation")
	public static String generateExpirationDate(String date) throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		final Date randomDate = sdf.parse(date);
		final String oneMoreDate = date;
		final Date randomDate1 = sdf.parse(oneMoreDate);
		randomDate1.setMonth(randomDate.getMonth() + 12);
		return sdf.format(randomDate1);
	}

	@SuppressWarnings("deprecation")
	public static String generateExpDateLessThanEffDate(String date) throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		final Date randomDate = sdf.parse(date);
		final String oneMoreDate = date;
		final Date randomDate1 = sdf.parse(oneMoreDate);
		randomDate1.setMonth(randomDate.getMonth() - 12);
		return sdf.format(randomDate1);
	}

	public static String generateDateMoreThanSpecificDate(String date) throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		final Date randomDate = sdf.parse(date);
		final String oneMoreDate = date;
		final Date randomDate1 = sdf.parse(oneMoreDate);
		randomDate1.setYear(randomDate.getYear()+ 1);
		randomDate1.setMonth(randomDate.getMonth() + BaseClass.generateRandomNumberAsInteger(13));
		return sdf.format(randomDate1);
	}

	public static String generateDateLessThanSpecificDate(String date) throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		final Date randomDate = sdf.parse(date);
		final String oneMoreDate = date;
		final Date randomDate1 = sdf.parse(oneMoreDate);
		randomDate1.setMonth(randomDate.getMonth() - BaseClass.generateRandomNumberAsInteger(13));
		randomDate1.setYear(randomDate.getYear()-BaseClass.generateRandomNumberAsInteger(10));
		randomDate1.setDate(BaseClass.generateRandomNumberAsInteger(31));
		return sdf.format(randomDate1);
	}

	public void switchToNextWindow(String window) {
		final Set<String> windows = driver.getWindowHandles();

		final Iterator<String> iterate = windows.iterator();
		while (iterate.hasNext()) {
			final String childWindow = iterate.next();

			if (!window.equalsIgnoreCase(driver.getTitle().trim())) {
				driver.switchTo().window(childWindow);

			}

			else {
				break;
			}
		}

	}

	public static HashMap<String, String> hashmap = new HashMap<String, String>();

	public static void saveValuesWithKeys(String key, String value) {

		hashmap.put(key, value);
	}

	public static String getValuesWithKeys(String key) {
		final String keyValue = hashmap.get(key);
		return keyValue;
	}

	public static String enterRandomNumber(int count) {
		final String temp = RandomStringUtils.randomNumeric(count);
		return temp;

	}

	public static String stringGeneratorl(int size) {
		final String temp = RandomStringUtils.randomAlphabetic(size).toUpperCase();

		return temp;
	}

	public static int generateRandomNumberAsInteger(int size) {

		int count = 0;
		for (int i = 0; i < 70; i++) {
			final Random random = new Random();
			count = random.nextInt(size - 1) + 1;
			if (count == 0) {
				continue;
			} else {
				break;
			}

		}
		return count;
	}

	public static String generatePolicyYear() {
		final Random random = new Random();
		final String year = Integer.toString(random.nextInt(2017 - 2000) + 2000);
		return year;

	}

	public String getCurrentDate() {
		final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		final Date date = new Date();
		return dateFormat.format(date);
	}
}

