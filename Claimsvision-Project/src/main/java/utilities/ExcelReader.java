package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {

 private static String ExcelFileLocation = Configuration.testDataFileLocation;

 // Description: used to get the value of cell
 // Parameter1: sheetNo.(Sheet no from which value need to fetch)
 // Parameter2: rows row number of the table
 // Parameter3: column number of the table

 public static  String getCell(int sheetno, int row, int coloumn) throws IOException {
  int i = row;
  
  String value = null;
  File file = new File(ExcelFileLocation);

  FileInputStream filestream = new FileInputStream(file);
  XSSFWorkbook workbook = new XSSFWorkbook(filestream);
  XSSFSheet sheet = workbook.getSheetAt(sheetno);

  try {
   XSSFCell cell = sheet.getRow(i).getCell(coloumn);
    if (cell.getCellType() == Cell.CELL_TYPE_STRING)
    {
        value=cell.getStringCellValue().toString();
    }
       else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {

       value= Double.toString(cell.getNumericCellValue()).toString();
       }
   

  } catch (Exception e) {
   e.printStackTrace();
  }

  workbook.close();
  return value;
 }

 public static void setCell(int sheetno, int row, int column,String str) throws IOException
 {
 
  int i = row; 
  File file=new File(ExcelFileLocation);
   
    // Load the file
   
    FileInputStream filestream=new FileInputStream(file);
   
     // load the workbook
   
     XSSFWorkbook workbook=new XSSFWorkbook(filestream);
   
    // get the sheet which you want to modify or create
   
     XSSFSheet sheet= workbook.getSheetAt(sheetno);
   
   
  // here createCell will create column
   
  // and setCellvalue will set the value
     
     try {
    	 
    	 
   sheet.getRow(i).createCell(column).setCellValue(str);
    

   } catch (Exception e) {
    e.printStackTrace();
   }
   
   
   
  // here we need to specify where you want to save file
   
   FileOutputStream fout=new FileOutputStream(new File(ExcelFileLocation));
   
   
  // finally write content 
   
   workbook.write(fout);
   
  // close the file
   
   fout.close();
   
  
   
    }
 
 public static void clearCell() throws IOException{
  
	
	  File file=new File(ExcelFileLocation);
	   
	    // Load the file
	   
	    FileInputStream filestream=new FileInputStream(file);
	   
	     // load the workbook
	   
	     XSSFWorkbook workbook=new XSSFWorkbook(filestream);
	   
	    // get the sheet which you want to modify or create
	   
	     XSSFSheet sheet= workbook.getSheetAt(1);
	     
	  
	   
	   
	  // here createCell will create column
	   
	  // and setCellvalue will set the value
	     
	     try {
	    	   XSSFRow cell=sheet.getRow(9);
	    	 for (int column=0;column<96;column++)
	    	 {
	    		 if (cell.createCell(column).getCellType() != Cell.CELL_TYPE_BLANK) {
	    			 cell.createCell(column).setCellValue("");
	    			} 
	    	 }
	
	    

	   } catch (Exception e) {
	    e.printStackTrace();
	   }
	   
	   
	   
	  // here we need to specify where you want to save file
	   
	   FileOutputStream fout=new FileOutputStream(new File(ExcelFileLocation));
	   
	   
	  // finally write content 
	   
	   workbook.write(fout);
	   
	  // close the file
	   
	   fout.close();
	   
	  
	   
 }
 }