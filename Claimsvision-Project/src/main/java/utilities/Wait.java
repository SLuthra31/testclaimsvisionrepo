package utilities;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {

	public static WebDriverWait webDriverWait;

	
	public static void modifyWait( WebDriver driver, WebElement element) throws InterruptedException 
	{
		int i=0;
		boolean temp =false;
		while(temp ==false)
		{	
			try{
				if(i>=10)
				{
					temp=true;
					
				}
				i++;
				webDriverWait = new WebDriverWait(driver, 10);

				webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
				temp=true;
				
			}
			
			catch(Exception e)
			{
				
				temp=false;
				Thread.sleep(1000);
			}
			
		}
	}
	public static void waitTillPresent( WebDriver driver, WebElement element) throws InterruptedException 
	{
		int i=0;
		boolean temp =false;
		while(temp ==false)
		{	
			try{
				if(i>=10)
				{
					temp=true;
					
				}
				i++;
				webDriverWait = new WebDriverWait(driver, 10);

				webDriverWait.until(ExpectedConditions.visibilityOf(element));
				temp=true;
				
			}
			
			catch(Exception e)
			{
				
				temp=false;
				Thread.sleep(1000);
			}
			
		}
	}
		
	public static void waitFor(int time)
	{
		try {
				Thread.sleep(time * 1000);
			}
		catch (Exception e) 
			{
				System.out.println("Exception in thread sleep");
			}
	}
	
	public static void implicitWait(WebDriver driver, int wait) 
	{
		driver.manage().timeouts().implicitlyWait(wait, TimeUnit.SECONDS);
	}
		
}

