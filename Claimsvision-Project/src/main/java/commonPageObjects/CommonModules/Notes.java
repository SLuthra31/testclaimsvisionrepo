package commonPageObjects.CommonModules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import utilities.BaseClass;
import utilities.Wait;

public class Notes {

	WebDriver driver;

	public Notes(WebDriver driver) {
		this.driver = driver;
	}
	
	@FindBy(css = "a#claimsTreeViewt6")
	public WebElement compNotes;

	@FindBy(css = "a#claimsTreeViewt5")
	public WebElement nonCompNotes;

	@FindBy(css="select#Notes_categoryDropDown")
	public WebElement category;

	@FindBy(css = "select#Notes_subCategoryDropDown")
	public WebElement subCategory;

	@FindBy(css="input#Notes_subjectNewTextBox")
	public WebElement subject;

	@FindBy(css="textarea#Notes_addAttachmentTextBox")
	public WebElement notesText;

	@FindBy(css="input#Notes_saveAsDraftButton")
	public WebElement saveAsDraft;

	@FindBy(css="input#Notes_saveButton")
	public WebElement save;

	@FindBy(css="input#Notes_ScreenButton")
	public WebElement expand;

	@FindBy(css="input#Notes_newButton")
	public WebElement newNotes;

	@FindBy(css="input#Notes_deleteDraftButton")
	public WebElement deleteDraft;
	
	@FindBy(css = "span#Notes_saveMessageLabel")
	public WebElement validationMessage;
	
	public void clickOnNotesWC(){
		compNotes.click();
	}
	
	public void clickOnNotesNC(){
		nonCompNotes.click();
	}
	
	public void selectCategory(){
		Select s = new Select(category);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectSubCategory(){
		Select s = new Select(subCategory);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void enterSubject(){
		subject.clear();
		subject.sendKeys(BaseClass.stringGeneratorl(15));
	}
	
	public void enterNotesText(){
		notesText.clear();
		notesText.sendKeys(BaseClass.stringGeneratorl(5)+BaseClass.stringGeneratorl(4));
	}
	
	public void clickOnSaveAsDraft(){
		saveAsDraft.click();
	}
	
	public void clickOnSave(){
		save.click();
	}
	
	public void clickOnExpand(){
		expand.click();
	}
	
	public void clickOnDeleteDraft(){
		deleteDraft.click();
	}
}
