package commonPageObjects.Login;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import commonPageObjects.NonComp.Claims.Claims;
import utilities.Wait;

public class SearchClaim {

	WebDriver driver;
	Dashboard dashboardPageObjects;
	public SearchClaim(WebDriver driver) {
		this.driver = driver;

	}

	/*
	 * WebElements of Buttons to perform action on Non Comp Policy Screen
	 */
	/*@FindBy(css = "input#claimSearchImageButton")
	private WebElement searchClaim;*/
	// WebElement of Validation Message Area
	@FindBy(css = "input#ClaimSearch_searchButton")
	private WebElement searchClaimButton;
	
	@FindBy(css="table#ClaimSearch_CustomizationGrid1_CustomGridView")
	private WebElement claimSearchGrid;
	
	@FindBy(css="input[id*='claimNumberTextBox']")
	public WebElement claimNumber;
	
	@FindBy(css="input:contains('_claimNumber_')")
	public WebElement claim;
	
	@FindBy(css="input#claims25ImageButton")
	public WebElement claims_25;
	
	@FindBy(css="ul#claims25BulletedList")
	public WebElement claimSearchList;
	
	@FindBy(css="select#ClaimSearch_statusDropDown")
	public WebElement claimSearch;
	

	

	
	public String clickAndGetClaimNumberThrough25Claims()
	{
		claims_25.click();
		driver.switchTo().frame("dropDownIframe");
		List<WebElement> claims=claimSearchList.findElements(By.tagName("li"));
		WebElement selectedClaim=claims.get(0);
		
		String claim= selectedClaim.getText();
		Wait.waitFor(3);
		selectedClaim.click();
		
		return claim;
		
	}
	

	public String clickAndGetClaimNumber()
	{
		searchClaimButton.click();
		Wait.waitFor(5);
		List<WebElement> claims=claimSearchGrid.findElements(By.tagName("tr"));
		WebElement selectClaimRow=claims.get(5);
		
		
		List<WebElement> selectClaimLink=selectClaimRow.findElements(By.tagName("td"));
		selectClaimLink.get(1).click();
		String selectedClaim=selectClaimLink.get(1).getText();
		return selectedClaim;
		
	}
	
	public void enterClaimNumber(String claim) throws InterruptedException{
		Wait.modifyWait(driver, claimNumber);
		claimNumber.sendKeys(claim);
	}
	
	public Claims selectClaimNumber() throws InterruptedException
	{
		Wait.modifyWait(driver, claimSearchGrid);
		List<WebElement> claims=claimSearchGrid.findElements(By.tagName("tr"));
		WebElement selectClaimRow=claims.get(1);
		List<WebElement> selectClaimLink=selectClaimRow.findElements(By.tagName("td"));
		selectClaimLink.get(1).click();
		return  PageFactory.initElements(driver, Claims.class);
	
	}
	
	public void selectSearch() throws InterruptedException{
		Wait.modifyWait(driver, searchClaimButton);
		searchClaimButton.click();
	}
	
	public void selectClaimStatus(int i) throws InterruptedException{
		Wait.modifyWait(driver, claimSearch);
		Select s = new Select(claimSearch);
		s.selectByIndex(i);
	}
	
	
	
	public void selectClaim() throws InterruptedException{
	Wait.modifyWait(driver, claimSearchGrid);
	List<WebElement> rows= claimSearchGrid.findElements(By.tagName("tr"));
	for(WebElement row : rows)
	{
		List<WebElement> columns= row.findElements(By.tagName("td"));
		if(!columns.isEmpty())
		{
		if(!columns.get(2).getText().trim().isEmpty())
		{
	    columns.get(1).click();
	    break;
	}
		else{
			continue;
		}
		}
		
		else{
			continue;
		}
		}
	
	}
	
}
