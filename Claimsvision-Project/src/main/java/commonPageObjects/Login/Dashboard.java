package commonPageObjects.Login;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import commonPageObjects.NonComp.IRF.IRF_ENS;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import commonPageObjects.WorkersComp.iRF.IRF;
import utilities.BaseClass;
import utilities.Wait;

public class Dashboard {

	WebDriver driver;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;
	BaseClass baseclass;
	String windowForDashboard;
	

	public Dashboard(WebDriver driver) {
		this.driver = driver;
		baseclass = new BaseClass(driver);
	}

	// Web Elements of User Activity
	@FindBy(css = "div#menuTitleHome")
	public WebElement userActivity;

	@FindBy(css = "input#btnAgree")
	public WebElement agree;

	// Web Elements of Fin Approval Hub
	@FindBy(css = "div#Div1")
	public WebElement finApprovalHub;

	// Web Elements of Diary
	@FindBy(css = "div#menuTitleDiary")
	public WebElement diary;

	// Web Elements of IRF
	@FindBy(css = "div#menuTitleIRF.menuTitle")
	public WebElement irf;

	// Web Elements of Claims menu
	@FindBy(css = "img#claimsImage")
	public WebElement claimsArrow;

	// Web Elements of Claim link
	@FindBy(css = "a#claimsTreeViewt0")
	public WebElement claim;

	// Web Elements of Claimant link
	@FindBy(css = "a#claimsTreeViewt1")
	public WebElement claimant;

	// Web Elements of Sub Claim link
	@FindBy(css = "a#claimsTreeViewt2")
	public WebElement subClaim;

	// Web Elements of Client/State Specific link
	@FindBy(css = "a#claimsTreeViewt3")
	public WebElement clientStateSpecific;

	// Web Elements of Financial Summary link
	@FindBy(css = "a#claimsTreeViewt4")
	public WebElement financialSummary;

	// Web Elements of Notes link
	@FindBy(css = "a#claimsTreeViewt5")
	public WebElement notes;

	// Web Elements of Reserves link
	@FindBy(css = "a#claimsTreeViewt6")
	public WebElement reserves;

	// Web Elements of Payments link
	@FindBy(css = "a#claimsTreeViewt7")
	public WebElement payments;

	// Web Elements of Litigation link
	@FindBy(css = "a#claimsTreeViewn8")
	public WebElement litigation;

	// Web Elements of Claim Authority Limits link
	@FindBy(css = "a#claimsTreeViewt10")
	public WebElement claimAuthorityLimit;

	// Web Elements of Finance menu
	@FindBy(css = "img#financialImage")
	public WebElement financeArrow;

	// Web Elements of TransactionSearch link
	@FindBy(css = "a#financialTreeViewt1")
	public WebElement transactionSearch;

	// Web Elements of PaymentTransactionSearch link
	@FindBy(css = "a#financialTreeViewt2")
	public WebElement paymentTransactionSearch;

	// Web Elements of CheckHistory link
	@FindBy(css = "a#financialTreeViewt3")
	public WebElement checkHistory;

	// Web Elements of Recoveries link
	@FindBy(css = "a#financialTreeViewt4")
	public WebElement recoveries;

	// Web Elements of RecipientOfBenefit link
	@FindBy(css = "a#financialTreeViewt5")
	public WebElement recipientOfBenefit;

	// Web Elements of CheckAdmin link
	@FindBy(css = "a#financialTreeViewn6")
	public WebElement checkAdmin;

	// Web Elements of ReleasePayments link
	@FindBy(css = "a#financialTreeViewt7")
	public WebElement releasePayment;

	// Web Elements of SelectPayment link
	@FindBy(css = "a#financialTreeViewt8")
	public WebElement selectPaymentForCheck;

	// Web Elements of CheckWriting link
	@FindBy(css = "a#financialTreeViewt9")
	public WebElement checkWriting;

	// Web Elements of PostedCheckReversal link
	@FindBy(css = "a#financialTreeViewt10")
	public WebElement postedCheckReversal;

	// Web Elements of Correspondence menu
	@FindBy(css = "img#formsImage")
	public WebElement correspondenceArrow;

	// Web Elements of AttachCorrespondence link
	@FindBy(css = "a#formsTreeViewt0")
	public WebElement attachCorrespondence;

	// Web Elements of CorrespondenceHistory link
	@FindBy(css = "a#formsTreeViewt1")
	public WebElement correspondenceHistory;

	// Web Elements of PrintCorrespondence link
	@FindBy(css = "a#formsTreeViewt2")
	public WebElement printCorrespondence;

	// Web Elements of Admin link
	@FindBy(css = "#adminImage")
	public WebElement adminArrow;

	// Web Elements of AccountClient link
	@FindBy(css = "a#adminTreeViewn0")
	public WebElement accountAndClient;

	// Web Elements of CompanyProfileSet link
	@FindBy(css = "a#adminTreeViewt1")
	public WebElement companyProfileSet;

	// Web Elements of CompanyHierarchy link
	@FindBy(css = "a#adminTreeViewt2")
	public WebElement companyHierarchy;

	// Web Elements of BankingInformation link
	@FindBy(css = "a#adminTreeViewn3")
	public WebElement bankingInformation;

	// Web Elements of Bank link
	@FindBy(css = "a#adminTreeViewt4")
	public WebElement bank;

	// Web Elements of BankAccount link
	@FindBy(css = "a#adminTreeViewt5")
	public WebElement bankAccount;

	// Web Elements of PaymentMode link
	@FindBy(css = "a#adminTreeViewt6")
	public WebElement paymentMode;

	// Web Elements of Permission link
	@FindBy(css = "a#adminTreeViewn7")
	public WebElement permission;

	// Web Elements of GroupSetup link
	@FindBy(css = "a#adminTreeViewt8")
	public WebElement groupSetUp;

	// Web Elements of UserSetup link
	@FindBy(css = "a#adminTreeViewt9")
	public WebElement userSetUp;

	// Web Elements of GroupUserSetup link
	@FindBy(css = "a#adminTreeViewt10")
	public WebElement groupUserSetup;

	// Web Elements of FieldLevelSecurity link
	@FindBy(css = "a#adminTreeViewt11")
	public WebElement fieldLevelSecurity;

	// Web Elements of AuditInformation link
	@FindBy(css = "a#adminTreeViewt12")
	public WebElement auditInformation;

	// Web Elements of DocumentService link
	@FindBy(css = "a#adminTreeViewt13")
	public WebElement documentService;

	// Web Elements of UserPolicySetup link
	@FindBy(css = "a#adminTreeViewt14")
	public WebElement userPolicySetUp;

	// Web Elements of AuthorizedApproverSetup link
	@FindBy(css = "a#adminTreeViewt15")
	public WebElement authorizedApproverSetup;

	// Web Elements of Customization link
	@FindBy(css = "a#adminTreeViewn16")
	public WebElement customization;

	// Web Elements of RequiredFieldSetup link
	@FindBy(css = "a#adminTreeViewt17")
	public WebElement requiredFieldSetup;

	// Web Elements of SpecialAnalysisField link
	@FindBy(css = "a#adminTreeViewt18")
	public WebElement specialAnalysisField;

	// Web Elements of StateSpecificFieldSetup link
	@FindBy(css = "a#adminTreeViewt19")
	public WebElement stateSpecificFielsSetUp;

	// Web Elements of SupportTable link
	@FindBy(css = "a#adminTreeViewt20")
	public WebElement supportTable;

	// Web Elements of PolicyType link
	@FindBy(css = "a#adminTreeViewt21")
	public WebElement policyType;

	// Web Elements of AggregateLimitDetail link
	@FindBy(css = "a#adminTreeViewt22")
	public WebElement aggregateLimitDetail;

	// Web Elements of CoverageLossType link
	@FindBy(css = "a#adminTreeViewt23")
	public WebElement coverageLossType;

	// Web Elements of Catastrophe link
	@FindBy(css = "a#adminTreeViewt24")
	public WebElement catastrophe;

	// Web Elements of MaintainPaymentCategory link
	@FindBy(css = "a#adminTreeViewt25")
	public WebElement paymentCategorySetup;

	// Web Elements of NonCompMaintainPaymentCategory link
	@FindBy(css = "a#adminTreeViewt26")
	public WebElement nonCompMaintainPaymentCategory;

	// Web Elements of ClassCode link
	@FindBy(css = "a#adminTreeViewt27")
	public WebElement classCode;

	// Web Elements of CustomizeItemField link
	@FindBy(css = "a#adminTreeViewt28")
	public WebElement customizeItemField;

	// Web Elements of PrivacyAuthorityLevel link
	@FindBy(css = "a#adminTreeViewt29")
	public WebElement privacyAuthorityLevel;

	// Web Elements of PrivacyReasonLevel link
	@FindBy(css = "a#adminTreeViewt30")
	public WebElement privacyReasonLevel;

	// Web Elements of ClaimFlagLevelSetup link
	@FindBy(css = "a#adminTreeViewt31")
	public WebElement claimFlagLevelSetup;

	// Web Elements of PolicyNumberAssignment link
	@FindBy(css = "a#adminTreeViewt31")
	public WebElement policyNumberAssignment;

	// Web Elements of Vendor link
	@FindBy(css = "a#adminTreeViewn33")
	public WebElement vendor;

	// Web Elements of VendorSetup link
	@FindBy(css = "a#adminTreeViewn34")
	public WebElement vendorSetup;

	// Web Elements of VendorMerge link
	@FindBy(css = "a#adminTreeViewn35")
	public WebElement vendorMerge;

	// Web Elements of DocumentManagement Link
	@FindBy(css = "a#adminTreeViewn36")
	public WebElement documentMgt;

	// Web Elements of CorrespondenceDefintion Link
	@FindBy(css = "a#adminTreeViewt37")
	public WebElement correspondenceDefinition;

	// Web Elements of CorrespondenceAssignment Link
	@FindBy(css = "a#adminTreeViewt38")
	public WebElement correspondenceAssignment;

	// Web Elements of AutomatedDiaryDefinition Link
	@FindBy(css = "a#adminTreeViewt39")
	public WebElement automatedDiaryDefinition;

	// Web Elements of ConditionalFormAttachment Link
	@FindBy(css = "a#adminTreeViewt40")
	public WebElement conditionalFormAttachment;

	// Web Elements of BusinessRuleVariables Link
	@FindBy(css = "a#adminTreeViewt41")
	public WebElement businessRuleVariables;

	// Web Elements of PolicySetup Link
	@FindBy(css = "a#adminTreeViewn41")
	public WebElement policySetUp;

	// Web Elements of WC Link
	@FindBy(css = "a#adminTreeViewn43")
	public WebElement wC;

	// Web Elements of WCPolicy Link
	@FindBy(css = "a#adminTreeViewt44")
	public WebElement wCPolicy;

	// Web Elements of WCOrgPolicy Link
	@FindBy(css = "a#adminTreeViewt45")
	public WebElement wCOrgPolicy;

	// Web Elements of PolicyCoverage Link
	@FindBy(css = "a#adminTreeViewt46")
	public WebElement wCPolicyCoverage;

	// Web Elements of PolicyClassCode Link
	@FindBy(css = "a#adminTreeViewt47")
	public WebElement wCPolicyClassCode;

	// Web Elements of NonComp Link
	@FindBy(css = "a#adminTreeViewn47")
	public WebElement nonComp;

	// Web Elements of NonCompPolicy Link
	@FindBy(css = "a#adminTreeViewt48")
	public WebElement nonCompPolicy;

	// Web Elements of NonCompOrgPolicy Link
	@FindBy(css = "a#adminTreeViewt50")
	public WebElement nonCompOrgPolicy;

	// Web Elements of NonCompPolicyItem Link
	@FindBy(css = "a#adminTreeViewt51")
	public WebElement nonCompPolicyItem;

	// Web Elements of NonCompPolicyItemCoverage Link
	@FindBy(css = "a#adminTreeViewt52")
	public WebElement nonCompPolicyItemCoverage;

	// Web Elements of NonCompPolicyView Link
	@FindBy(css = "a#adminTreeViewt53")
	public WebElement nonCompPolicyView;

	// Web Elements of State Link
	@FindBy(css = "a#adminTreeViewn54")
	public WebElement state;

	// Web Elements of BenefitGuidelines Link
	@FindBy(css = "a#adminTreeViewt55")
	public WebElement benefitGuidelines;

	// Web Elements of ReassignClaim Link
	@FindBy(css = "a#adminTreeViewt56")
	public WebElement ReassignClaim;

	@FindBy(css = "input#claimSearchImageButton")
	public WebElement searchClaim;

	@FindBy(css = "a#claimsTreeViewt7")
	public WebElement payment;

	/*
	 * Navigation Methods
	 */
	// Navigation method to navigate to Non Comp Policy Setup
	public PolicySetUp navigateToPolicySetup() throws Exception {
		Thread.sleep(2000);
		Wait.modifyWait(driver, adminArrow);
		adminArrow.click();
		policySetUp.click();
		nonComp.click();
		nonCompPolicy.click();
		System.out.println("Navigated to PolicySetup page");
		return PageFactory.initElements(driver, PolicySetUp.class);
	}

	// Navigation method to navigate to PolicyNumberAssignment Screen
	public PolicyNumberAssignment navigateToPolicyNumberAssignment() throws InterruptedException {

		Wait.modifyWait(driver, adminArrow);
		adminArrow.click();
		customization.click();
		Wait.modifyWait(driver, customization);
		policyNumberAssignment.click();
		return PageFactory.initElements(driver, PolicyNumberAssignment.class);
	}
	
	public void navigateToAggregateLimitDetail() throws InterruptedException
	{
		Wait.modifyWait(driver, adminArrow);
		customization.click();
		aggregateLimitDetail.click();
		
		
	}

	public IRF_ENS navigateToIRF() throws Exception {
		Thread.sleep(2000);
		String parent = driver.getWindowHandle();
		irf.click();

		Wait.waitFor(2);
		baseclass.switchToNextWindow("IRF");
		
		return PageFactory.initElements(driver, IRF_ENS.class);

	}

	public IRF navigateToIRFWC() throws Exception {
		Wait.modifyWait(driver, irf);

		irf.click();

		Wait.waitFor(2);

		baseclass.switchToNextWindow("IRF");

		return PageFactory.initElements(driver, IRF.class);

	}

	public void switchToParentWindow() {
		Set<String> window = driver.getWindowHandles();
		Iterator<String> iterate = window.iterator();
		String parent = iterate.next();
		driver.switchTo().window(parent);
	}
	
	public void getWindowForDashboard()
	{
		windowForDashboard=driver.getWindowHandle();
	}
	public void getBackToParentWindow()
	{
		driver.switchTo().window(windowForDashboard);
	}

	public SearchClaim navigateToClaimSearch() throws InterruptedException {
		Wait.waitFor(3);
		Wait.modifyWait(driver, searchClaim);
		searchClaim.click();

		return PageFactory.initElements(driver, SearchClaim.class);

	}

	public SearchClaim navigateToUserActivity() {
		Wait.waitFor(3);
		userActivity.click();
		System.out.println("Navigated to User Activity page");
		return PageFactory.initElements(driver, SearchClaim.class);
	}

	public void clickReserve() throws InterruptedException {
		Wait.modifyWait(driver, reserves);
		while(!"Claim Reserves For Non Comp".equals(driver.getTitle().trim())){
			reserves.click();
			Wait.waitFor(10);
		}
	
	}

	public void clickPayment() throws InterruptedException {
		Wait.modifyWait(driver, payment);
		payment.click();
	}

	public void selectAgree() throws InterruptedException {
		Wait.modifyWait(driver, agree);
		agree.click();

	}

}
