package commonPageObjects.Login;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.ExcelReader;

public class LoginPageObjects {

WebDriver driver;
	
	public LoginPageObjects(WebDriver driver)
	{
		this.driver=driver;
	}
	
	/*
	 * WebElements of Fields to be entered while Logging in
	 */
	@FindBy(css="input#customerCodeTextBox")
	public WebElement customercode; 
	
	@FindBy(css="input#userNameTextBox")
	public WebElement username;

	@FindBy(css="input#passwordTextBox")
	public WebElement password;

	/*
	 * WebElements of Login Button to be clicked
	 */
	@FindBy(css="input#loginImageButton")
	public WebElement login;

	/*
	 * WebElements of ValidationMessages labels if error occurred
	 */
	@FindBy(css="span#messageLabel")
	public WebElement MessageText;

	@FindBy(css="div#validationSummary")
	public WebElement ValidationMessage;

	/*
	 * Methods that will be used while writing Test Scripts.
	 */
	
	// This method will be used to enter Customer Code
	public void enterCustomerCode(String customercode)
	{
		this.customercode.sendKeys(customercode);
	
	}
	
	// This method will be used to enter User Name
	public void enterUserName(String username)
	{
		this.username.sendKeys(username);
	}
	
	// This method will be used to enter Password
	public void enterPassWord(String password)
	{
		this.password.sendKeys(password);
	}
	
	// This method will be used to click on Login button
	public void clickOnLogin()
	{
		login.click();
	}
	
	// This method will be used to test error message
	public String testErrorMessage()
	{
		String ss=this.MessageText.getText();
		return ss;
	}

	// This method will be used to test validation message
	public String testValidationMessage()
	{
		String tt=this.ValidationMessage.getText();
		return tt;
	}
	
	// This method will be used to navigate to Dashboard screen after Login
	public Dashboard Login(int row) throws IOException{
		enterCustomerCode(ExcelReader.getCell(0, row, 2));
		
		enterUserName(ExcelReader.getCell(0, row, 3));
		
		enterPassWord(ExcelReader.getCell(0, row, 4));
		
		clickOnLogin();
			return PageFactory.initElements(driver, Dashboard.class);
	}

}
