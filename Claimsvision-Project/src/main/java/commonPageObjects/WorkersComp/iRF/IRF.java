package commonPageObjects.WorkersComp.iRF;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import utilities.BaseClass;
import utilities.ExcelReader;
import utilities.Wait;

public class IRF {

	WebDriver driver;
	BaseClass baseclass;
	// ExcelReader excelReader;

	public IRF(WebDriver driver) {
		this.driver = driver;
		baseclass = new BaseClass(driver);
		// excelReader = new ExcelReader();

	}

	/*
	 * WebElements of fields on IRF - WC Screen
	 */
	// Incident Date
	@FindBy(css = "input#dateOfIncidentTextBox")
	private WebElement incidentDate;

	// TimeOfLoss-Hours
	@FindBy(css = "select#timeOfLossTimeControl_hourDropDown")
	public WebElement timeOfLoss_hour;

	// TimeOfLoss-Minutes
	@FindBy(css = "select#timeOfLossTimeControl_minuteDropDown")
	public WebElement timeOfLoss_min;

	// TimeOfLoss-Meridian
	@FindBy(css = "select#timeOfLossTimeControl_meridianDropDown")
	public WebElement timeOfLoss_meridian;

	// Client
	@FindBy(css = "input#clientTextBox")
	public WebElement client;

	// LevelAssign
	@FindBy(css = "input#locationTextBox")
	public WebElement location;


	// SearchClient
	@FindBy(css = "img#searchClientImageButton")
	public WebElement searchClient;

	// SearchButton
	@FindBy(css = "input#searchButton.defaultButton01")
	public WebElement searchButton;

	// SelectValue
	@FindBy(css = "#searchResultDataList_ctl01_TableCell1")
	public WebElement selectValue;

	// LocationAssignedTo
	@FindBy(css = "img#searchLocationImage")
	public WebElement searchLocation;

	// PolicyType
	@FindBy(css = "select#policyTypeDropDown")
	public WebElement policyType;

	// ClaimantID
	@FindBy(css = "input#socialSecurityTextBox")
	public WebElement claimantID;

	// ID Type
	@FindBy(css = "select#IdTypeDropDown")
	public WebElement idType;

	// SearchButton
	@FindBy(css = "input#claimantFirstNameTextBox")
	public WebElement firstName;

	// SelectValue
	@FindBy(css = "input#claimantMiddleNameTextBox")
	public WebElement middleName;

	// LocationAssignedTo
	@FindBy(css = "input#claimantLastNameTextBox")
	public WebElement lastName;

	// PolicyType
	@FindBy(css = "input#claimantSuffixTextBox")
	public WebElement suffix;

	// SearchClaimant
	@FindBy(css = "img#searchClaimantImage")
	public WebElement searchClaimant;

	// ClaimantID
	@FindBy(css = "input#employeeIdTextBox")
	public WebElement employeeID;

	// ID Type
	@FindBy(css = "input#accidentDescriptionTextBox")
	public WebElement incidentDesc;

	// AcquiredClaim
	@FindBy(css = "input#isAcquiredClaimCheckBox")
	public WebElement isAcquiredClaim;

	/*
	 * WebElements of buttons on IRF - WC Screen
	 */
	// SearchClaimant
	@FindBy(css = "input#cancel1Button")
	public WebElement cancelButton;

	// ClaimantID
	@FindBy(css = "input#save1Button")
	public WebElement saveButton;

	// ID Type
	@FindBy(css = "input#nextButton")
	public WebElement nextButton;

	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Claimant Information Section:
	 */
	// Gender
	@FindBy(css = "input#genderRadioButtonList_0")
	public WebElement genderMale;

	@FindBy(css = "input#genderRadioButtonList_1")
	public WebElement genderFemale;

	// dateOfBirth
	@FindBy(css = "input#dateOfBirthTextBox")
	public WebElement dateOfBirth;

	// ageOnIncidentDate
	@FindBy(css = "input#ageOnInjuryDateTextBox")
	public WebElement ageOnIncidentDate;

	// isForeignAddress
	@FindBy(css = "input#foreignAddressCheckBox")
	public WebElement isForeignAddress;

	// claimantAddress1
	@FindBy(css = "input#claimantAddress1TextBox")
	public WebElement claimantAddress1;

	// claimantAddress2
	@FindBy(css = "input#claimantAddress2TextBox")
	public WebElement claimantAddress2;

	// claimantCity
	@FindBy(css = "input#cityTextBox")
	public WebElement claimantCity;

	// claimantState
	@FindBy(css = "select#stateDropDown")
	public WebElement claimantState;

	// claimantPostal
	@FindBy(css = "input#postalTextBox")
	public WebElement claimantPostal;

	// claimantCounty
	@FindBy(css = "input#countyTextBox")
	public WebElement claimantCounty;

	// homePhone
	@FindBy(css = "input#homePhoneTextBox")
	public WebElement homePhone;

	// cellPhone
	@FindBy(css = "input#cellPhoneTextBox")
	public WebElement cellPhone;

	// emailAddress
	@FindBy(css = "input#emailAddressTextBox")
	public WebElement emailAddress;

	// primaryLanguage
	@FindBy(css = "select#primaryLanguageDropDown")
	public WebElement primaryLanguage;

	// maritalStatus
	@FindBy(css = "select#maritalStatusDropDown")
	public WebElement maritalStatus;

	// noOfDependents
	@FindBy(css = "input#noOfDependentsTextBox")
	public WebElement noOfDependents;

	// employmentStatus
	@FindBy(css = "select#employmentStatusDropDown")
	public WebElement employmentStatus;

	// hireDate
	@FindBy(css = "input#hireDateTextBox")
	public WebElement hireDate;

	// hireState
	@FindBy(css = "select#hireStateDropDown")
	public WebElement hireState;

	@FindBy(css = "input#witness0FirstNameTextBox")
	public WebElement witnessFirstName;

	@FindBy(css = "input#witness0MiddleNameTextBox")
	public WebElement witnessMiddleName;

	@FindBy(css = "input#witness0LastNameTextBox")
	public WebElement witnessLastName;
	@FindBy(css = "input#witness0PhoneTextBox")
	public WebElement witnessPhoneNumber;

	// jurisdictionState
	@FindBy(css = "select#benefitDropDown")
	public WebElement jurisdictionState;

	// occupationCode
	@FindBy(css = "input#ncciCodeTextBox")
	public WebElement occupationCode;

	// occupationDesc
	@FindBy(css = "input#ncciReadonlyTextBox")
	public WebElement occupationDesc;

	// nAICSCode
	@FindBy(css = "input#naicsTextBox")
	public WebElement nAICSCode;

	// searchNAICS
	@FindBy(css = "img#searchNaicsImageButton")
	public WebElement searchNAICS;

	// nAICSDesc
	@FindBy(css = "input#naicsReadOnlyTextBox")
	public WebElement nAICSDesc;

	// wageRate
	@FindBy(css = "input#wageRateTextBox")
	public WebElement wageRate;

	// Per
	@FindBy(css = "select#perDropDown")
	public WebElement per;

	// Days WorkedPerWeek
	@FindBy(css = "select#daysWorkedPerWeekDropDown")
	public WebElement daysWorkedPerWeek;

	// Is Full Day pay
	@FindBy(css = "input#isFullPayRadioButton_0")
	public WebElement fullDayYes;

	@FindBy(css = "input#isFullPayRadioButton_1")
	public WebElement fullDayNo;

	// Did Salary Continue?
	@FindBy(css = "input#isSalaryContinueRadioButton_0")
	public WebElement didSalaryContinueYes;

	@FindBy(css = "input#isSalaryContinueRadioButton_1")
	public WebElement didSalaryContinueNo;

	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Employee/Incident Information Section:
	 */
	// dateOfKnowledge
	@FindBy(css = "input#employerDateOfKnowledgeTextBox")
	public WebElement dateOfKnowledge;

	// adminReportedDate
	@FindBy(css = "input#adminReportedDateTextBox")
	public WebElement adminReportedDate;

	// reportedToFirstName
	@FindBy(css = "input#reportedToFirstNameTextBox")
	public WebElement reportedToFirstName;

	@FindBy(css = "div#irfImageInformation")
	public WebElement claimantInformation;

	@FindBy(css = "div#irfTitleAccidentDetail")
	public WebElement incidentDetail;

	// reportedToMiddleName
	@FindBy(css = "input#reportedToMiddleNameTextBox")
	public WebElement reportedToMiddleName;

	// reportedToLastName
	@FindBy(css = "input#reportedToLastNameTextBox")
	public WebElement reportedToLastName;

	// reportedToPhoneNumber
	@FindBy(css = "input#reportedToPhoneNumberTextBox")
	public WebElement reportedToPhoneNumber;

	@FindBy(css = "input#lastWorkDateTextBox")
	public WebElement lastWorkDate;

	// reportedToJobTitle
	@FindBy(css = "input#reportedToJobTitleTextBox")
	public WebElement reportedToJobTitle;

	// reportedToEmailAddress
	@FindBy(css = "input#reportedToEmailAddressTextBox")
	public WebElement reportedToEmailAddress;

	// isfatality
	@FindBy(css = "select#fatalityDropDownList")
	public WebElement isfatality;

	// fatalityDate
	@FindBy(css = "input#fatalityDateTextBox")
	public WebElement fatalityDate;

	// isPremisesRadio
	@FindBy(css = "input#isPremisesRadio_1")
	public WebElement isPremisesRadioNo;

	@FindBy(css = "input#isPremisesRadio_0")
	public WebElement isPremisesRadioYes;

	// accidentSummary
	@FindBy(css = "textarea#accidentSummaryTextBox")
	public WebElement accidentSummary;

	// majorInjuryCause
	@FindBy(css = "select#majorInjuryCauseDropDown")
	public WebElement majorInjuryCause;

	// minorInjuryCause
	@FindBy(css = "select#minorInjuryCauseDropDown")
	public WebElement minorInjuryCause;

	@FindBy(css = "input#occurredAddress1TextBox")
	public WebElement occuredAddress1;

	@FindBy(css = "input#occurredAddress2TextBox")
	public WebElement occuredAddress2;

	@FindBy(css = "input#accidentCityTextBox")
	public WebElement occuredCity;

	@FindBy(css = "select#accidentStateDropDown")
	public WebElement occuredState;

	@FindBy(css = "input#accidentPostalTextBox")
	public WebElement occuredPostal;

	// majorInjuryNature
	@FindBy(css = "select#majorInjuryNatureDropDown")
	public WebElement majorInjuryNature;

	// minorInjuryNature
	@FindBy(css = "select#minorInjuryNatureDropDown")
	public WebElement minorInjuryNature;

	// majorBodyPart
	@FindBy(css = "select#majorBodyPartDropDown")
	public WebElement majorBodyPart;

	// minorBodyPart
	@FindBy(css = "select#minorBodyPartDropDown")
	public WebElement minorBodyPart;

	@FindBy(css = "select#treatmentTypeDropDown")
	public WebElement treatmentType;

	// injuryType
	@FindBy(css = "select#injuryTypeDropDownList")
	public WebElement injuryType;

	// isRelatedOccurrenceRadio
	@FindBy(css = "input#isRelatedOccurrenceRadio_0")
	public WebElement isRelatedOccurrenceRadioYes;

	@FindBy(css = "input#isRelatedOccurrenceRadio_1")
	public WebElement isRelatedOccurrenceRadioNo;

	// isLostTimeAnticipatedRadio
	@FindBy(css = "input#isLostTimeAnticipatedRadio_0")
	public WebElement isLostTimeAnticipatedRadioYes;

	@FindBy(css = "input#isLostTimeAnticipatedRadio_1")
	public WebElement isLostTimeAnticipatedRadioNo;

	// timeBeganWork
	@FindBy(css = "input#timeBeganWorkTextBox")
	public WebElement timeBeganWork;

	// isWitnessRadioList
	@FindBy(css = "input#isWitnessRadioList_0")
	public WebElement isWitnessRadioListYes;

	@FindBy(css = "input#isWitnessRadioList_1")
	public WebElement isWitnessRadioListNo;
	// compfirstExposure
	@FindBy(css = "input#CompfirstExposureTextBox")
	public WebElement compfirstExposure;

	@FindBy(css = "input#dateDisabilityBeganTextBox")
	public WebElement dateDisabilityBegin;

	@FindBy(css = "div#irfImageSpecialAnalysisFields")
	public WebElement specialAnalysis;

	@FindBy(css = "input#returnToWorkDateTextBox")
	public WebElement returnToWork;

	// compLastExposure
	@FindBy(css = "input#CompLastExposureTextBox")
	public WebElement compLastExposure;

	// stateClaimNumber
	@FindBy(css = "input#stateClaimNumberTextBox")
	public WebElement stateClaimNumber;

	@FindBy(css = "input#bodyPartPercentTextBox")
	public WebElement bodyPartPercent;

	@FindBy(css = "input#isContinuousTraumaCheckBox")
	public WebElement isContinuousTrauma;

	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Treatment Detail Section:
	 */
	@FindBy(css = "input#isTreatmentRequired_0")
	public WebElement isTreatmentRequiredYes;

	@FindBy(css = "input#isTreatmentRequired_1")
	public WebElement isTreatmentRequiredNo;

	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Special Analysis fields Section:
	 */
	@FindBy(css = "select#specialAnalysisControl0")
	public WebElement awardsPaidInFull;
	
	@FindBy(css = "input#specialAnalysisControl0")
	public WebElement deductible;

	@FindBy(css = "select#specialAnalysisControl1")
	public WebElement caseAccepted;
	
	@FindBy(css = "input#specialAnalysisControl1")
	public WebElement sir;

	@FindBy(css = "select#specialAnalysisControl2")
	public WebElement caseDismissed;

	@FindBy(css = "select#specialAnalysisControl3")
	public WebElement delayInDecision;
	
	@FindBy(css = "input#specialAnalysisControl2")
	public WebElement deductibleReceived;

	@FindBy(css = "select#specialAnalysisControl4")
	public WebElement destroyed;
	
	@FindBy(css = "input#specialAnalysisControl3")
	public WebElement uncollectedDeductible;
	
	@FindBy(css = "select#specialAnalysisControl5")
	public WebElement lifeMedical;

	@FindBy(css = "select#specialAnalysisControl6")
	public WebElement lifePension;

	@FindBy(css = "select#specialAnalysisControl7")
	public WebElement longTermDisability;

	@FindBy(css = "select#specialAnalysisControl8")
	public WebElement maintenanceCase;

	@FindBy(css = "select#specialAnalysisControl9")
	public WebElement megaFlexParticipant;

	@FindBy(css = "select#specialAnalysisControl10")
	public WebElement presumptiveStatus;

	@FindBy(css = "select#specialAnalysisControl11")
	public WebElement salaryContinuation;

	@FindBy(css = "select#specialAnalysisControl12")
	public WebElement scifContribution;

	@FindBy(css = "select#specialAnalysisControl13")
	public WebElement sensitiveClaim;

	@FindBy(css = "select#specialAnalysisControl14")
	public WebElement transferToIndemnity;

	@FindBy(css = "select#specialAnalysisControl15")
	public WebElement carveOut;

	@FindBy(css = "select#specialAnalysisControl16")
	public WebElement injuryCodeMisc1;

	@FindBy(css = "select#specialAnalysisControl17")
	public WebElement nCCIInjuryType;

	@FindBy(css = "select#specialAnalysisControl18")
	public WebElement maywoodFire;

	@FindBy(css = "input#conditionTextBox")
	public WebElement searchTextBoxClient;

	@FindBy(css = "div#searchResultDataList_ctl01_showImage")
	public WebElement firstCell;

	@FindBy(css = "div#irfImageTreatmentDetail")
	public WebElement treatmentRequired;
	
	@FindBy(css = "select#specialAnalysisControl0")
	public WebElement civilDefense;

	@FindBy(css = "select#specialAnalysisControl1")
	public WebElement classified;

	@FindBy(css = "select#specialAnalysisControl2")
	public WebElement closedSymptomatic;

	@FindBy(css = "select#specialAnalysisControl3")
	public WebElement text15_8;

	@FindBy(css = "select#specialAnalysisControl4")
	public WebElement finalAdjustment;

	@FindBy(css = "select#specialAnalysisControl5")
	public WebElement lumpsum;

	@FindBy(css = "select#specialAnalysisControl6")
	public WebElement maritime;

	@FindBy(css = "select#specialAnalysisControl7")
	public WebElement rehab;

	@FindBy(css = "select#specialAnalysisControl8")
	public WebElement thridParty;

	@FindBy(css = "select#specialAnalysisControl9")
	public WebElement thirdPartyLienCollect;

	@FindBy(css = "select#specialAnalysisControl10")
	public WebElement thirdPartyLienEstab;

	@FindBy(css = "select#specialAnalysisControl11")
	public WebElement thirdPartyReviewed;

	@FindBy(css = "select#specialAnalysisControl12")
	public WebElement text5105Est;

	@FindBy(css = "select#specialAnalysisControl13")
	public WebElement text5105DoesNotApply;

	@FindBy(css = "select#specialAnalysisControl14")
	public WebElement text5105Reviewed;

	@FindBy(css = "select#specialAnalysisControl15")
	public WebElement text25A;

	@FindBy(css = "select#specialAnalysisControl16")
	public WebElement subrogation;

	@FindBy(css = "select#specialAnalysisControl17")
	public WebElement medicareMedication;

	@FindBy(css = "input#specialAnalysisControl18")
	public WebElement timeInAssignment;


	@FindBy(css = "select#specialAnalysisControl19")
	public WebElement timeInAssignmentUnknown;
	
	/*
	 * WebElements for Buttons on IRF Submit screen
	 */
	@FindBy(css = "input#osha301aButton")
	public WebElement osha301aButton;

	@FindBy(css = "input#cancelButton")
	public WebElement cancelButton1;

	@FindBy(css = "input#saveButton")
	public WebElement saveButton1;

	@FindBy(css = "input#saveAndNew")
	public WebElement saveAndNewButton;

	@FindBy(css = "input#submitButton")
	public WebElement submitButton;

	@FindBy(css = "div.submittedSuccessfully")
	public WebElement successfulMessage;
	
	
	
	/*
	 * public String getClient() throws IOException{ return
	 * ExcelReader.getCell(1, 0, 1); }
	 */
	public String enterIncidentDate() throws InterruptedException, IOException {

		final Random random = new Random();
		final String month = Integer.toString(random.nextInt(12 - 1) + 1);
		final String day = Integer.toString(random.nextInt(30 - 10) + 10);
		final String year = Integer.toString(random.nextInt(2016 - 1975) + 1975);
		final String date = month + "/" + day + "/" + year;

		Wait.modifyWait(driver, incidentDate);
		incidentDate.sendKeys(date);
		ExcelReader.setCell(1, 9, 0, date);
		return date;
	}
	
	public void enterTimeOfLoss() throws IOException
	{
		Select s = new Select(timeOfLoss_hour);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		Select s1= new Select(timeOfLoss_min);
		s1.selectByIndex(BaseClass.generateRandomNumberAsInteger(s1.getOptions().size()));
		Select s2= new Select(timeOfLoss_meridian);
		s2.selectByIndex(BaseClass.generateRandomNumberAsInteger(s2.getOptions().size()));
		String time =s.getFirstSelectedOption().getText()+":"+s1.getFirstSelectedOption().getText()+" "+s2.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 95, time);
		
	}

	public void enterClient() throws Exception {
		Wait.modifyWait(driver, client);

		final String currentWindow = driver.getWindowHandle();
		searchClient.click();
		Wait.waitFor(2);
		// BaseClass baseclass = new BaseClass(driver);
		baseclass.switchToNextWindow("Search Client");
		((JavascriptExecutor) driver).executeScript("window.focus();");
		Wait.waitFor(2);
		final String client = ExcelReader.getCell(1, 0, BaseClass.generateRandomNumberAsInteger(5));
		searchTextBoxClient.sendKeys(client);
		searchButton.click();
		Wait.modifyWait(driver, firstCell);
		firstCell.click();
		Wait.waitFor(2);
		driver.switchTo().window(currentWindow);
		ExcelReader.setCell(1, 9, 1, client);

	}
	
	public void enterClientForAMLJIA() throws Exception {
		Wait.modifyWait(driver, client);

		final String currentWindow = driver.getWindowHandle();
		searchClient.click();
		Wait.waitFor(2);
		// BaseClass baseclass = new BaseClass(driver);
		baseclass.switchToNextWindow("Search Client");
		((JavascriptExecutor) driver).executeScript("window.focus();");
		Wait.waitFor(2);
		final String client = ExcelReader.getCell(2, 0, BaseClass.generateRandomNumberAsInteger(5));
		searchTextBoxClient.sendKeys(client);
		searchButton.click();
		Wait.modifyWait(driver, firstCell);
		firstCell.click();
		Wait.waitFor(2);
		driver.switchTo().window(currentWindow);
		ExcelReader.setCell(2, 9, 1, client);

	}
	
	public void enterClientForNYLAW() throws Exception {
		Wait.modifyWait(driver, client);

		final String currentWindow = driver.getWindowHandle();
		searchClient.click();
		Wait.waitFor(2);
		// BaseClass baseclass = new BaseClass(driver);
		baseclass.switchToNextWindow("Search Client");
		((JavascriptExecutor) driver).executeScript("window.focus();");
		Wait.waitFor(2);
		final String client = ExcelReader.getCell(3, 0, BaseClass.generateRandomNumberAsInteger(5));
		searchTextBoxClient.sendKeys(client);
		searchButton.click();
		Wait.modifyWait(driver, firstCell);
		firstCell.click();
		Wait.waitFor(2);
		driver.switchTo().window(currentWindow);
		ExcelReader.setCell(3, 9, 1, client);

	}

	public void enterLevelAssigned() throws Exception {
		Wait.modifyWait(driver, searchLocation);

		final String currentWindow = driver.getWindowHandle();
		searchLocation.click();
		Wait.waitFor(2);
		baseclass.switchToNextWindow("Search Location");
		((JavascriptExecutor) driver).executeScript("window.focus();");
		Wait.waitFor(2);
		final List<WebElement> clients = driver.findElements(By.cssSelector("a[id^='locationTreeView']"));
		clients.get(BaseClass.generateRandomNumberAsInteger(clients.size())).click();
		driver.switchTo().window(currentWindow);
		Wait.waitFor(3);
		final String levelAssigned = location.getAttribute("value");
		ExcelReader.setCell(1, 9, 2, levelAssigned);

	}
	
	public void enterLevelAssignedForAMLJIA() throws Exception {
		Wait.modifyWait(driver, searchLocation);

		final String currentWindow = driver.getWindowHandle();
		searchLocation.click();
		Wait.waitFor(2);
		baseclass.switchToNextWindow("Search Location");
		((JavascriptExecutor) driver).executeScript("window.focus();");
		Wait.waitFor(2);
		final List<WebElement> clients = driver.findElements(By.cssSelector("a[id^='locationTreeView']"));
		clients.get(BaseClass.generateRandomNumberAsInteger(clients.size())).click();
		driver.switchTo().window(currentWindow);
		Wait.waitFor(3);
		final String levelAssigned = location.getAttribute("value");
		ExcelReader.setCell(2, 9, 2, levelAssigned);

	}
	
	public void enterLevelAssignedForNYLAW() throws Exception {
		Wait.modifyWait(driver, searchLocation);

		final String currentWindow = driver.getWindowHandle();
		searchLocation.click();
		Wait.waitFor(2);
		baseclass.switchToNextWindow("Search Location");
		((JavascriptExecutor) driver).executeScript("window.focus();");
		Wait.waitFor(2);
		final List<WebElement> clients = driver.findElements(By.cssSelector("a[id^='locationTreeView']"));
		clients.get(BaseClass.generateRandomNumberAsInteger(clients.size())).click();
		driver.switchTo().window(currentWindow);
		Wait.waitFor(3);
		final String levelAssigned = location.getAttribute("value");
		ExcelReader.setCell(3, 9, 2, levelAssigned);

	}

	public void selectPolicyType() throws InterruptedException, IOException {
		Thread.sleep(2000);
		Wait.modifyWait(driver, policyType);
		final Select s = new Select(policyType);
		s.selectByIndex(s.getOptions().size() - 1);
		final String policyType = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 3, policyType);
	}

	public void selectPolicyTypeForAMLJIA() throws InterruptedException, IOException {
		Thread.sleep(2000);
		Wait.modifyWait(driver, policyType);
		final Select s = new Select(policyType);
		s.selectByVisibleText("Workers Compensation");
		final String policyType = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(2, 9, 3, policyType);
	}
	
	public void selectPolicyTypeForNYLAW() throws InterruptedException, IOException {
		Thread.sleep(2000);
		Wait.modifyWait(driver, policyType);
		final Select s = new Select(policyType);
		//s.selectByVisibleText("Workers Compensation");
		s.selectByIndex(1);
		final String policyType = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 3, policyType);
	}
	
	public void enterClaimantID() throws InterruptedException, IOException {
		Wait.modifyWait(driver, claimantID);
		final String claimantIDValue = BaseClass.enterRandomNumber(10);
		claimantID.sendKeys(claimantIDValue);
		ExcelReader.setCell(1, 9, 4, claimantIDValue);
	}

	public void enterFirstName() throws InterruptedException, IOException {
		Wait.modifyWait(driver, firstName);
		final String firstNameValue = BaseClass.stringGeneratorl(6);
		firstName.sendKeys(firstNameValue);
		ExcelReader.setCell(1, 9, 5, firstNameValue);

	}

	public void enterMiddleName() throws InterruptedException, IOException {
		Wait.modifyWait(driver, middleName);
		final String middleNameValue = BaseClass.stringGeneratorl(1);
		middleName.sendKeys(middleNameValue);
		ExcelReader.setCell(1, 9, 6, middleNameValue);
	}

	public void enterLastName() throws IOException {
		final String lastNameValue = BaseClass.stringGeneratorl(5);
		lastName.sendKeys(lastNameValue);
		ExcelReader.setCell(1, 9, 7, lastNameValue);
	}

	public void enterSuffix() throws IOException {
		final String suffixValue = BaseClass.stringGeneratorl(2);
		suffix.sendKeys(suffixValue);
		ExcelReader.setCell(1, 9, 8, suffixValue);

	}

	public void enterIncidentDescription() throws IOException {

		final String incidentValue = "The Incident is" + " " + BaseClass.stringGeneratorl(5);
		incidentDesc.sendKeys(incidentValue);
		ExcelReader.setCell(1, 9, 9, incidentValue);

	}

	public void selectNext() {
		nextButton.click();
	}

	public void clickClaimantInformation() throws InterruptedException {
		Wait.modifyWait(driver, claimantInformation);
		claimantInformation.click();
	}

	public void clickIncidentDetail() throws InterruptedException {
		Wait.modifyWait(driver, incidentDetail);
		incidentDetail.click();
	}

	public void selectGender() throws InterruptedException, IOException {

		final int count = BaseClass.clickRandomWebElement(genderFemale, genderMale);
		if (count == 0) {
			ExcelReader.setCell(1, 9, 10, "Female");
		} else {
			ExcelReader.setCell(1, 9, 10, "Male");
		}

	}

	public String enterDateOfBirth(String date) throws InterruptedException, ParseException, IOException {
		Wait.modifyWait(driver, dateOfBirth);
		final String dateOfBirthValue = BaseClass.generateDateLessThanSpecificDate(date);
		dateOfBirth.sendKeys(dateOfBirthValue);
		ExcelReader.setCell(1, 9, 11, dateOfBirthValue);
		return dateOfBirthValue;
	}

	public void enterAddress1() throws InterruptedException, IOException {
		Wait.modifyWait(driver, claimantAddress1);
		final String claimantAddress1Value = BaseClass.stringGeneratorl(5);
		claimantAddress1.sendKeys(claimantAddress1Value);
		ExcelReader.setCell(1, 9, 12, claimantAddress1Value);
	}

	public void enterAddress2() throws InterruptedException, IOException {
		Wait.modifyWait(driver, claimantAddress2);
		final String claimantAddress2Value = BaseClass.stringGeneratorl(5);
		claimantAddress2.sendKeys(claimantAddress2Value);
		ExcelReader.setCell(1, 9, 13, claimantAddress2Value);
	}

	public void enterPostal() throws InterruptedException, IOException {
		Wait.modifyWait(driver, claimantPostal);
		final String claimantPostalValue = BaseClass.enterRandomNumber(5);
		claimantPostal.sendKeys(claimantPostalValue);
		ExcelReader.setCell(1, 9, 14, claimantPostalValue);
	}

	public void enterCity() throws InterruptedException, IOException {
		Wait.modifyWait(driver, claimantCity);

		final String claimantCityValue = BaseClass.stringGeneratorl(5);
		claimantCity.click();
		while(!claimantCity.getAttribute("value").isEmpty())
		{
			claimantCity.clear();
		}

		claimantCity.sendKeys(claimantCityValue);
		ExcelReader.setCell(1, 9, 15, claimantCityValue);
	}



	public void enterState() throws InterruptedException, IOException {
		Wait.modifyWait(driver, claimantState);
		Wait.waitFor(3);
		final Select s = new Select(claimantState);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size() - 1));
		final String state = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 16, state);

	}

	public void enterCounty() throws InterruptedException, IOException {
		Wait.modifyWait(driver, claimantCounty);
		if (claimantCounty.getAttribute("value").isEmpty()) {
			final String claimantCountryValue = BaseClass.stringGeneratorl(5);
			claimantCounty.sendKeys(claimantCountryValue);
			ExcelReader.setCell(1, 9, 17, claimantCountryValue);
		}
	}

	public void enterHomePhone() throws InterruptedException, IOException {
		Wait.modifyWait(driver, homePhone);
		if (homePhone.getAttribute("value").isEmpty()) {
			final String homePhoneValue = BaseClass.enterRandomNumber(10);
			homePhone.sendKeys(homePhoneValue);
			ExcelReader.setCell(1, 9, 18, homePhoneValue);
		}
	}

	public void enterEmailAddress() throws InterruptedException, IOException {
		Wait.modifyWait(driver, emailAddress);
		final String emailAddressValue = "test@test.com";
		emailAddress.sendKeys(emailAddressValue);
		ExcelReader.setCell(1, 9, 19, emailAddressValue);
	}

	public void enterCellPhone() throws InterruptedException, IOException {
		Wait.modifyWait(driver, cellPhone);
		if (cellPhone.getAttribute("value").isEmpty()) {
			final String cellPhoneValue = BaseClass.enterRandomNumber(10);
			cellPhone.sendKeys(cellPhoneValue);
			ExcelReader.setCell(1, 9, 20, cellPhoneValue);
		}
	}

	public void enterPrimaryLanguage() throws InterruptedException, IOException {
		Wait.modifyWait(driver, primaryLanguage);
		final Select s = new Select(primaryLanguage);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String primaryLanguageValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 21, primaryLanguageValue);
	}

	public void enterMaritalStatus() throws InterruptedException, IOException {
		Wait.modifyWait(driver, maritalStatus);
		final Select s = new Select(maritalStatus);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String maritalStatus = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 22, maritalStatus);

	}

	public void enterNoOfDependents() throws InterruptedException, IOException {
		Wait.modifyWait(driver, noOfDependents);
		final String noOfDependentsValue = Integer.toString(BaseClass.generateRandomNumberAsInteger(10));
		noOfDependents.sendKeys(noOfDependentsValue);
		ExcelReader.setCell(1, 9, 23, noOfDependentsValue);
	}

	public void enterEmployeeStatus() throws InterruptedException, IOException {
		Wait.modifyWait(driver, employmentStatus);
		final Select s = new Select(employmentStatus);
		s.selectByIndex(s.getOptions().size() - 1);
		final String employeeStatusValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 24, employeeStatusValue);
	}

	public void enterHireDate(String date) throws InterruptedException, ParseException, IOException {
		Wait.modifyWait(driver, hireDate);
		final String hireDateValue = BaseClass.generateDateMoreThanSpecificDate(date);
		hireDate.sendKeys(hireDateValue);
		ExcelReader.setCell(1, 9, 25, hireDateValue);

	}

	public void enterHireState() throws InterruptedException, IOException {
		Wait.modifyWait(driver, hireState);
		final Select s = new Select(hireState);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String hireStateValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 26, hireStateValue);
	}

	public void enterJurisdictionSate() throws InterruptedException, IOException {
		Wait.modifyWait(driver, jurisdictionState);
		final Select s = new Select(jurisdictionState);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String jurisdictionStateValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 27, jurisdictionStateValue);
	}

	public void enterOccupationCode() throws InterruptedException, IOException {
		Wait.modifyWait(driver, occupationCode);
		final String occupationCodeValue = BaseClass.enterRandomNumber(4);
		occupationCode.sendKeys(occupationCodeValue);
		ExcelReader.setCell(1, 9, 28, occupationCodeValue);
	}

	public void enterNAICS() throws InterruptedException, IOException {
		Wait.modifyWait(driver, nAICSCode);
		final String naicsValue = BaseClass.enterRandomNumber(4);
		nAICSCode.sendKeys(naicsValue);
		ExcelReader.setCell(1, 9, 29, naicsValue);
	}

	public void enterWageRate() throws InterruptedException, IOException {
		Wait.modifyWait(driver, wageRate);
		final String wageRateValue = BaseClass.enterRandomNumber(4);
		wageRate.sendKeys(wageRateValue);
		ExcelReader.setCell(1, 9, 30, wageRateValue);
	}

	public void enterPer() throws InterruptedException, IOException {
		Wait.modifyWait(driver, per);
		final Select s = new Select(per);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size() - 1));
		final String perValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 31, perValue);
	}

	public void enterNoOfDaysWeek() throws InterruptedException, IOException {
		Wait.modifyWait(driver, daysWorkedPerWeek);
		final Select s = new Select(daysWorkedPerWeek);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size() - 1));
		final String noOfDaysWeekValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 32, noOfDaysWeekValue);

	}

	public void selectFullPayOfIncident() throws IOException {
		final int count = BaseClass.clickRandomWebElement(fullDayNo, fullDayYes);
		if (count == 0) {
			ExcelReader.setCell(1, 9, 33, "No");
		} else {
			ExcelReader.setCell(1, 9, 33, "Yes");
		}
	}

	public void selectDidSalaryContinue() throws IOException {
		final int count = BaseClass.clickRandomWebElement(didSalaryContinueYes, didSalaryContinueNo);
		if (count == 0) {
			ExcelReader.setCell(1, 9, 34, "Yes");
		} else {
			ExcelReader.setCell(1, 9, 34, "No");
		}
	}

	public void enterDateOfKnowledge(String date) throws ParseException, InterruptedException, IOException {
		Wait.modifyWait(driver, dateOfKnowledge);
		final String dateOfKnowledgeValue = BaseClass.generateDateMoreThanSpecificDate(date);
		dateOfKnowledge.sendKeys(dateOfKnowledgeValue);
		ExcelReader.setCell(1, 9, 35, dateOfKnowledgeValue);

	}

	public void enterDateReportedToAdmin(String date) throws ParseException, InterruptedException, IOException {
		Wait.modifyWait(driver, adminReportedDate);
		final String adminReportedDateValue = BaseClass.generateDateMoreThanSpecificDate(date);
		adminReportedDate.sendKeys(adminReportedDateValue);
		ExcelReader.setCell(1, 9, 36, adminReportedDateValue);
	}

	public void enterIncidentDateReportedToFirstName() throws InterruptedException, IOException {
		Wait.modifyWait(driver, reportedToFirstName);
		final String reportedToFirstNameValue = BaseClass.stringGeneratorl(5);
		reportedToFirstName.sendKeys(reportedToFirstNameValue);
		ExcelReader.setCell(1, 9, 37, reportedToFirstNameValue);

	}

	public void enterIncidentDateReportedToMiddleName() throws InterruptedException, IOException {
		Wait.modifyWait(driver, reportedToMiddleName);
		final String reportedToMiddleNameValue = BaseClass.stringGeneratorl(1);
		reportedToMiddleName.sendKeys(reportedToMiddleNameValue);
		ExcelReader.setCell(1, 9, 38, reportedToMiddleNameValue);
	}

	public void enterIncidentDateReportedToLastName() throws InterruptedException, IOException {
		Wait.modifyWait(driver, reportedToLastName);
		final String reportedToLastNameValue = BaseClass.stringGeneratorl(5);
		reportedToLastName.sendKeys(reportedToLastNameValue);
		ExcelReader.setCell(1, 9, 39, reportedToLastNameValue);
	}

	public void enterPhoneNumberInIncident() throws InterruptedException, IOException {
		Wait.modifyWait(driver, reportedToPhoneNumber);
		final String reportedToPhoneNumberValue = BaseClass.enterRandomNumber(10);
		reportedToPhoneNumber.sendKeys(reportedToPhoneNumberValue);
		ExcelReader.setCell(1, 9, 40, reportedToPhoneNumberValue);
	}

	public void enterJobTitle() throws InterruptedException, IOException

	{
		Wait.modifyWait(driver, reportedToJobTitle);
		final String reportedToJobTitleValue = BaseClass.stringGeneratorl(5);
		reportedToJobTitle.sendKeys(reportedToJobTitleValue);
		ExcelReader.setCell(1, 9, 41, reportedToJobTitleValue);

	}

	public void enterIncidentEmailAddress() throws InterruptedException, IOException

	{
		Wait.modifyWait(driver, reportedToEmailAddress);
		final String reportedToEmailAddressValue = BaseClass.stringGeneratorl(5) + "@" + "test.com";
		reportedToEmailAddress.sendKeys(reportedToEmailAddressValue);
		ExcelReader.setCell(1, 9, 42, reportedToEmailAddressValue);
	}

	public void enterFatalityRelatedToIncident() throws InterruptedException, IOException

	{
		Wait.modifyWait(driver, isfatality);
		final Select s = new Select(isfatality);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String fatlalityValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 43, fatlalityValue);

	}

	public void enterFatalityDate(String date) throws InterruptedException, ParseException, IOException

	{

		final Select s = new Select(isfatality);
		if (s.getFirstSelectedOption().getText().equals("Yes")) {
			final String fatlalityDateValue = BaseClass.generateDateMoreThanSpecificDate(date);
			fatalityDate.sendKeys(fatlalityDateValue);
			ExcelReader.setCell(1, 9, 44, fatlalityDateValue);
		}
	}

	public void selectIsPremisesCheckbox() throws InterruptedException, IOException {

		Wait.modifyWait(driver, isPremisesRadioNo);
		while (!isPremisesRadioNo.isSelected()) {
			isPremisesRadioNo.click();
		}
		ExcelReader.setCell(1, 9, 45, "No");
	}

	public void enterAddress1OfEmpoyerPremises() throws InterruptedException, IOException {

		final String occuredAddress1Value = BaseClass.stringGeneratorl(5);
		occuredAddress1.sendKeys(occuredAddress1Value);
		ExcelReader.setCell(1, 9, 46, occuredAddress1Value);

	}

	public void enterAddress2OfEmpoyerPremises() throws InterruptedException, IOException {
		Wait.modifyWait(driver, occuredAddress2);
		final String occuredAddress2Value = BaseClass.stringGeneratorl(5);
		occuredAddress2.sendKeys(occuredAddress2Value);
		ExcelReader.setCell(1, 9, 47, occuredAddress2Value);
	}

	public void enterCityEmpoyerPremises() throws InterruptedException, IOException {
		Wait.modifyWait(driver, occuredCity);
		final String occuredAddress2Value = BaseClass.stringGeneratorl(5);
		occuredCity.click();
		Wait.waitFor(4);

		while(!occuredCity.getAttribute("value").isEmpty()){
			occuredCity.clear();
		}

		occuredCity.sendKeys(occuredAddress2Value);
		ExcelReader.setCell(1, 9, 48, occuredAddress2Value);
	}

	public void enterStateEmpoyerPremises() throws InterruptedException, IOException {
		Wait.modifyWait(driver, occuredState);
		final Select s = new Select(occuredState);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String occuredStateValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 49, occuredStateValue);
	}

	public void enterPostalEmpoyerPremises() throws InterruptedException, IOException {
		Wait.modifyWait(driver, occuredPostal);
		final String occuredPostalValue = BaseClass.enterRandomNumber(5);
		occuredPostal.sendKeys(occuredPostalValue);
		ExcelReader.setCell(1, 9, 50, occuredPostalValue);
	}

	public void enterAccidentSummary() throws InterruptedException, IOException {
		Wait.modifyWait(driver, accidentSummary);
		final String accidentSummaryValue = BaseClass.stringGeneratorl(5);
		accidentSummary.sendKeys(accidentSummaryValue);
		ExcelReader.setCell(1, 9, 51, accidentSummaryValue);
	}

	public void enterCauseOfIncidentMajor() throws InterruptedException, IOException {
		Wait.modifyWait(driver, majorInjuryCause);
		final Select s = new Select(majorInjuryCause);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String majorInjuryCauseValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 52, majorInjuryCauseValue);
	}

	public void enterCauseOfIncidentMinor() throws InterruptedException, IOException {
		Wait.modifyWait(driver, minorInjuryCause);
		final Select s = new Select(minorInjuryCause);
		if (!(s.getOptions().size() == 1)) {
			s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		}
		final String minorInjuryCauseValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 53, minorInjuryCauseValue);

	}

	public void enterNatureOfIncidentMajor() throws InterruptedException, IOException {
		Wait.modifyWait(driver, majorInjuryNature);
		final Select s = new Select(majorInjuryNature);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String majorInjuryNatureValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 54, majorInjuryNatureValue);
	}

	public void enterNatureOfIncidentMinor() throws InterruptedException, IOException {
		Wait.modifyWait(driver, minorInjuryNature);
		final Select s = new Select(minorInjuryNature);
		if (!(s.getOptions().size() == 1)) {
			s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		}
		final String minorInjuryNatureValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 55, minorInjuryNatureValue);
	}

	public void enterPartOfIncidentMajor() throws InterruptedException, IOException {
		Wait.modifyWait(driver, majorBodyPart);
		final Select s = new Select(majorBodyPart);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String majorBodyPartValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 56, majorBodyPartValue);
	}

	public void enterPartOfIncidentMinor() throws InterruptedException, IOException {
		Wait.modifyWait(driver, minorBodyPart);
		final Select s = new Select(minorBodyPart);
		if (!(s.getOptions().size() == 1)) {
			s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		}
		final String minorBodyPartValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 57, minorBodyPartValue);
	}

	public void enterTypeOfIncidentCode() throws InterruptedException, IOException {
		Wait.modifyWait(driver, injuryType);
		final Select s = new Select(injuryType);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String injuryTypeValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 58, injuryTypeValue);
	}

	public void selectIncidentRelated() throws IOException {
		final int count = BaseClass.clickRandomWebElement(isRelatedOccurrenceRadioYes, isRelatedOccurrenceRadioNo);
		if (count == 0) {
			ExcelReader.setCell(1, 9, 59, "Yes");
		} else {
			ExcelReader.setCell(1, 9, 59, "No");
		}
	}

	public void selectLostTimeAnticipated() throws IOException {
		final int count = BaseClass.clickRandomWebElement(isLostTimeAnticipatedRadioYes, isLostTimeAnticipatedRadioNo);
		if (count == 0) {
			ExcelReader.setCell(1, 9, 60, "Yes");
		} else {
			ExcelReader.setCell(1, 9, 60, "No");
		}
	}

	public void enterTimeBeganWork() throws InterruptedException, IOException {
		Wait.modifyWait(driver, timeBeganWork);
		String time = BaseClass.generateRandomNumberAsInteger(24) + "00";
		if (time.length() == 3) {
			time = "0" + time;
		}
		timeBeganWork.sendKeys(time);
		ExcelReader.setCell(1, 9, 61, time);
	}

	public void selecWasThereAWitness() throws IOException {
		final int count = BaseClass.clickRandomWebElement(isWitnessRadioListYes, isWitnessRadioListNo);
		if (count == 0) {
			ExcelReader.setCell(1, 9, 62, "Yes");
		} else {
			ExcelReader.setCell(1, 9, 62, "No");
		}
	}

	public void enterWitnessFirstName() throws IOException {
		if (isWitnessRadioListYes.isSelected()) {
			final String witnessFistNameValue = BaseClass.stringGeneratorl(5);
			witnessFirstName.sendKeys(witnessFistNameValue);
			ExcelReader.setCell(1, 9, 63, witnessFistNameValue);
		}

	}

	public void enterWitnessMiddleName() throws IOException {
		if (isWitnessRadioListYes.isSelected()) {
			final String witnessMiddleNameValue = BaseClass.stringGeneratorl(1);
			witnessMiddleName.sendKeys(witnessMiddleNameValue);
			ExcelReader.setCell(1, 9, 64, witnessMiddleNameValue);
		}
	}

	public void enterWitnessLastName() throws IOException {
		if (isWitnessRadioListYes.isSelected()) {
			final String witnessLastNameValue = BaseClass.stringGeneratorl(5);
			witnessLastName.sendKeys(witnessLastNameValue);
			ExcelReader.setCell(1, 9, 65, witnessLastNameValue);
		}
	}

	public void enterWitnessPhoneNumber() throws IOException {
		if (isWitnessRadioListYes.isSelected()) {
			final String witnessPhoneNumberValue = BaseClass.enterRandomNumber(10);
			witnessPhoneNumber.sendKeys(witnessPhoneNumberValue);
			ExcelReader.setCell(1, 9, 66, witnessPhoneNumberValue);
		}
	}

	public void enterFirstExposureDate(String date) throws ParseException, InterruptedException, IOException {
		Wait.modifyWait(driver, compfirstExposure);
		final String compfirstExposureValue = BaseClass.generateDateMoreThanSpecificDate(date);
		compfirstExposure.sendKeys(compfirstExposureValue);
		ExcelReader.setCell(1, 9, 67, compfirstExposureValue);

	}

	public void enterLastExposureDate(String date) throws ParseException, InterruptedException, IOException {
		Wait.modifyWait(driver, compLastExposure);
		final String compLastExposureValue = BaseClass.generateDateMoreThanSpecificDate(date);
		compLastExposure.sendKeys(compLastExposureValue);
		ExcelReader.setCell(1, 9, 68, compLastExposureValue);

	}

	public void enterStateClaimNumber() throws ParseException, InterruptedException, IOException {
		Wait.modifyWait(driver, stateClaimNumber);
		final String stateClaimNumberValue = BaseClass.enterRandomNumber(5);
		stateClaimNumber.sendKeys(stateClaimNumberValue);
		ExcelReader.setCell(1, 9, 69, stateClaimNumberValue);
	}

	public void enterBodyPerCent() throws ParseException, InterruptedException, IOException {
		Wait.modifyWait(driver, bodyPartPercent);
		final String bodyPartPercentValue = BaseClass.enterRandomNumber(2);
		bodyPartPercent.sendKeys(bodyPartPercentValue);
		ExcelReader.setCell(1, 9, 70, bodyPartPercentValue);
	}

	public void selectContinousTrauma() throws ParseException, InterruptedException {
		Wait.modifyWait(driver, isContinuousTrauma);
		isContinuousTrauma.click();
	}

	public void selectIncidentDetail() throws InterruptedException {
		Wait.modifyWait(driver, incidentDetail);
		incidentDetail.click();
	}

	public String enterDateDisabilityBegin(String date) throws ParseException, IOException {

		String dateDisabilityBeginValue = "";
		if (isLostTimeAnticipatedRadioYes.isSelected()) {
			dateDisabilityBeginValue = BaseClass.generateDateMoreThanSpecificDate(date);
			dateDisabilityBegin.sendKeys(dateDisabilityBeginValue);
			ExcelReader.setCell(1, 9, 71, dateDisabilityBeginValue);

		}
		return dateDisabilityBeginValue;
	}

	public void enterTreatmentDetail() throws IOException {
		final int count = BaseClass.clickRandomWebElement(isTreatmentRequiredYes, isTreatmentRequiredNo);
		if (count == 0) {
			ExcelReader.setCell(1, 9, 74, "Yes");
		} else {
			ExcelReader.setCell(1, 9, 74, "No");
		}

	}

	public void enterTreatmentType() throws IOException {
		if (isTreatmentRequiredYes.isSelected()) {
			final Select s = new Select(treatmentType);
			s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
			final String temp = s.getFirstSelectedOption().getText();
			ExcelReader.setCell(1, 9, 75, temp);
		}

	}

	public void clickTreatmentDetail() throws InterruptedException {
		Wait.modifyWait(driver, treatmentRequired);
		treatmentRequired.click();
	}

	public void enterReturnToWork(String date) throws ParseException, IOException {
		if (isLostTimeAnticipatedRadioYes.isSelected()) {
			final String temp = BaseClass.generateDateMoreThanSpecificDate(date);
			returnToWork.sendKeys(temp);
			ExcelReader.setCell(1, 9, 72, temp);
		}
	}

	public void enterLastWorkDate(String date) throws ParseException, IOException {
		if (isLostTimeAnticipatedRadioYes.isSelected()) {
			final String lastWorkDateValue = BaseClass.generateDateLessThanSpecificDate(date);
			lastWorkDate.sendKeys(lastWorkDateValue);
			ExcelReader.setCell(1, 9, 73, lastWorkDateValue);
		}
	}

	public void clickSpecialAnalysis() throws InterruptedException {
		Wait.modifyWait(driver, specialAnalysis);
		specialAnalysis.click();
	}

	public void selectAwardsPaidInFull() throws InterruptedException, IOException {

		Wait.modifyWait(driver, awardsPaidInFull);
		final Select s = new Select(awardsPaidInFull);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String awardsPaidInFullValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 76, awardsPaidInFullValue);

	}

	public void selectCaseAccepted() throws InterruptedException, IOException {
		Wait.modifyWait(driver, caseAccepted);
		final Select s = new Select(caseAccepted);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String caseAcceptedValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 77, caseAcceptedValue);

	}

	public void selectCaseDismissed() throws InterruptedException, IOException {
		Wait.modifyWait(driver, caseDismissed);
		final Select s = new Select(caseDismissed);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String caseDismissedValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 78, caseDismissedValue);

	}

	public void selectDelayInDecision() throws InterruptedException, IOException {
		Wait.modifyWait(driver, delayInDecision);
		final Select s = new Select(delayInDecision);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String delayInDecisionValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 79, delayInDecisionValue);

	}

	public void selectDestroyed() throws InterruptedException, IOException {
		Wait.modifyWait(driver, destroyed);
		final Select s = new Select(destroyed);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String destroyedValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 80, destroyedValue);

	}

	public void selectLifeMedical() throws InterruptedException, IOException {
		Wait.modifyWait(driver, lifeMedical);
		final Select s = new Select(lifeMedical);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String lifeMedicalValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 81, lifeMedicalValue);

	}

	public void selectLifePension() throws InterruptedException, IOException {
		Wait.modifyWait(driver, lifePension);
		final Select s = new Select(lifePension);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String lifePensionValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 82, lifePensionValue);

	}

	public void selectLongTermDisability() throws InterruptedException, IOException {
		Wait.modifyWait(driver, longTermDisability);
		final Select s = new Select(longTermDisability);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 83, temp);

	}

	public void selectMaintenanceCase() throws InterruptedException, IOException {
		Wait.modifyWait(driver, maintenanceCase);
		final Select s = new Select(maintenanceCase);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 84, temp);

	}

	public void selectMegaFlexParticipant() throws InterruptedException, IOException {
		Wait.modifyWait(driver, megaFlexParticipant);
		final Select s = new Select(megaFlexParticipant);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 85, temp);

	}

	public void selectPresumptiveStatus() throws InterruptedException, IOException {
		Wait.modifyWait(driver, presumptiveStatus);
		final Select s = new Select(presumptiveStatus);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 86, temp);

	}

	public void selectSalaryContination() throws InterruptedException, IOException {
		Wait.modifyWait(driver, salaryContinuation);
		final Select s = new Select(salaryContinuation);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 87, temp);

	}

	public void selectSCIFContribution() throws InterruptedException, IOException {
		Wait.modifyWait(driver, scifContribution);
		final Select s = new Select(scifContribution);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 88, temp);

	}

	public void selectSensitiveClaim() throws InterruptedException, IOException {
		Wait.modifyWait(driver, sensitiveClaim);
		final Select s = new Select(sensitiveClaim);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 89, temp);
	}

	public void selectTransferToIndemnity() throws InterruptedException, IOException {
		Wait.modifyWait(driver, transferToIndemnity);
		final Select s = new Select(transferToIndemnity);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 90, temp);
	}

	public void selectCarveOut() throws InterruptedException, IOException {
		Wait.modifyWait(driver, carveOut);
		final Select s = new Select(carveOut);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 91, temp);
	}

	public void selectInjuryCodeMisc() throws InterruptedException, IOException {
		Wait.modifyWait(driver, injuryCodeMisc1);
		final Select s = new Select(injuryCodeMisc1);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 92, temp);
	}

	public void selectNCCIInjuryCode() throws InterruptedException, IOException {
		Wait.modifyWait(driver, nCCIInjuryType);
		final Select s = new Select(nCCIInjuryType);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String temp = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(1, 9, 93, temp);
	}

	public void selectMaywoodFire() throws InterruptedException, IOException {
		try {

			final Select s = new Select(maywoodFire);
			s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
			final String temp = s.getFirstSelectedOption().getText();
			ExcelReader.setCell(1, 9, 94, temp);
		} catch (final NoSuchElementException e) {
			e.getMessage();
		}

	}

	public void enterDeductible(){
		deductible.sendKeys(BaseClass.enterRandomNumber(3));
		
	}
	
	public void enterSIR(){
		sir.sendKeys(BaseClass.enterRandomNumber(3));
		
	}
	
	public void enterDeductibleReceived(){
		deductibleReceived.sendKeys(BaseClass.enterRandomNumber(3));
		
	}
	
	public void enterUncollectedDeductible(){
		uncollectedDeductible.sendKeys(BaseClass.enterRandomNumber(3));
		
	}
	
	
	public void selectSubmit() throws InterruptedException {
		Wait.modifyWait(driver, submitButton);
		submitButton.click();
	}
	
	public String getClaimNumber(){
		String firstMessage = successfulMessage.getText();
		String test = firstMessage.substring(81);
		System.out.println("Claim number: "+test);
		System.out.println("Claim successful message" + successfulMessage.getText());
		return test;
	}

	public String getClaimNumberForNYLAW(){
		String firstMessage = successfulMessage.getText();
		String test = firstMessage.substring(80);
		System.out.println("Claim number: "+test);
		System.out.println("Claim successful message" + successfulMessage.getText());
		return test;
	}

	public void selectcivilDefense() throws Exception{
		Wait.modifyWait(driver, civilDefense);
		final Select s = new Select(civilDefense);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String civilDefenseValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 76, civilDefenseValue);
	}
	public void selectclassified() throws Exception{
		Wait.modifyWait(driver, classified);
		final Select s = new Select(classified);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String classifiedValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 77, classifiedValue);
	}
	public void selectclosedSymptomatic() throws Exception{
		Wait.modifyWait(driver, closedSymptomatic);
		final Select s = new Select(closedSymptomatic);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String closedSymptomaticValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 78, closedSymptomaticValue);
	}
	public void selecttext15_8() throws Exception{
		Wait.modifyWait(driver, text15_8);
		final Select s = new Select(text15_8);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String text15_8Value = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 79, text15_8Value);
	}
	public void selectfinalAdjustment() throws Exception{
		Wait.modifyWait(driver, finalAdjustment);
		final Select s = new Select(finalAdjustment);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String finalAdjustmentValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 80, finalAdjustmentValue);
	}
	public void selectlumpsum() throws Exception{
		Wait.modifyWait(driver, lumpsum);
		final Select s = new Select(lumpsum);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String lumpsumValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 81, lumpsumValue);
	}
	public void selectmaritime() throws Exception{
		Wait.modifyWait(driver, maritime);
		final Select s = new Select(maritime);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String maritimeValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 82, maritimeValue);
	}
	public void selectrehab() throws Exception{
		Wait.modifyWait(driver, rehab);
		final Select s = new Select(rehab);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String rehabValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 83, rehabValue);
	}
	public void selectthridParty() throws Exception{
		Wait.modifyWait(driver, thridParty);
		final Select s = new Select(thridParty);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String thridPartyValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 84, thridPartyValue);
	}
	public void selectthirdPartyLienCollect() throws Exception{
		Wait.modifyWait(driver, thirdPartyLienCollect);
		final Select s = new Select(thirdPartyLienCollect);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String thirdPartyLienCollectValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 85, thirdPartyLienCollectValue);
	}
	public void selectthirdPartyLienEstab() throws Exception{
		Wait.modifyWait(driver, thirdPartyLienEstab);
		final Select s = new Select(thirdPartyLienEstab);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String thirdPartyLienEstabValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 86, thirdPartyLienEstabValue);
	}
	public void selectthirdPartyReviewed() throws Exception{
		Wait.modifyWait(driver, thirdPartyReviewed);
		final Select s = new Select(thirdPartyReviewed);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String thirdPartyReviewedValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 87, thirdPartyReviewedValue);
	}
	public void selecttext5105Est() throws Exception{
		Wait.modifyWait(driver, text5105Est);
		final Select s = new Select(text5105Est);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String text5105EstValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 88, text5105EstValue);
	}
	public void selecttext5105DoesNotApply() throws Exception{
		Wait.modifyWait(driver, text5105DoesNotApply);
		final Select s = new Select(text5105DoesNotApply);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String text5105DoesNotApplyValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 89, text5105DoesNotApplyValue);
	}
	public void selecttext5105Reviewed() throws Exception{
		Wait.modifyWait(driver, text5105Reviewed);
		final Select s = new Select(text5105Reviewed);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String text5105ReviewedValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 90, text5105ReviewedValue);
	}
	public void selecttext25A() throws Exception{
		Wait.modifyWait(driver, text25A);
		final Select s = new Select(text25A);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String text25AValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 91, text25AValue);
	}
	public void selectsubrogation() throws Exception{
		Wait.modifyWait(driver, subrogation);
		final Select s = new Select(subrogation);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String subrogationValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 92, subrogationValue);
	}
	public void selectmedicareMedication() throws Exception{
		Wait.modifyWait(driver, medicareMedication);
		final Select s = new Select(medicareMedication);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String medicareMedicationValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 93, medicareMedicationValue);
	}
	public void entertimeInAssignment() throws Exception{
		timeInAssignment.sendKeys(BaseClass.enterRandomNumber(5));
		String textValue = timeInAssignment.getText();
		ExcelReader.setCell(3, 9, 94, textValue);
		
	}
	public void selecttimeInAssignmentUnknown() throws Exception{
		Wait.modifyWait(driver, timeInAssignmentUnknown);
		final Select s = new Select(timeInAssignmentUnknown);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		final String timeInAssignmentUnknownValue = s.getFirstSelectedOption().getText();
		ExcelReader.setCell(3, 9, 95, timeInAssignmentUnknownValue);
	}


}
