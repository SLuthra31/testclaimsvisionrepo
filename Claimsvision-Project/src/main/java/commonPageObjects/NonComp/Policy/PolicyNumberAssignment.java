package commonPageObjects.NonComp.Policy;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.BaseClass;

public class PolicyNumberAssignment {

	WebDriver driver;
	
	@FindBy(css="div#controlPanel > fieldset > legend")
	public WebElement workerCompHeading;
	
	@FindBy(css="input#PolicyNumberAssignment_CompIncludeHyphen")
	public WebElement includeHyphensComp;
	
	@FindBy(css="input#PolicyNumberAssignment_CompCompanyTextBox")
	public WebElement compCompany;
	
	@FindBy(css="input#PolicyNumberAssignment_CompCarrierTextBox")
	public WebElement compCarrier;
	
	@FindBy(css="input#PolicyNumberAssignment_CompClientTextBox")
	public WebElement compClient;
	
	@FindBy(css="input#PolicyNumberAssignment_CompPolicyType")
	public WebElement compPolicyType;
	
	@FindBy(css="input#PolicyNumberAssignment_CompShortPolicyNumber")
	public WebElement compShortPolicyNumber;
	
	@FindBy(css="input#PolicyNumberAssignment_CompRenewalNumber")
	public WebElement compRenewalNumber;
	
	@FindBy(css="input#PolicyNumberAssignment_CompPolicyNumberAddButton")
	public WebElement compSave;
	
	@FindBy(css="input#PolicyNumberAssignment_CompPolicyNumberClearButton")
	public WebElement compClear;
	
	/*
	 * WebElements of Non Comp Policy Setup
	 */
	@FindBy(css="div#controlPanel > fieldset + fieldset > legend")
	public WebElement nonCompHeading;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompIncludeHyphen")
	public WebElement includeHyphensNonComp;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompCompanyTextBox")
	public WebElement nonCompCompany;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompCarrierTextBox")
	public WebElement nonCompCarrier;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompClientTextBox")
	public WebElement nonCompClient;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompPolicyType")
	public WebElement nonCompPolicyType;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompShortPolicyNumber")
	public WebElement nonCompShortPolicyNumber;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompRenewalNumber")
	public WebElement nonCompRenewalNumber;

	@FindBy(css="input#PolicyNumberAssignment_NonCompPolicyNumberAddButton")
	public WebElement nonCompSave;
	
	@FindBy(css="input#PolicyNumberAssignment_NonCompPolicyNumberClearButton")
	public WebElement nonCompClear;
	
	@FindBy(css="span#PolicyNumberAssignment_messageLabel")
	public WebElement messageValidation;
	

	public boolean isSelectedCompHyphen(){
		boolean temp=false;
		if(temp==includeHyphensComp.isSelected())
			temp=true;
		else
			temp=false;
		return temp;
	}
	
	public void comp_PolicyNumberAscending(){
		
		if(includeHyphensComp.isSelected()){
			setUp_Comp_PolicyNumberAssignmentConfiguration_Ascending();		
		}
		else{
		includeHyphensComp.click();
		setUp_Comp_PolicyNumberAssignmentConfiguration_Ascending();
		}
			
	}
	
	public void comp_PolicyNumberDescending_withHyphens(){
		if(includeHyphensComp.isSelected()){
			setUp_Comp_PolicyNumberAssignmentConfiguration_Descending();
		}
		else{
		includeHyphensComp.click();
		setUp_Comp_PolicyNumberAssignmentConfiguration_Descending();
		}
	}
	
	

	public void nonComp_PolicyNumberAscending(){
	
		if(includeHyphensNonComp.isSelected()){
			setUp_NonComp_PolicyNumberAssignmentConfiguration_Ascending();
		}
		else{
			includeHyphensNonComp.click();
			setUp_NonComp_PolicyNumberAssignmentConfiguration_Ascending();
		}
		
	}
	
	public void nonComp_PolicyNumberDescending_withHyphens(){
		
		if(includeHyphensNonComp.isSelected()){
			setUp_NonComp_PolicyNumberAssignmentConfiguration_Descending();
		}
		else{
		includeHyphensNonComp.click();
		setUp_NonComp_PolicyNumberAssignmentConfiguration_Descending();
		}
	}
	
	public void nonComp_PolicyNumber_InvalidOrder(){
		if(includeHyphensNonComp.isSelected()){
			nonCompCompany.clear();
			nonCompCompany.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompCarrier.clear();
			nonCompCarrier.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompClient.clear();
			nonCompClient.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompPolicyType.clear();
			nonCompPolicyType.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompShortPolicyNumber.clear();
			nonCompShortPolicyNumber.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompRenewalNumber.clear();
			nonCompRenewalNumber.sendKeys(BaseClass.enterRandomNumber(2));
			
		}
		else{
			includeHyphensNonComp.click();
			nonCompCompany.clear();
			nonCompCompany.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompCarrier.clear();
			nonCompCarrier.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompClient.clear();
			nonCompClient.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompPolicyType.clear();
			nonCompPolicyType.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompShortPolicyNumber.clear();
			nonCompShortPolicyNumber.sendKeys(BaseClass.enterRandomNumber(2));
			nonCompRenewalNumber.clear();
			nonCompRenewalNumber.sendKeys(BaseClass.enterRandomNumber(2));	}
	}
	
	public void comp_PolicyNumber_InvalidOrder(){
		if(includeHyphensNonComp.isSelected()){
			compCompany.clear();
			compCompany.sendKeys(BaseClass.enterRandomNumber(2));
			compCarrier.clear();
			compCarrier.sendKeys(BaseClass.enterRandomNumber(2));
			compClient.clear();
			compClient.sendKeys(BaseClass.enterRandomNumber(2));
			compPolicyType.clear();
			compPolicyType.sendKeys(BaseClass.enterRandomNumber(2));
			compShortPolicyNumber.clear();
			compShortPolicyNumber.sendKeys(BaseClass.enterRandomNumber(2));
			compRenewalNumber.clear();
			compRenewalNumber.sendKeys(BaseClass.enterRandomNumber(2));
			
		}
		else{
			includeHyphensNonComp.click();
			compCompany.clear();
			compCompany.sendKeys(BaseClass.enterRandomNumber(2));
			compCarrier.clear();
			compCarrier.sendKeys(BaseClass.enterRandomNumber(2));
			compClient.clear();
			compClient.sendKeys(BaseClass.enterRandomNumber(2));
			compPolicyType.clear();
			compPolicyType.sendKeys(BaseClass.enterRandomNumber(2));
			compShortPolicyNumber.clear();
			compShortPolicyNumber.sendKeys(BaseClass.enterRandomNumber(2));
			compRenewalNumber.clear();
			compRenewalNumber.sendKeys(BaseClass.enterRandomNumber(2));
			}
	}
	
	public void nonComp_PolicyNumber_NotConfiguredAllFields(){
		if(includeHyphensNonComp.isSelected()){
			setUp_NonComp_PolicyNumberAssignmentConfiguration_NotFull();
		}
		else{
			includeHyphensNonComp.click();
			setUp_NonComp_PolicyNumberAssignmentConfiguration_NotFull();
		}
	}
	
	public void comp_PolicyNumber_NotConfiguredAllFields(){
		if(includeHyphensNonComp.isSelected()){
			setUp_Comp_PolicyNumberAssignmentConfiguration_NotFull();
		}
		else{
			includeHyphensNonComp.click();
			setUp_Comp_PolicyNumberAssignmentConfiguration_NotFull();
		}
	}
	
	public void setUp_NonComp_PolicyNumberAssignmentConfiguration_Ascending() {
		boolean temp = false;
		List<String> expectedOrders = new ArrayList<String>();
		expectedOrders.add("1");
		expectedOrders.add("2");
		expectedOrders.add("3");
		expectedOrders.add("4");
		expectedOrders.add("5");
		expectedOrders.add("6");

		List<String> actualOrders = new ArrayList<String>();
		actualOrders.add(nonCompCompany.getAttribute("value"));
		actualOrders.add(nonCompCarrier.getAttribute("value"));
		actualOrders.add(nonCompClient.getAttribute("value"));
		actualOrders.add(nonCompPolicyType.getAttribute("value"));
		actualOrders.add(nonCompShortPolicyNumber.getAttribute("value"));
		actualOrders.add(nonCompRenewalNumber.getAttribute("value"));

		if (!expectedOrders.equals(actualOrders)) {
			temp = true;
		}

		if (temp) {
			nonCompCompany.clear();
			nonCompCompany.sendKeys("1");
			nonCompCarrier.clear();
			nonCompCarrier.sendKeys("2");
			nonCompClient.clear();
			nonCompClient.sendKeys("3");
			nonCompPolicyType.clear();
			nonCompPolicyType.sendKeys("4");
			nonCompShortPolicyNumber.clear();
			nonCompShortPolicyNumber.sendKeys("5");
			nonCompRenewalNumber.clear();
			nonCompRenewalNumber.sendKeys("6");
		}

	}
	
	public void setUp_NonComp_PolicyNumberAssignmentConfiguration_NotFull() {
		
		if(!includeHyphensNonComp.isSelected())
		{
			includeHyphensNonComp.click();
		}
		
//		List<String> expectedOrders = new ArrayList<String>();
//		expectedOrders.add("1");
//		expectedOrders.add("2");
//		expectedOrders.add("");
//		expectedOrders.add("3");
//		expectedOrders.add("4");
//		expectedOrders.add("5");
//
//		List<String> actualOrders = new ArrayList<String>();
//		actualOrders.add(nonCompCompany.getAttribute("value"));
//		actualOrders.add(nonCompCarrier.getAttribute("value"));
//		actualOrders.add(nonCompClient.getAttribute("value"));
//		actualOrders.add(nonCompPolicyType.getAttribute("value"));
//		actualOrders.add(nonCompShortPolicyNumber.getAttribute("value"));
//		actualOrders.add(nonCompRenewalNumber.getAttribute("value"));
//
//		if (!expectedOrders.equals(actualOrders)) {
//			temp = true;
//		}
//
//		if (temp) {
			nonCompCompany.clear();
			nonCompCompany.sendKeys("1");
			nonCompCarrier.clear();
			nonCompCarrier.sendKeys("2");
			nonCompClient.clear();
			nonCompPolicyType.clear();
			nonCompPolicyType.sendKeys("3");
			nonCompShortPolicyNumber.clear();
			nonCompShortPolicyNumber.sendKeys("4");
			nonCompRenewalNumber.clear();
			nonCompRenewalNumber.sendKeys("5");
//		}

	}
	
	public void setUp_NonComp_PolicyNumberAssignmentConfiguration_AllBlank() {
		boolean temp = false;
		List<String> actualOrders = new ArrayList<String>();
		actualOrders.add(nonCompCompany.getAttribute("value"));
		actualOrders.add(nonCompCarrier.getAttribute("value"));
		actualOrders.add(nonCompClient.getAttribute("value"));
		actualOrders.add(nonCompPolicyType.getAttribute("value"));
		actualOrders.add(nonCompShortPolicyNumber.getAttribute("value"));
		actualOrders.add(nonCompRenewalNumber.getAttribute("value"));

		if (!actualOrders.isEmpty()) {
			temp = true;
		}

		if (temp) {
			nonCompCompany.clear();
			nonCompCarrier.clear();
			nonCompClient.clear();
			nonCompPolicyType.clear();
			nonCompShortPolicyNumber.clear();
			nonCompRenewalNumber.clear();
			
		}

	}
	
	
	public void setUp_NonComp_PolicyNumberAssignmentConfiguration_Descending() {
		boolean temp = false;
		List<String> expectedOrders = new ArrayList<String>();
		expectedOrders.add("6");
		expectedOrders.add("5");
		expectedOrders.add("4");
		expectedOrders.add("3");
		expectedOrders.add("2");
		expectedOrders.add("1");

		List<String> actualOrders = new ArrayList<String>();
		actualOrders.add(nonCompCompany.getAttribute("value"));
		actualOrders.add(nonCompCarrier.getAttribute("value"));
		actualOrders.add(nonCompClient.getAttribute("value"));
		actualOrders.add(nonCompPolicyType.getAttribute("value"));
		actualOrders.add(nonCompShortPolicyNumber.getAttribute("value"));
		actualOrders.add(nonCompRenewalNumber.getAttribute("value"));

		if (!expectedOrders.equals(actualOrders)) {
			temp = true;
		}

		if (temp) {
			nonCompCompany.clear();
			nonCompCompany.sendKeys("6");
			nonCompCarrier.clear();
			nonCompCarrier.sendKeys("5");
			nonCompClient.clear();
			nonCompClient.sendKeys("4");
			nonCompPolicyType.clear();
			nonCompPolicyType.sendKeys("3");
			nonCompShortPolicyNumber.clear();
			nonCompShortPolicyNumber.sendKeys("2");
			nonCompRenewalNumber.clear();
			nonCompRenewalNumber.sendKeys("1");
		}

	}
	
	public void setUp_Comp_PolicyNumberAssignmentConfiguration_Ascending() {
		boolean temp = false;
		List<String> expectedOrders = new ArrayList<String>();
		expectedOrders.add("1");
		expectedOrders.add("2");
		expectedOrders.add("3");
		expectedOrders.add("4");
		expectedOrders.add("5");
		expectedOrders.add("6");

		List<String> actualOrders = new ArrayList<String>();
		actualOrders.add(compCompany.getAttribute("value"));
		actualOrders.add(compCarrier.getAttribute("value"));
		actualOrders.add(compClient.getAttribute("value"));
		actualOrders.add(compPolicyType.getAttribute("value"));
		actualOrders.add(compShortPolicyNumber.getAttribute("value"));
		actualOrders.add(compRenewalNumber.getAttribute("value"));

		if (!expectedOrders.equals(actualOrders)) {
			temp = true;
		}

		if (temp) {
			compCompany.clear();
			compCompany.sendKeys("1");
			compCarrier.clear();
			compCarrier.sendKeys("2");
			compClient.clear();
			compClient.sendKeys("3");
			compPolicyType.clear();
			compPolicyType.sendKeys("4");
			compShortPolicyNumber.clear();
			compShortPolicyNumber.sendKeys("5");
			compRenewalNumber.clear();
			compRenewalNumber.sendKeys("6");
		}

	}

	public void setUp_Comp_PolicyNumberAssignmentConfiguration_NotFull() {
		boolean temp = false;
		List<String> expectedOrders = new ArrayList<String>();
		expectedOrders.add("1");
		expectedOrders.add("2");
		expectedOrders.add("");
		expectedOrders.add("3");
		expectedOrders.add("4");
		expectedOrders.add("5");

		List<String> actualOrders = new ArrayList<String>();
		actualOrders.add(compCompany.getAttribute("value"));
		actualOrders.add(compCarrier.getAttribute("value"));
		actualOrders.add(compClient.getAttribute("value"));
		actualOrders.add(compPolicyType.getAttribute("value"));
		actualOrders.add(compShortPolicyNumber.getAttribute("value"));
		actualOrders.add(compRenewalNumber.getAttribute("value"));

		if (!expectedOrders.equals(actualOrders)) {
			temp = true;
		}

		if (temp) {
			compCompany.clear();
			compCompany.sendKeys("1");
			compCarrier.clear();
			compCarrier.sendKeys("2");
			compClient.clear();
			compPolicyType.clear();
			compPolicyType.sendKeys("3");
			compShortPolicyNumber.clear();
			compShortPolicyNumber.sendKeys("4");
			compRenewalNumber.clear();
			compRenewalNumber.sendKeys("5");
		}

	}

	public void setUp_Comp_PolicyNumberAssignmentConfiguration_Descending() {
		boolean temp = false;
		List<String> expectedOrders = new ArrayList<String>();
		expectedOrders.add("6");
		expectedOrders.add("5");
		expectedOrders.add("4");
		expectedOrders.add("3");
		expectedOrders.add("2");
		expectedOrders.add("1");

		List<String> actualOrders = new ArrayList<String>();
		actualOrders.add(compCompany.getAttribute("value"));
		actualOrders.add(compCarrier.getAttribute("value"));
		actualOrders.add(compClient.getAttribute("value"));
		actualOrders.add(compPolicyType.getAttribute("value"));
		actualOrders.add(compShortPolicyNumber.getAttribute("value"));
		actualOrders.add(compRenewalNumber.getAttribute("value"));

		if (!expectedOrders.equals(actualOrders)) {
			temp = true;
		}

		if (temp) {
			compCompany.clear();
			compCompany.sendKeys("6");
			compCarrier.clear();
			compCarrier.sendKeys("5");
			compClient.clear();
			compClient.sendKeys("4");
			compPolicyType.clear();
			compPolicyType.sendKeys("3");
			compShortPolicyNumber.clear();
			compShortPolicyNumber.sendKeys("2");
			compRenewalNumber.clear();
			compRenewalNumber.sendKeys("1");
		}

	}
	

	
}
