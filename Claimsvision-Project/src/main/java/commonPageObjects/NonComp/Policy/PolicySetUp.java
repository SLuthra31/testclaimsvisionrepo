package commonPageObjects.NonComp.Policy;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.BaseClass;
import utilities.Browser;
import utilities.Wait;

public class PolicySetUp {

	WebDriver driver;
	BaseClass bc;
	String policySetUpWindow;

	public PolicySetUp(WebDriver driver) {
		this.driver = driver;
		bc = new BaseClass(driver);

	}

	/*
	 * WebElements of Buttons to perform action on Non Comp Policy Screen
	 */
	@FindBy(css = "#NonCompPolicy_addButton")
	public WebElement addPolicy;
	// WebElement of Validation Message Area
	@FindBy(css = "#NonCompPolicy_SaveValidationSummary")
	public WebElement messageArea;

	@FindBy(css = "#NonCompPolicy_messageLabel")
	public WebElement message;

	@FindBy(css = "input#NonCompPolicy_renewButton")
	public WebElement renewPolicy;

	@FindBy(css = "#NonCompPolicy_saveButton")
	public WebElement savePolicy;

	@FindBy(css = "#NonCompPolicy_endButton")
	public WebElement endorsePolicy;

	@FindBy(css = "#NonCompPolicy_cancelButton")
	public WebElement cancel;

	@FindBy(css = "#NonCompPolicy_autoDriversButton")
	public WebElement addDrivers;

	@FindBy(css = "#NonCompPolicy_itemsButton")
	public WebElement items;

	@FindBy(css = "input#NonCompPolicy_cancelPolicyButton")
	public WebElement cancelPolicy;

	@FindBy(css = "input#NonCompPolicy_reinstatePolicyButton")
	public WebElement reinstatePolicy;

	/*
	 * WebElements of Elements to be used while searching from Lookup
	 */
	// SearchButton
	@FindBy(css = "input#searchButton.defaultButton01")
	public WebElement searchButton;

	// SelectValue
	@FindBy(css = "#searchResultDataList_ctl01_TableCell1")
	public WebElement selectValue;

	/*
	 * WebElements of Elements to be filled to add Non Comp Policy
	 */

	// PolicyNumber
	@FindBy(css = "input#NonCompPolicy_NonCompPolicyNumberTextBox")
	public WebElement policyNumber;

	@FindBy(css = "#vendorSearchPager_panelPageNumber")
	public WebElement pageNumber;

	// RenewalVersion
	@FindBy(css = "input#NonCompPolicy_RenewalVersionTextBox")
	public WebElement renewalVersion;

	// InActive
	@FindBy(css = "input#NonCompPolicy_inactiveCheckBox")
	public WebElement inactive;

	// Company
	@FindBy(css = "select#NonCompPolicy_CompanyDropDown")
	public WebElement company;

	// SaveButtonHeader
	@FindBy(css = "input#saveClaimImageButton")
	public WebElement saveButtonHeader;

	// SearchCarrier
	@FindBy(css = "img#carrierImage")
	public WebElement searchCarrier;

	// Carrier
	@FindBy(css = "input#NonCompPolicy_carrierTextBox")
	public WebElement carrier;

	// SearchClient
	@FindBy(css = "img#entryClientImage")
	public WebElement searchClient;

	// PolicyType
	@FindBy(css = "select#NonCompPolicy_policyTypeDropDown")
	public WebElement policyType;

	// EffectiveDate
	@FindBy(css = "#NonCompPolicy_effectiveDateTextBox")
	public WebElement effectiveDate;

	// EffectiveDateCalendar
	@FindBy(css = "#NonCompPolicy_effectiveDateImage")
	public WebElement effectiveDateCalendar;

	// Calendar
	@FindBy(css = "#calendar")
	public WebElement calendar;

	@FindBy(css = "#searchResultDataList_ctl06_returnSelectImage")
	public WebElement fifthMenuId;

	@FindBy(css = "td#searchResultDataList_ctl06_VendorNameCell2")
	public WebElement fifthMenuName;

	@FindBy(css = "input#conditionTextBox")
	public WebElement searchTextBox;
	// today
	@FindBy(css = "#lblToday")
	public WebElement todayDate;

	// ExpirationDate
	@FindBy(css = "#NonCompPolicy_expirationDateTextBox")
	public WebElement expirationDate;

	// TransactionType
	@FindBy(css = "input#NonCompPolicy_TransactionTypeTextBox")
	public WebElement transactionType;

	@FindBy(css = "input#NonCompPolicy_clientNameTextBox")
	public WebElement client;

	@FindBy(css = "input#NonCompPolicy_carrierTextBox")
	public WebElement carrierTextBox;
	// TransactionType
	@FindBy(css = "input#NonCompPolicy_VersionEffectiveDateTextBox")
	public WebElement versionEffDate;

	// TransactionType
	@FindBy(css = "input#NonCompPolicy_TransactionVersionTextBox")
	public WebElement transactionVersion;

	/*
	 * Insured Details fields
	 */
	// InsureIsClient
	@FindBy(css = "input#NonCompPolicy_insuredIsClientCheckBox")
	public WebElement insuredIsClient;
	
	@FindBy(css="input#NonCompPolicy_ContinueButton")
	public WebElement continueButton;
	// InsuredName
	@FindBy(css = "input#NonCompPolicy_insuredNameTextBox")
	public WebElement insuredName;

	// DBAName
	@FindBy(css = "input#NonCompPolicy_dbaNameTextBox")
	public WebElement dBAName;

	// Contact
	@FindBy(css = "#input#NonCompPolicy_contactTextBox")
	public WebElement contact;

	// ForeignAddress
	@FindBy(css = "input#NonCompPolicy_isForeignAddCheckBox")
	public WebElement foreignAddress;

	/*
	 * If Foreign Address is not checked following fields displayed
	 */
	// StreetLine1
	@FindBy(css = "input#NonCompPolicy_streetLine1TextBox")
	public WebElement streetLine1;

	// StreetLine2
	@FindBy(css = "input#NonCompPolicy_streetLine2TextBox")
	public WebElement streetLine2;

	// City
	@FindBy(css = "input#NonCompPolicy_cityTextBox")
	public WebElement city;

	@FindBy(css = "td#searchResultDataList_ctl01_TableCell1")
	public WebElement firstCell;

	// State
	@FindBy(css = "select#NonCompPolicy_stateDropDown")
	public WebElement state;

	// Postal
	@FindBy(css = "input#NonCompPolicy_zipTextBox")
	public WebElement postal;

	// County
	@FindBy(css = "input#NonCompPolicy_countyTextBox")
	public WebElement county;

	/*
	 * If Foreign Address is checked following fields displayed
	 */
	// ForeignAddress1
	@FindBy(css = "input#NonCompPolicy_foreignAddressTextBox")
	public WebElement foreignAddress1;

	// StreetLine2
	@FindBy(css = "input#NonCompPolicy_foreignAddress2TextBox")
	public WebElement foreignAddress2;

	// City
	@FindBy(css = "input#NonCompPolicy_ForeignCityTextBox")
	public WebElement foreignCity;

	// State
	@FindBy(css = "input#NonCompPolicy_ForeignStateTextBox")
	public WebElement foreignState;

	// Postal
	@FindBy(css = "input#NonCompPolicy_ForeignZipTextBox")
	public WebElement foreignZip;

	// County
	@FindBy(css = "input#NonCompPolicy_ForeignCountyTextBox")
	public WebElement foreignCounty;

	// Country
	@FindBy(css = "input#NonCompPolicy_ForeignCountryTextBox")
	public WebElement foreignCountry;

	// Phone
	@FindBy(css = "input#NonCompPolicy_phoneTextBox")
	public WebElement phone;

	// Fax
	@FindBy(css = "input#NonCompPolicy_faxTextBox")
	public WebElement fax;

	// Email
	@FindBy(css = "input#NonCompPolicy_emailTextBox")
	public WebElement email;

	/*
	 * Policy Details Elements
	 */
	// ShortPolicyNumber
	@FindBy(css = "input#NonCompPolicy_ShortPolicyNumberTextBox")
	public WebElement shortPolicyNumber;

	// Premium
	@FindBy(css = "input#NonCompPolicy_premiumTextBox")
	public WebElement premium;

	// Deductible
	@FindBy(css = "input#NonCompPolicy_deductibleTextBox")
	public WebElement deductible;

	// Agency
	@FindBy(css = "input#NonCompPolicy_agencyTextBox")
	public WebElement agency;

	// Agent
	@FindBy(css = "#NonCompPolicy_agentImageButton")
	public WebElement agentSearch;

	// rightArrow
	@FindBy(css = "#vendorSearchPager_lbtnNext")
	public WebElement rightArrow;

	// table
	@FindBy(css = "#searchResultDataList")
	public WebElement table;

	// PolicyYear
	@FindBy(css = "input#NonCompPolicy_policyYearTextBox")
	public WebElement policyYear;

	// AltPolicy#
	@FindBy(css = "input#NonCompPolicy_AltPolicyNumberTextBox")
	public WebElement altPolicyNumber;

	// AltPolicy#2
	@FindBy(css = "input#NonCompPolicy_AltPolicyNumber2TextBox")
	public WebElement altPolicyNumber2;

	// DomicileState
	@FindBy(css = "select#NonCompPolicy_domicileStateDropdown")
	public WebElement domicileState;

	// Occurrence
	@FindBy(css = "input#NonCompPolicy_perRadioButtonList_0")
	public WebElement occurrence;

	// ClaimMade
	@FindBy(css = "input#NonCompPolicy_perRadioButtonList_1")
	public WebElement claimMade;

	// RetroActive Date
	@FindBy(css = "input#NonCompPolicy_RetroActiveDateTextBox")
	public WebElement retroActiveDate;

	// AllowCoverages
	@FindBy(css = "input#NonCompPolicy_IsAllowCoverageModificationCheckBox")
	public WebElement allowCoverages;

	// BuybackPolicy
	@FindBy(css = "#NonCompPolicy_isBuyBackCheckbox")
	public WebElement buyBack;

	// AggregateLimits
	@FindBy(css = "#NonCompPolicy_policyAggregateLimitsImage")
	public WebElement policyAggregateLimits;

	// Items
	@FindBy(css = "#NonCompPolicy_policyItemsImage")
	public WebElement itemsOnPolicy;

	/*
	 * WebElements of Search Policy
	 */
	@FindBy(css = "#NonCompPolicy_filterEffectiveFromDtImage")
	public WebElement effectiveFromDate;

	@FindBy(css = "#NonCompPolicy_filterEffectiveThruDtImage")
	public WebElement effectiveThruDate;

	@FindBy(css = "input#NonCompPolicy_filterPolicyNumberTextBox")
	public WebElement policyNumberFilter;

	@FindBy(css = "#NonCompPolicy_filterExpirationFromDtImage")
	public WebElement expirationFromDate;

	@FindBy(css = "#NonCompPolicy_filterExpirationThruDtImage")
	public WebElement expirationThruDate;

	@FindBy(css = "input#NonCompPolicy_filterPolicyYearTextBox")
	public WebElement policyYearFilter;

	@FindBy(css = "#NonCompPolicy_searchClientImage")
	public WebElement searchClientFilter;

	@FindBy(css = "input#searchButton")
	public WebElement searchButtonFliter;

	@FindBy(css = "#searchResultDataList_ctl01_TableCell1")
	public WebElement selectSearchClient;

	@FindBy(css = "select#NonCompPolicy_filterPolicyTypeDropDown")
	public WebElement policyTypeFilter;

	@FindBy(css = "input#NonCompPolicy_clearButton")
	public WebElement clearAllButton;

	@FindBy(css = "input#NonCompPolicy_searchButton")
	public WebElement searchPolicyFilter;

	@FindBy(css = "#searchResultDataList_ctl01_TableCell1")
	public WebElement firstMenuItem;

	public void clickOnAddButton() {

		addPolicy.click();
	}

	public void getBackToPolicySetUp() {
		driver.switchTo().window(policySetUpWindow);
	}

	public void getWindowForPolicySetUp() {
		policySetUpWindow = driver.getWindowHandle();
	}

	public void clickSearchButton() {
		searchPolicyFilter.click();
	}

	// Method to select Carrier Value
	public String selectCarrierValue() throws InterruptedException {

		Wait.waitFor(3);
		String parent = driver.getWindowHandle();
		searchCarrier.click();
		Wait.waitFor(2);
		bc.switchToNextWindow("Search Client");
		Wait.waitFor(2);

		searchButton.click();
		Wait.waitFor(3);
		Wait.modifyWait(driver, firstCell);

		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> carrierIcon = new ArrayList<WebElement>();
		List<WebElement> carrierName = new ArrayList<WebElement>();
		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.tagName("td"));
			carrierIcon.add(columns.get(0));
			carrierName.add(columns.get(1));

		}

		int randomCount = BaseClass.generateRandomNumberAsInteger(carrierIcon.size());
		String temp = carrierName.get(randomCount).getText();
		carrierIcon.get(randomCount).click();
		driver.switchTo().window(parent);
		// driver.switchTo().window(windows.get(0));
		Wait.waitFor(6);
		return temp;

	}

	// Method to select client values
	public String selectClientValue() throws InterruptedException {

		Wait.waitFor(3);
		String parent = driver.getWindowHandle();
		searchClient.click();
		Wait.waitFor(2);

		bc.switchToNextWindow("Search Client");

		Wait.waitFor(2);

		searchButton.click();
		Wait.waitFor(3);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(firstCell));

		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> clientIcon = new ArrayList<WebElement>();
		List<WebElement> clientName = new ArrayList<WebElement>();
		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.tagName("td"));
			clientIcon.add(columns.get(0));
			clientName.add(columns.get(1));

		}
		int randomCount = BaseClass.generateRandomNumberAsInteger(rows.size());
		String temp = clientName.get(randomCount).getText();
		clientIcon.get(randomCount).click();
		driver.switchTo().window(parent);
		//
		Wait.waitFor(3);
		return temp;

	}

	// Method to fetch all validation messages
	public List<String> fetchAllValidationMessages() {
		List<String> allMessages = new ArrayList<String>();
		List<WebElement> allValidationMessages = messageArea.findElements(By.tagName("li"));
		for (WebElement validation : allValidationMessages) {
			allMessages.add(validation.getText());
		}
		return allMessages;
	}

	// Method to fetch all validation messages
	public List<String> fetchAllGeneralMessages() {
		List<String> allMessages = new ArrayList<String>();
		List<WebElement> allValidationMessages = message.findElements(By.tagName("li"));
		for (WebElement validation : allValidationMessages) {
			allMessages.add(validation.getText());
		}
		return allMessages;
	}

	public String fetchMessages() {
		String text = message.getText();
		return text;
	}

	public void clickSaveOnClaimMade() {
		this.claimMade.click();
		
	}

	// Click on Renew Non Comp Policy Button
	public void clickOnRenewNonCompPolicyButton() {
		this.renewPolicy.click();
	}

	// Click on Save button to Save Policy Changes
	public void clickOnSaveButton() throws InterruptedException {
		Wait.modifyWait(driver, savePolicy);
		this.savePolicy.click();
	}

	// Click on Endorse button to endorse a Policy
	public void clickOnEndorseButton() {
		this.endorsePolicy.click();
	}

	// Click on Cancel button to un-save changes made
	public void clickOnCancelButton() {
		this.cancel.click();
	}

	// Click on Add Drivers button to add drivers
	public void clickOnAddDriversButton() {
		this.addDrivers.click();
	}

	// Click on Items button
	public void clickOnItemsButton() {
		this.items.click();
	}

	// Click on Cancel Policy button to Cancel the policy
	public void clickOnCancelPolicyButton() {
		this.cancelPolicy.click();
	}

	// Click on Reinstate Policy button to re-instate the cancelled policy
	public void clickOnReinstatePolicyButton() {
		this.reinstatePolicy.click();
	}

	// Method to fetch all validation messages
	public void selectCompany() {
		Select s = new Select(company);
		List<String> arryList = new ArrayList<String>(Arrays.asList("HOME OFFICE", "CANADA"));
		Random random = new Random();
		int rnd = random.nextInt(arryList.size());
		System.out.println("rnd-->" + rnd);
		String arrVal = arryList.get(rnd).toString();
		System.out.println("val-->" + arrVal);
		// s.selectByValue(arrVal);
		s.selectByVisibleText(arrVal);
	}

	public void selectCompanyByText(String value) throws InterruptedException {
		Wait.modifyWait(driver, company);
		Select s = new Select(company);
		s.selectByVisibleText(value);
	}

	public void selectCarrierValueThroughText(String value) throws InterruptedException {
		Wait.waitFor(3);
		String parent = driver.getWindowHandle();
		searchCarrier.click();
		Wait.waitFor(2);
		bc.switchToNextWindow("Search Client");
		Wait.waitFor(2);
		searchTextBox.sendKeys(value);
		Wait.waitFor(3);
		searchButton.click();
		Wait.waitFor(3);
		Wait.modifyWait(driver, firstCell);
		firstCell.click();
		driver.switchTo().window(parent);

	}

	public void selectClientValueThroughText(String value) throws InterruptedException {
		Wait.waitFor(3);
		String parent = driver.getWindowHandle();
		searchClient.click();
		Wait.waitFor(2);
		bc.switchToNextWindow("Search Client");
		Wait.waitFor(2);
		searchTextBox.sendKeys(value);
		Wait.waitFor(3);
		searchButton.click();
		Wait.waitFor(3);
		Wait.modifyWait(driver, firstCell);
		firstCell.click();
		driver.switchTo().window(parent);

	}

	public void selectPolicyTypeThroughText(String value) throws InterruptedException {
		Wait.modifyWait(driver, policyType);
		Select s = new Select(policyType);
		s.selectByVisibleText(value);
	}

	public String fetchCompany() {

		Select s = new Select(company);
		return s.getFirstSelectedOption().getText();
	}

	public void selectPolicyType() throws InterruptedException {
		Thread.sleep(2000);
		Select s = new Select(policyType);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}

	public void enterEffectiveDateAndExpirationDate() throws ParseException, InterruptedException {

		Thread.sleep(3000);
		String date = BaseClass.generateEffectiveDate();
		effectiveDate.sendKeys(date);
		expirationDate.sendKeys(BaseClass.generateExpirationDate(date));

	}

	public void enterIncorrectExpirationDate() throws ParseException {

		String date = BaseClass.generateEffectiveDate();
		effectiveDate.sendKeys(date);
		expirationDate.sendKeys(BaseClass.generateExpDateLessThanEffDate(date));

	}

	public void enterRetroActiveDate() throws ParseException {

		String date = BaseClass.generateEffectiveDate();
		retroActiveDate.sendKeys(date);

	}

	public void enterExpDateSameAsEffDate() throws ParseException {

		String date = BaseClass.generateEffectiveDate();
		effectiveDate.sendKeys(date);
		expirationDate.sendKeys(date);

	}

	public void checkInsuredIsClient() {
		insuredIsClient.click();
	}

	public String enterShortPolicyNumber() {
		shortPolicyNumber.clear();
		String temp = BaseClass.enterRandomNumber(4);
		shortPolicyNumber.sendKeys(temp);
		return temp;
	}

	public void enterPremium() {
		premium.sendKeys(BaseClass.enterRandomNumber(2));
	}

	public void enterDeductible() {
		deductible.sendKeys(BaseClass.enterRandomNumber(2));
	}

	public void enterAgency() {
		agency.sendKeys(BaseClass.stringGeneratorl(4));
	}

	public void enterInvalidDataInPostal() {
		postal.sendKeys(BaseClass.stringGeneratorl(7));
	}

	public void enterInvalidDataInPhone() {
		phone.sendKeys(BaseClass.stringGeneratorl(7) + BaseClass.enterRandomNumber(3));
	}

	public void enterInvalidDataInFax() {
		fax.sendKeys(BaseClass.stringGeneratorl(7) + BaseClass.enterRandomNumber(3));
	}

	public void enterInvalidDataInEmail() {
		email.sendKeys(BaseClass.stringGeneratorl(10));
	}

	public void enterInvalidDataInPremium() {
		premium.sendKeys(BaseClass.stringGeneratorl(5));
	}

	public void enterInvalidDataInDeductible() {
		deductible.sendKeys(BaseClass.stringGeneratorl(5));
	}

	public void enterInvalidDataInPolicyYear() {
		policyYear.sendKeys(BaseClass.stringGeneratorl(4));
	}

	public String enterAgent() throws InterruptedException {

		Wait.waitFor(3);
		String parent = driver.getWindowHandle();
		agentSearch.click();
		Wait.waitFor(3);
		Wait.waitFor(2);
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> iterate = windows.iterator();
		while (iterate.hasNext()) {
			String childWindow = iterate.next();

			if (!parent.equals(childWindow)) {
				driver.switchTo().window(childWindow);
			}
		}
		// ArrayList<String> windows = new
		// ArrayList<String>(driver.getWindowHandles());
		// driver.switchTo().window(windows.get(1));
		Wait.waitFor(3);
		searchButton.click();
		Wait.waitFor(5);

		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> vendorIcon = new ArrayList<WebElement>();
		List<WebElement> vendorName = new ArrayList<WebElement>();
		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.tagName("td"));
			vendorIcon.add(columns.get(0));
			vendorName.add(columns.get(5));

		}
		int randomCount = BaseClass.generateRandomNumberAsInteger(9);
		String temp = vendorName.get(randomCount).getText();
		vendorIcon.get(randomCount).click();
		driver.switchTo().window(parent);
		// driver.switchTo().window(windows.get(0));
		Wait.waitFor(3);
		return temp;

	}

	public void enterPolicyYear() {
		policyYear.sendKeys(BaseClass.generatePolicyYear());
	}

	public void enterAltPolicy1() {
		altPolicyNumber.sendKeys(BaseClass.enterRandomNumber(4));
	}

	public void enterAltPolicy2() {
		altPolicyNumber2.sendKeys(BaseClass.enterRandomNumber(4));
	}

	public void enterDomicileState() {
		Select s = new Select(domicileState);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}

	public void selectingAllowCoveragesCheckbox() {
		allowCoverages.click();
	}

	public void selectingBuyBackPolicyCheckbox() {
		buyBack.click();

	}

	// -----------------------------------------------------------//
	@FindBy(css = "a#adminTreeViewn16")
	public WebElement customization;

	// Web Elements of PolicyNumberAssignment link
	@FindBy(css = "a#adminTreeViewt32")
	public WebElement policyNumberAssignment;

	public boolean checkPolicyFieldIsDisabled() {
		boolean temp = true;
		if (policyNumber.isEnabled()) {
			temp = false;
		}
		return temp;
	}

	public boolean checkTransactionVersionFieldIsDisabled() {
		boolean temp = true;
		if (transactionVersion.isEnabled()) {
			temp = false;
		}
		return temp;
	}

	public boolean checkRenewalVersionFieldIsDisabled() {
		boolean temp = true;
		if (renewalVersion.isEnabled()) {
			temp = false;
		}
		return temp;
	}

	public boolean checkTransactionTypeFieldIsDisabled() {
		boolean temp = true;
		if (transactionType.isEnabled()) {
			temp = false;
		}
		return temp;
	}

	public boolean checkVersionEffectiveTypeFieldIsDisabled() {
		boolean temp = true;
		if (transactionType.isEnabled()) {
			temp = false;
		}
		return temp;
	}

	public void clickOnForeignAddressCheckBox() {
		foreignAddress.click();
	}

	@FindBy(css = "a#NonCompPolicy_NonCompPolicySearchPager_lbtnLast")
	public WebElement lastSearchResultButton;

	@FindBy(css = "div#NonCompPolicy_foreignAddressDiv")
	public WebElement foreignAddressDiv;
	@FindBy(css = "Span#NonCompPolicy_CustomValidatorForeignAddress1")
	public WebElement foreignAddress2Div;

	@FindBy(css = "Span#NonCompPolicy_CustomValidatorForeignAddress1")
	public WebElement insuredAddressFields;

	@FindBy(css = "input#NonCompPolicy_SpecialAnalysisPolicyFields_19Field")
	public WebElement policyUserDefinedFields1;

	@FindBy(css = "input#NonCompPolicy_SpecialAnalysisPolicyFields_20Field")
	public WebElement policyUserDefinedFields2;
	@FindBy(css = "input#NonCompPolicy_SpecialAnalysisPolicyFields_21Field")
	public WebElement policyUserDefinedFields3;

	@FindBy(css = "select#NonCompPolicy_SpecialAnalysisPolicyFields_22Field")
	public WebElement policyUserDefinedFields4;

	@FindBy(css = "input#NonCompPolicy_SpecialAnalysisPolicyFields_23Field")
	public WebElement policyUserDefinedFields5;

	@FindBy(css = "table#NonCompPolicy_NonCompPolicyGridView")
	public WebElement searchGrid;

	public void enterValuesInForeignAddressFields() {
		foreignZip.sendKeys(BaseClass.enterRandomNumber(5));
		foreignAddress1.sendKeys(BaseClass.stringGeneratorl(5));
		foreignAddress2.sendKeys(BaseClass.stringGeneratorl(5));
		foreignCity.sendKeys(BaseClass.stringGeneratorl(5));
		foreignCountry.sendKeys(BaseClass.stringGeneratorl(5));
		foreignCounty.sendKeys(BaseClass.stringGeneratorl(5));
		foreignState.sendKeys(BaseClass.stringGeneratorl(2));
	}

	public void enterValuesInPolicyUserDefinedField1() {
		policyUserDefinedFields1.sendKeys(BaseClass.stringGeneratorl(5));
	}

	public void enterValuesInPolicyUserDefinedField2() {
		policyUserDefinedFields2.sendKeys(BaseClass.enterRandomNumber(5));
	}

	public void enterValuesInPolicyUserDefinedField3() {
		policyUserDefinedFields3.sendKeys(BaseClass.generateEffectiveDate());
	}

	public void enterValuesInPolicyUserDefinedField4() {
		Select s = new Select(policyUserDefinedFields4);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}

	public void enterValuesInPolicyUserDefinedField5() {
		policyUserDefinedFields5.sendKeys(BaseClass.enterRandomNumber(3));
	}

	public String enterEffectiveDate() throws ParseException {

		String date = BaseClass.generateEffectiveDate();
		effectiveDate.sendKeys(date);
		return date;

	}

	public void enterExpirationDate() throws ParseException {

		expirationDate.sendKeys(BaseClass.generateEffectiveDate());

	}

	public boolean checkInsuredNameFieldIsEnabled() {

		boolean temp = true;
		List<WebElement> fieldsets = driver.findElements(By.tagName("fieldset"));
		WebElement insuredDetailsBox = fieldsets.get(2);
		List<WebElement> fields = insuredDetailsBox.findElements(By.tagName("input"));
		for (WebElement field : fields) {
			if (!field.isEnabled()) {
				temp = false;
				break;

			}
		}
		return temp;

	}

	public void goToLastSearchResultScreen() {
		lastSearchResultButton.click();
	}

	public String fetchLatestPolicyNumber() {
		List<WebElement> policies = searchGrid.findElements(By.tagName("tr"));
		WebElement latestPolicy = policies.get(policies.size() - 1);

		List<WebElement> latestPolicyNo = latestPolicy.findElements(By.tagName("td"));
		String latestPolicyNumber = latestPolicyNo.get(0).getText();
		latestPolicyNo.get(latestPolicyNo.size() - 1).click();
		return latestPolicyNumber;
	}

	public String fetchLatestEffectiveDate() {
		List<WebElement> policies = searchGrid.findElements(By.tagName("tr"));
		WebElement latestPolicy = policies.get(policies.size() - 1);

		List<WebElement> latestPolicyNo = latestPolicy.findElements(By.tagName("td"));
		String latestEffectiveDate = latestPolicyNo.get(3).getText();

		return latestEffectiveDate;
	}

	public String fetchLatestExpirationDate() {
		List<WebElement> policies = searchGrid.findElements(By.tagName("tr"));
		WebElement latestPolicy = policies.get(policies.size() - 1);

		List<WebElement> latestPolicyNo = latestPolicy.findElements(By.tagName("td"));
		String latestExpirationDate = latestPolicyNo.get(4).getText();

		return latestExpirationDate;
	}

	public String fetchShortPolicyNumber() {
		String temp = shortPolicyNumber.getAttribute("value");
		return temp;

	}

	public String fetchEffectiveDate() {
		String temp = effectiveDate.getAttribute("value");
		return temp;

	}

	public String fetchExpirationDate() {
		String temp = expirationDate.getAttribute("value");
		return temp;

	}

	public String fetchRenewalVers() {
		String temp = renewalVersion.getAttribute("value");
		return temp;

	}

	public String fetchClient() {
		return client.getAttribute("value");
	}

	public String fetchCarrier() {
		return carrier.getAttribute("value");
	}

	public String getCarrierCode(String CarrierName) {

		switch (CarrierName) {
		case "NORTHBROOK EXCESS & SURPLUS CO": {
			return "NS";

		}

		case "ALLSTATE CANADA - COMPANY 90": {

			return "AL";

		}

		case "PAFCO - COMPANY 93": {

			return "PA";

		}

		case "PAFCO WARRANTY - COMPANY 264": {

			return "PW";

		}
		}
		return CarrierName;

	}

	public String getPolicyCode(String policyName) {
		switch (policyName) {
		case "CASUALTY": {
			return "CA";

		}

		case "PROPERTY": {

			return "PR";

		}

		case "MISCELLANEOUS OTHER": {

			return "HA";

		}

		case "COMMERCIAL": {

			return "CO";

		}
		case "AUTO": {

			return "AU";

		}

		}
		return policyName;
	}

	public String fetchPolicyType() {
		Select s = new Select(policyType);
		return s.getFirstSelectedOption().getText();
	}

	public void enterSameEffectiveDate(String date) {
		effectiveDate.sendKeys(date);
	}

	public void enterSameExpirationDate(String date) {
		expirationDate.sendKeys(date);
	}

	public void selectContinueButton() throws InterruptedException {
		
		Wait.modifyWait(driver, continueButton);
		continueButton.click();
	}

	public String fetchRenewalVersion() {
		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 2);
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * cssSelector("input#NonCompPolicy_RenewalVersionTextBox")));
		 */
		String temp = renewalVersion.getAttribute("value");
		return temp;
	}

	public String fetchTransactionType() {
		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 2);
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * cssSelector("input#NonCompPolicy_TransactionTypeTextBox")));
		 */
		String temp = transactionType.getAttribute("value");
		return temp;
	}

	public String fetchVersionEffectiveDate() {
		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 2);
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * cssSelector("input#NonCompPolicy_VersionEffectiveDateTextBox")));
		 */
		String temp = versionEffDate.getAttribute("value");
		return temp;
	}

	public String fetchTransactionVersion() {
		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 2);
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * cssSelector("input#NonCompPolicy_TransactionVersionTextBox")));
		 */
		String temp = transactionVersion.getAttribute("value");
		return temp;
	}

	public void clickSaveButtonFromHeader() {
		// Actions action = new Actions(driver);
		// action.moveToElement(saveButtonHeader).doubleClick();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(saveButtonHeader));
		// JavascriptExecutor executor = (JavascriptExecutor) driver;
		// executor.executeScript ("arguments[0].click();" , saveButtonHeader);
		saveButtonHeader.click();
		Wait.waitFor(5);

	}

	public String getCompanyCode() {
		String companyCode;
		Select s = new Select(company);
		if (s.getFirstSelectedOption().getText().equals("HOME OFFICE")) {
			companyCode = "HO";
			return companyCode;
		} else if (s.getFirstSelectedOption().getText().equals("CANADA")) {
			companyCode = "CA";
			return companyCode;
		} else {
			return null;
		}
	}

	public String getCarrierCode() {
		String carrierCode = "";
		System.out.println("carrier val-->" + carrier.getAttribute("value"));
		if (carrier.getAttribute("value").equals("NORTHBROOK EXCESS & SURPLUS CO")) {
			carrierCode = "NS";
			return carrierCode;
		} else if (carrier.getAttribute("value").equals("ALLSTATE CANADA - COMPANY 90")) {
			carrierCode = "AL";
			return carrierCode;
		} else if (carrier.getAttribute("value").equals("PAFCO - COMPANY 93")) {
			carrierCode = "PA";
			return carrierCode;
		} else if (carrier.getAttribute("value").equals("PAFCO WARRANTY - COMPANY 264")) {
			carrierCode = "PW";
			return carrierCode;
		}

		else {
			return null;
		}
	}

	public String getPolicyTypeCode() {

		String policyTypeCode;
		Select s = new Select(policyType);
		if (s.getFirstSelectedOption().getText().equals("CASUALTY")) {
			policyTypeCode = "CA";
			return policyTypeCode;
		} else if (s.getFirstSelectedOption().getText().equals("PROPERTY")) {
			policyTypeCode = "PR";
			return policyTypeCode;
		} else if (s.getFirstSelectedOption().getText().equals("MISCELLANEOUS OTHER")) {
			policyTypeCode = "HA";
			return policyTypeCode;
		} else if (s.getFirstSelectedOption().getText().equals("COMMERCIAL")) {
			policyTypeCode = "CO";
			return policyTypeCode;
		} else if (s.getFirstSelectedOption().getText().equals("AUTO")) {
			policyTypeCode = "AU";
			return policyTypeCode;
		}

		else {
			return null;
		}
	}

	public String getExpectedPolicyNumber() {

		String companyCode = getCompanyCode();
		String carrierCode = getCarrierCode();
		String policyTypeCode = getPolicyTypeCode();
		String shortPolicyNumber = fetchShortPolicyNumber();
		String renewalVers = fetchRenewalVers();
		String policyNum = companyCode + "-" + carrierCode + "-" + policyTypeCode + "-" + shortPolicyNumber + "-"
				+ renewalVers;
		return policyNum;
	}

	public void refresh() {
		Browser.refresh(driver);

		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (NoAlertPresentException e) {

			e.printStackTrace();
		}
	}

}
