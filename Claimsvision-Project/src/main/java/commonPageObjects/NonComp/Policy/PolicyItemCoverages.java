package commonPageObjects.NonComp.Policy;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import utilities.BaseClass;
import utilities.Wait;

public class PolicyItemCoverages extends PolicyItem{

	WebDriver driver;
	BaseClass bc;

	public PolicyItemCoverages(WebDriver driver) {
		super(driver);
		this.driver = driver;
		bc=new BaseClass(driver);

	}
	
	@FindBy(css="input#itemNameTextBox")
	public WebElement ItemName;
	
	@FindBy(css="img#policyCoverageImage")
	public WebElement coverageIcon;
	
	@FindBy(css="select#coverageCategoryDropDown")
	public WebElement coverageCategoryDropDown;

	@FindBy(css="input#deductibleRadioButtonList_0")
	public WebElement deductibleRadioButton;

	@FindBy(css="input#deductibleRadioButtonList_1")
	public WebElement sirRadioButton;

	@FindBy(css="table#CoverageGridView")
	public WebElement coverageTable;
	
	@FindBy(css="input#deductibleAmountTextBox")
	public WebElement deductibleTextBox;

	@FindBy(css="input#limitPerClaimTextBox")
	public WebElement limitPerPersonextBox;

	@FindBy(css="input#limitPerOccurrenceTextBox")
	public WebElement limitPerOccurrenceTextBox;

	@FindBy(css="input#claimReportingLevelTextBox")
	public WebElement claimReportingLevelTextBox;

	@FindBy(css="input#erodeAggLimitRadioButtonList_0")
	public WebElement erodeAggLimitRadioButton_Yes;

	@FindBy(css="input#erodeAggLimitRadioButtonList_1")
	public WebElement erodeAggLimitRadioButtonList_No;

	@FindBy(css="input#premiumTextBox")
	public WebElement premiumTextBox;
	
	
	@FindBy(css="input#addButton")
	public WebElement addButton;

	@FindBy(css="input#deleteButton")
	public WebElement deleteButton;

	@FindBy(css="input#saveButton")
	public WebElement saveButton;

	@FindBy(css="input#saveButton")
	public WebElement cancelButton;

	@FindBy(css="input#closeButton")
	public WebElement closeButton;
	
	public void clickOnCoverageIcon()
	{
		coverageIcon.click();
	}
	public void switchToCoverageScreen()
	{
		bc.switchToNextWindow("Non Comp Policy Item Coverages");
	}
	
	public String selectCoverage(int count)
	{
		Select s = new Select(coverageCategoryDropDown);
		s.selectByIndex(count);
		Wait.waitFor(2);
		return s.getFirstSelectedOption().getText();
	}
	public void enterInvalidDataInDeductible()
	{
		deductibleTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}
	public void enterDataInDeductible()
	{
		deductibleTextBox.sendKeys(BaseClass.enterRandomNumber(4));
	}
	
	public void enterInvalidDataInLimitPerPerson()
	{
		limitPerPersonextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}
	public void enterDataInLimitPerPerson()
	{
		limitPerPersonextBox.sendKeys(BaseClass.enterRandomNumber(4));
	}
	
	public void enterInvalidDataInLimitPerOccurence()
	{
		limitPerOccurrenceTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}
	public void enterDataInLimitPerOccurence()
	{
		limitPerOccurrenceTextBox.sendKeys(BaseClass.enterRandomNumber(4));
	}
	public void enterInvalidDataInClaimReportingLevel()
	{
		claimReportingLevelTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}
	public void enterDataInClaimReportingLevel()
	{
		claimReportingLevelTextBox.sendKeys(BaseClass.enterRandomNumber(4));
	}
	public void enterInvalidDataIPremium()
	{
		premiumTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}
	
	public void enterDataMoreThanLimitInDeductible()
	{
		deductibleTextBox.sendKeys("9876543219876543");
	}
	
	public void enterDataMoreThanLimitInLimitPerPerson()
	{
		limitPerPersonextBox.sendKeys("9876543219876543");
	}
	public void enterDataInPremium()
	{
		premiumTextBox.sendKeys(BaseClass.enterRandomNumber(4));
	}
	public String fetchCoveragesFromTable(int index)
	{
		List<WebElement> rows = coverageTable.findElements(By.tagName("tr"));
		WebElement latestCoverageRow = rows.get(rows.size() - 1);

		List<WebElement> latestCoverageColumns = latestCoverageRow.findElements(By.tagName("td"));
		String latestCoverage = latestCoverageColumns.get(index).getText();
		return latestCoverage;
	}
	
	public String fetchCoverage()
	{
		Select s = new Select(coverageCategoryDropDown);
		return s.getFirstSelectedOption().getText();
	}
	
	public String fetchItemName()
	{
		return ItemName.getAttribute("value");
	}
	
	
	public void clickCloseButtonInCoveregeScreen(){

//		String parentWindow = iterate.next();
//		String item = iterate.next();
//		String coverage = iterate.next();

	closeButton.click();
	
	
		
//		driver.close();
		
		 
//		  try{
//		   Alert alert = driver.switchTo().alert();
//		   alert.accept();
//		  }
//		  catch(NoAlertPresentException e)
//		  {
//		   
//		   e.getMessage();
//		  }

		//driver.switchTo().window(item);
		
			
		
	}

	}
