package commonPageObjects.NonComp.Policy;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import testScripts.ALLSTATE.Policy.AddPolicy.TestAggregateLimitValidations;
import testScripts.ALLSTATE.Policy.AddPolicy.TestPolicyAggregateLimit;
import utilities.BaseClass;
import utilities.Browser;

public class PolicyAggregateLimit extends PolicyItem{

	WebDriver driver;

	public PolicyAggregateLimit(WebDriver driver) {
		super(driver);
		this.driver = driver;

	}

	@FindBy(css = "img#NonCompPolicy_policyAggregateLimitsImage")
	public WebElement addAggregateLimit;

	@FindBy(css = "select#aggregateLimitCategoryDropDown")
	public WebElement aggregateLimitCategoryDropDown;

	@FindBy(css = "input#LimitTextBox")
	public WebElement limitTextBox;

	@FindBy(css = "input#effectiveDateTextBox")
	public WebElement effectiveDateTextBox;

	@FindBy(css = "input#expirationDateTextBox")
	public WebElement expirationDateTextBox;

	@FindBy(css = "div#ValidationSummary1")
	public WebElement validationMessageArea;
	
	@FindBy(css = "span#noSearchResultLabel")
	public WebElement validationMessageAreaForNoSearchResults;

	@FindBy(css="span#messageLabel")
	public WebElement validationMessage;

	
	@FindBy(css = "table#aggregateLimitsGridView")
	public WebElement aggregateLimitTable;

	@FindBy(css = "input[alt='Edit']")
	public WebElement modify;

	public void clickOnAggregaeIcon() {
		addAggregateLimit.click();
	}

	public void switchToAggregateScreen() {
		String parent = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> iterate = windows.iterator();
		while (iterate.hasNext()) {
			String childWindow = iterate.next();

			if (!parent.equals(childWindow)) {
				driver.switchTo().window(childWindow);

			}
		}
	}

	public void clickOnAddButton() {
		addButton.click();
	}

	public void clickOnModifyButton() {
		
		modify.click();
	}

	public void enterValueInLimit() {
		limitTextBox.clear();

		limitTextBox.sendKeys(BaseClass.enterRandomNumber(4));
	}

	public void enterEffectiveAndExpiraitonDate() throws ParseException {

		effectiveDateTextBox.sendKeys(TestAggregateLimitValidations.effectiveDate);
		expirationDateTextBox.sendKeys(TestAggregateLimitValidations.expirationDate);
	}
	public void enterEffectiveDateAndExpirationDateOutsiePolicyEffectiveDate() throws ParseException
	{
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date randomDate = sdf.parse(TestAggregateLimitValidations.effectiveDate);
		String oneMoreDate = TestAggregateLimitValidations.effectiveDate;
		Date randomDate1 = sdf.parse(oneMoreDate);
		randomDate1.setMonth(randomDate.getMonth() - 1);
		effectiveDateTextBox.sendKeys(sdf.format(randomDate1));
		
		expirationDateTextBox.clear();
		SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
		Date randomDate2 = sdf1.parse(TestAggregateLimitValidations.expirationDate);
		String oneMoreDate1 = TestAggregateLimitValidations.expirationDate;
		Date randomDate3 = sdf1.parse(oneMoreDate1);
		randomDate3.setMonth(randomDate2.getMonth() + 1);
		expirationDateTextBox.sendKeys(sdf.format(randomDate3));
	}

	public void enterCategory(int index) {
		Select select = new Select(aggregateLimitCategoryDropDown);
		select.selectByIndex(index);
	}

	public String getSelectedCategory() {
		Select s = new Select(aggregateLimitCategoryDropDown);
		return s.getFirstSelectedOption().getText();

	}

	public String getEnteredEffectiveDate() {
		return effectiveDateTextBox.getAttribute("value");

	}

	public String getEnteredExpirationDate() {
		return expirationDateTextBox.getAttribute("value");
	}

	public void clickOnSaveButton() {
		saveButton.click();
	}
	public void clickOnDeleteButton() {
		deleteButton.click();
	}
	public void clickOnCancelButton() {
		cancelButton.click();
	}

	public void enterExpirationDate() {
		expirationDateTextBox.clear();
		expirationDateTextBox.sendKeys(BaseClass.generateEffectiveDate());
	}

	public void enterEffectiveDate() {
		effectiveDateTextBox.clear();
		effectiveDateTextBox.sendKeys(BaseClass.generateEffectiveDate());
	}

	public String fetchLatestValueFromTable(int index) {
		List<WebElement> rows = aggregateLimitTable.findElements(By.tagName("tr"));
		WebElement latestAggregateLimit = rows.get(rows.size() - 1);

		List<WebElement> latestLimitColumns = latestAggregateLimit.findElements(By.tagName("td"));
		String latestLimit = latestLimitColumns.get(index).getText();
		return latestLimit;
	}

	public String fetchLimitValue() {

		NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
		return n.format(Integer.valueOf(limitTextBox.getAttribute("value")));

	}

	public void refresh() {
		Browser.refresh(driver);

		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (NoAlertPresentException e) {

			e.printStackTrace();
		}

	}

	public void putInvalidDataInLimitField() {
		limitTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}

	public void putInvalidDataInEffectiveDateField() {
		effectiveDateTextBox.sendKeys(BaseClass.stringGeneratorl(4));

	}

	public void putInvalidDataInExpirationDateField() {
		expirationDateTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}

	public void enterValueInLimitFieldMoreThanLimit() {
		limitTextBox.sendKeys(BaseClass.enterRandomNumber(16));
	}

	public void enterEffectiveDateAgain() throws ParseException {
		effectiveDateTextBox.clear();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date randomDate = sdf.parse(TestPolicyAggregateLimit.effectiveDate);
		String oneMoreDate = TestPolicyAggregateLimit.effectiveDate;
		Date randomDate1 = sdf.parse(oneMoreDate);
		randomDate1.setMonth(randomDate.getMonth() + 1);
		effectiveDateTextBox.sendKeys(sdf.format(randomDate1));
	}

	public void  enterExpirationDateAgain() throws ParseException {
		expirationDateTextBox.clear();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date randomDate = sdf.parse(TestPolicyAggregateLimit.expirationDate);
		String oneMoreDate = TestPolicyAggregateLimit.expirationDate;
		Date randomDate1 = sdf.parse(oneMoreDate);
		randomDate1.setMonth(randomDate.getMonth() - 1);
		expirationDateTextBox.sendKeys(sdf.format(randomDate1));
	}

	public String changeTheFormatOfDate(String date) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
		Date randomDate = sdf.parse(date);
		return sdf.format(randomDate);
	}
	
	
}
