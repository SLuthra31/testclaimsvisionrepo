package commonPageObjects.NonComp.Policy;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import utilities.BaseClass;
import utilities.Browser;
import utilities.Wait;

public class PolicyItem {

	WebDriver driver;
	BaseClass bc;
	String policyItemWindow;

	public PolicyItem(WebDriver driver) {
		this.driver = driver;
		bc=new BaseClass(driver);

	}
	
	@FindBy(css="img#NonCompPolicy_policyItemsImage")
	public WebElement policyItem;
	
	@FindBy(css="table#ItemsGridView")
	public WebElement policyItemTable;
	
	@FindBy(css="input#policyNumberTextBox")
	public WebElement policyNumberTextBox;

	@FindBy(css="input#ItemsGridView_ctl02_ImageButton1")
	public WebElement modify;

	@FindBy(css="input#itemIdTextBox")
	public WebElement itemIdTextbox;

	@FindBy(css="input#itemNameTextBox")
	public WebElement itemName;

	@FindBy(css="input#itemYearTextBox")
	public WebElement itemYearTextBox;

	@FindBy(css="select#itemCategoryDropDown")
	public WebElement itemCategoryDropDown;

	@FindBy(css="select#itemTypeDropDown")
	public WebElement itemTypeDropDown;

	@FindBy(css="select#itemSubTypeDropDown")
	public WebElement itemSubTypeDropDown;

	@FindBy(css="input#itemValueTextBox")
	public WebElement itemValueTextBox;

	@FindBy(css="input#itemDescriptionTextBox")
	public WebElement itemDescriptionTextBox;

	@FindBy(css="img#policyCoverageImage")
	public WebElement policyCoverage;

	@FindBy(css="input#addButton")
	public WebElement addButton;

	@FindBy(css="input#deleteButton")
	public WebElement deleteButton;

	@FindBy(css="input#saveButton")
	public WebElement saveButton;

	@FindBy(css="input#cancelButton")
	public WebElement cancelButton;

	@FindBy(css="input#closeButton")
	public WebElement closeButton;
	
	@FindBy(css="table#ItemsGridView")
	public WebElement searchGrid;
	
	@FindBy(css="div#ValidationSummary1")
	public WebElement validationMessageArea;
	
	@FindBy(css="input#ItemsGridView_ctl02_ImageButton1")
	public WebElement modifyIcon;
	
	
	@FindBy(css="input#CoverageGridView_ctl02_ImageButton1")
	public WebElement modifyCoverageIcon;
	
	@FindBy(css="span#noSearchResultLabel")
	public WebElement validationMessageAreaForNoSearchResults;
	

	public void getWindowHandleForItem()
	{
	policyItemWindow=driver.getWindowHandle();	
	}
	public void clickPolicyItem(){
		policyItem.click();
	}
	
	public void clickModfyCoverage()
	{
		modifyCoverageIcon.click();
	}
	
	public void switchToPolicyItemScreen()
	{
		bc.switchToNextWindow("Non Comp Policy Items");
	}
	
	public void getBackToPolicyItemScreen()
	{
		driver.switchTo().window(policyItemWindow);
	}
	
	public void clickAddPolicyItem() throws InterruptedException{
		Wait.modifyWait(driver, addButton);
		addButton.click();
	}
	
	public void clickSavePolicyItem(){
		saveButton.click();
	}
	
	public void clickCancelPolicyItem(){
		cancelButton.click();
	}
	
	public void clickDeleteButton(){
		deleteButton.click();
		try{
			   Alert alert = driver.switchTo().alert();
			   alert.accept();
			  }
			  catch(NoAlertPresentException e)
			  {
			   
			   System.out.println(e.getMessage());
			  }
	}
	

	
	public void selectItemCategory()
	{
		Select s = new Select(this.itemCategoryDropDown);
		s.selectByIndex(1);
	}
	
	public void selectItemType()
	{
		Select s = new Select(this.itemTypeDropDown);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()-1));
	}
	
	public void selectItemSubType()
	{
		Select s = new Select(this.itemSubTypeDropDown);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()-1));
	}
	public String fetchItemName()
	{
		return itemName.getAttribute("value");
	}
	
	public String fetchLatestPolicyItem()
	{
		List<WebElement> policyItems=searchGrid.findElements(By.tagName("tr"));
		WebElement latestPolicyItem=policyItems.get(policyItems.size()-1);
		
		List<WebElement> latestPolicyItemNumber=latestPolicyItem.findElements(By.tagName("td"));
		String latestItem=latestPolicyItemNumber.get(0).getText();
		latestPolicyItemNumber.get(latestPolicyItemNumber.size()-1).click();
		return latestItem;
	}
	
	public void enterItemId(){
		itemIdTextbox.sendKeys(BaseClass.stringGeneratorl(7));
	}
	public String fetchItemID()
	{
	return	itemIdTextbox.getAttribute("value");
	}
	
	public String enterItemName(){
		String itemValue=BaseClass.stringGeneratorl(7);
		itemName.sendKeys(itemValue);
		return itemValue;
	}
	
	public void enterItemYear(){
		itemYearTextBox.sendKeys(BaseClass.enterRandomNumber(4));
	}
	
	public void enterItemValue(){
		itemValueTextBox.sendKeys(BaseClass.enterRandomNumber(3));
	}
	
	public void enterItemDescription(){
	this.itemDescriptionTextBox.sendKeys(BaseClass.stringGeneratorl(9));
	}
	
	public void enterInvalidValueInItemYear()
	{
		itemYearTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}
	public void enterInvalidValueInItemValue()
	{
		itemValueTextBox.sendKeys(BaseClass.stringGeneratorl(4));
	}
	
	public boolean itemIdIsDisabled()
	{
	boolean temp=true;
			if(itemIdTextbox.isEnabled())
			{
				temp=false;
			}
	return temp;
	}public String fetchLatestValueFromTable(int index) {
		List<WebElement> rows = policyItemTable.findElements(By.tagName("tr"));
		WebElement latestPolicyItem = rows.get(rows.size() - 1);

		List<WebElement> latestLimitColumns = latestPolicyItem.findElements(By.tagName("td"));
		String latestLimit = latestLimitColumns.get(index).getText();
		return latestLimit;
	}
	public void clickOnModifyButton()
	{
		modifyIcon.click();
	}
	
	public void refresh()
	 {
	  Browser.refresh(driver);
	  
	  try{
	   Alert alert = driver.switchTo().alert();
	   alert.accept();
	  }
	  catch(NoAlertPresentException e)
	  {
	   
	   e.printStackTrace();
	  }
	 }
	

	public void clickCloseButtonInItemScreen(){
		closeButton.click();
	}

	
	
}
