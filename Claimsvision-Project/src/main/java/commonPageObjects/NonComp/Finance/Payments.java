package commonPageObjects.NonComp.Finance;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Wait;

public class Payments {
	
	WebDriver driver;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;

	public Payments(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "table#ReservesForNonComp_ReservesGridView")
	public WebElement reserveGrid;

	@FindBy(css = "select#Payments_paymentTypeDropdown")
	public WebElement paymentType;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl00_payeeTypeDropdownList")
	public WebElement payeeType;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_modeTypeDropDown")
	public WebElement modeType;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_payCategoryDown")
	public WebElement payCat;

	@FindBy(css = "select#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_paySubCategoryDown")
	public WebElement paySubCat;

	@FindBy(css = "input#Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_amountTextBox")
	public WebElement payAmt;
	
	

	@FindBy(css="input#Payments_ClaimPaymentOneTime_saveNewButton1")
	public WebElement save;
	
	public void selectPaymentTypeAsOneTime() throws InterruptedException {
		Wait.modifyWait(driver, paymentType);
		Select s = new Select(paymentType);
		s.selectByVisibleText("One Time");

	}

	public void selectPayeeType() throws InterruptedException {
		Wait.modifyWait(driver, payeeType);
		Select s = new Select(payeeType);
		s.selectByVisibleText("Claimant");
	}

	public void selectModeType() throws InterruptedException {
		Wait.modifyWait(driver, modeType);
		Select s = new Select(modeType);
//		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getAllSelectedOptions().size() - 1));
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}

	public void selectpayCat(String value) throws InterruptedException {
		Wait.modifyWait(driver, payCat);
		Select s = new Select(payCat);
		s.selectByVisibleText(value);

	}

	public void selectpaySubCat(String value) throws InterruptedException {
		Wait.modifyWait(driver, paySubCat);
		Select s = new Select(paySubCat);
		s.selectByVisibleText(value);

	}

	public String enterPayAmt() throws InterruptedException {
		Wait.modifyWait(driver, payAmt);
		String amt = BaseClass.enterRandomNumber(2);
		String finalAmount="2" + amt;
		payAmt.sendKeys(finalAmount);
		return finalAmount;
	}
	
	public void clickSaveButton() throws InterruptedException
	{
		Wait.modifyWait(driver, save);
		save.click();
		
	}
	
	
}
