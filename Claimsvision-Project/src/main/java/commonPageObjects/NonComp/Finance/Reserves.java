package commonPageObjects.NonComp.Finance;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Wait;

public class Reserves {

	WebDriver driver;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	BaseClass bc;
	String windowForReserveWorksheet;
	String windowForReserveScreen;
	PolicyItemCoverages policyItemCoverges;

	public Reserves(WebDriver driver) {
		this.driver = driver;
		bc = new BaseClass(driver);
	}

	@FindBy(css = "input#ReservesGridView_ctl03_ChangeReserveTextBox")
	public WebElement newReserveTextBox;

	@FindBy(css = "select#ReservesGridView_ctl03_reserveReasonCodeDropdownList")
	public WebElement reasonCode;

	@FindBy(css = "input#PostButton")
	public WebElement postButton;

	@FindBy(css = "input#ReservesForNonComp_WorksheetButton")
	private WebElement workSheet;

	@FindBy(css = "div#ReservesForNonComp_ReservesGridViewDiv table#ReservesForNonComp_ReservesGridView")
	public WebElement reserveGrid;

	@FindBy(css = "input#SaveNoteButton")
	public WebElement yesButton;

	public void switchToWorkSheetScreen() throws InterruptedException {

		Wait.waitFor(2);
		((JavascriptExecutor) driver).executeScript("window.focus();");
		bc.switchToNextWindow("Reserve Worksheet for Non Comp");
		Wait.waitFor(3);
		((JavascriptExecutor) driver).executeScript("window.focus();");
	}

	public void switchToWorkSheetScreenAgain() throws InterruptedException {
		driver.switchTo().window(windowForReserveWorksheet);

	}

	public void getWindowForReserveWorksheet() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.focus();");
		Wait.modifyWait(driver, newReserveTextBox);
		windowForReserveWorksheet = driver.getWindowHandle();
	}

	public void getWindowForReserveScreen() throws InterruptedException {
		Wait.modifyWait(driver, workSheet);
		Set<String> window=driver.getWindowHandles();
		final Iterator<String> iterate = window.iterator();
		windowForReserveScreen =iterate.next();
	}

	public void switchToReserveScreenAgain() {
		driver.switchTo().window(windowForReserveScreen);
	}

	public void clickWorkSheet() throws InterruptedException {
		Wait.modifyWait(driver, workSheet);
		workSheet.click();
	}

	public String enterNewReserves() throws InterruptedException {
		Wait.modifyWait(driver, newReserveTextBox);
		Wait.waitFor(3);
		String enteredReserveAmount = "5" + BaseClass.enterRandomNumber(2);
		newReserveTextBox.sendKeys(enteredReserveAmount);
		return enteredReserveAmount;
	}

	public void enterReasonCode() throws InterruptedException {
		Wait.modifyWait(driver, reasonCode);
		Select s = new Select(reasonCode);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size() - 1));

	}

	public void clickPostChangesButton() throws InterruptedException {
		Wait.modifyWait(driver, postButton);
		postButton.click();
	}

	public void closeWindow() {
		driver.close();
	}

	public void switchToReserveNotesWindow() {
		bc.switchToNextWindow("Create Reserve Change Note?");
	}

	public void switchToParentWindow() {

		Set<String> windows = driver.getWindowHandles();
		Iterator<String> iterate = windows.iterator();

		String parentWindow = iterate.next();

		driver.switchTo().window(parentWindow);
	}

	public void refresh() {
		Browser.refresh(driver);
	}

	public String fetchReserveAmount() throws InterruptedException {
		Wait.modifyWait(driver, reserveGrid);
		WebElement rows = reserveGrid.findElement(By.cssSelector("tr+tr+tr td+td+td+td"));
		return rows.getText();
		// System.out.println(rows.size());
		// WebElement row = rows.get(2);
		// List<WebElement> columns = row.findElements(By.tagName("td"));
		// return columns.get(3).getText();

	}

	public String fetchReserveCategory() throws InterruptedException {
		Wait.modifyWait(driver, reserveGrid);
		WebElement rows = reserveGrid.findElement(By.cssSelector("tr+tr+tr td+td+td"));
		return rows.getText();
	}

	public String fetchPaymentAmt() throws InterruptedException {
		Wait.modifyWait(driver, reserveGrid);
		WebElement rows = reserveGrid.findElement(By.cssSelector("tr+tr+tr td+td+td+td+td"));
		return rows.getText();
	}

	public void clickYesButton() throws InterruptedException {
		Wait.modifyWait(driver, yesButton);
		yesButton.click();
	}
	
	public static void main(String[] args) {
		String s = "$456,789.00";
//		System.out.println(Float.valueOf(s.replaceAll("[^0-9]", "")));
		System.out.println(Float.parseFloat(s.replaceAll("[^0-9.]", "")));
		System.out.println(Float.toString(Float.parseFloat(s.replaceAll("[^0-9.]", ""))));
		
	}

}
