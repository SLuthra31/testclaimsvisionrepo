package commonPageObjects.NonComp.IRF;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.BaseClass;
import utilities.Wait;

public class IRF_ENS {

	WebDriver driver;
	BaseClass bc;
	public IRF_ENS(WebDriver driver) {
		this.driver = driver;
		bc=new BaseClass(driver);

	}

	/*
	 * WebElements of Buttons to perform action on Non Comp Policy Screen
	 */
	@FindBy(css = "img#policyNumberImage")
	public WebElement policyNumberSearch;
		
	@FindBy(css="input#claimNumberTextBox")
	public WebElement claimNumber;
	
	@FindBy(css="input#dateOfIncidentTextBox")
	public WebElement incidentDate;
	
	@FindBy(css="select#timeOfLossTimeControl_hourDropDown")
	public WebElement timeOfLoss_hour;
	
	@FindBy(css="select#timeOfLossTimeControl_minuteDropDown")
	public WebElement timeOfLoss_min;

	@FindBy(css="select#timeOfLossTimeControl_meridianDropDown")
	public WebElement timeOfLoss_meridian;
	
	@FindBy(css="input#policyEffectiveDateTextBox")
	public WebElement policyEffectiveDate;
	
	@FindBy(css="input#policyExpirationDateTextBox")
	public WebElement policyExpirationDate;
	
	@FindBy(css="input#filterPolicyNumberTextBox")
	public WebElement policyNumberTextBox;
		
	@FindBy(css="input#carrierTextBox")
	public WebElement carrier;

	@FindBy(css="input#clientTextBox")
	public WebElement client;
	
	@FindBy(css="select#policyTypeDropDown")
	public WebElement policyType;
	
	@FindBy(css="input#locationTextBox")
	public WebElement levelAssignedTo;
	
	@FindBy(css = "input#searchButton.defaultButton01")
	public WebElement searchPolicyButton;
	
	@FindBy(css="div#searchResultDataList_ctl01_showImage")
	public WebElement searchedResult;
	
	@FindBy(css="input#cancelButton")
	public WebElement cancelButton;
	
	@FindBy(css="input#saveButton")
	public WebElement saveButton;
	
	@FindBy(css = "input#saveAndNew")
	public WebElement saveAndNewButton;
	
	@FindBy(css="input#submitButton")
	public WebElement submitButton;
	
	@FindBy(css="td#searchResultDataList_ctl01_TableCell1")
	public WebElement policyIcon;
	
	@FindBy(css="input#returnButton")
	public WebElement returnButton;


	public void selectPolicy(String policyNumber) throws InterruptedException {

		Wait.waitFor(3);
		String parent=driver.getWindowHandle();
		
		bc.switchToNextWindow("Search Policy");
		Wait.modifyWait(driver, searchPolicyButton);
		policyNumberTextBox.sendKeys(policyNumber);
		searchPolicyButton.click();
		Wait.waitFor(3);

		
		
		policyIcon.click();
		driver.switchTo().window(parent);

		Wait.waitFor(3);
		

	}
	
	public void clickPolicyIcon() throws InterruptedException
	{
		Wait.modifyWait(driver, policyNumberSearch);
		policyNumberSearch.click();
	}
	
	public String enterCustomClaimNumber(){
		String customClaimNumber=BaseClass.enterRandomNumber(4);
		claimNumber.sendKeys(customClaimNumber);
		return customClaimNumber;
	}
	
	
	
	public void enterIncidentDate(String incident){
		incidentDate.sendKeys(incident);
	}
	public void clickSubmitButton(){
		submitButton.click();
	}
	
	public void clickContinueButton() throws InterruptedException{
		Wait.modifyWait(driver, returnButton);
		returnButton.click();
	}
	public void closeIrfScreen() throws InterruptedException{
		driver.close();
	}
	
	public void switchToIrfScreen(){
		Set<String> window=driver.getWindowHandles();
		Iterator<String> iterate=window.iterator();
		String parent=iterate.next();
		String irF=iterate.next();
		driver.switchTo().window(irF);
	}
	
	
}
