package commonPageObjects.NonComp.Claims;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Wait;

public class Claimants {

	WebDriver driver;
	BaseClass baseclass;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;

	public Claimants(WebDriver driver) {
		this.driver = driver;

	}

	/*
	 * WebElements of Buttons to perform action on Non Comp Policy Screen
	 */

	@FindBy(css ="div#Claimants_claimInfo_claimInfoPanel>table>tbody>tr+tr>td+td+td")
	private WebElement testElement;
	
	@FindBy(css = "a#claimsTreeViewt1")
	private WebElement claimantTab;

	@FindBy(css = "input#Claimants_AddPartyButton")
	private WebElement addClaimantButton;

	@FindBy(css = "select#Claimants_claimantTypeDropDown")
	private WebElement claimantDropDown;

	@FindBy(css = "input#Claimants_PartyContactControl_ClaimantFirstNameTextBox")
	private WebElement claimantFirstName;

	@FindBy(css = "input#Claimants_PartyContactControl_ClaimantMiddleNameTextBox")
	private WebElement claimantMiddleName;

	@FindBy(css = "input#Claimants_PartyContactControl_ClaimantLastNameTextBox")
	private WebElement claimanLastName;
	
	@FindBy(css="input#Claimants_PartyContactControl_AddressControl1_ZipCodeTextBox")
	private WebElement zip;
	
	@FindBy(css="input#Claimants_reportDateToInsuredTextBox")
	private WebElement reportDateToInsured;
	
	@FindBy(css = "input#Claimants_incidentDateText")
	private WebElement incidentDate;
	
	@FindBy(css="input#Claimants_saveButton")
	private WebElement save;
	
	@FindBy(css="span#Claimants_saveMessageLabel")
	private WebElement messageArea;
	
	@FindBy(css="input#Claimants_PartyContactControl_AddressControl1_Address1TextBox")
	private WebElement address;
	
	@FindBy(css="select#Claimants_PartyContactControl_AddressControl1_CityDropDownList")
	private WebElement city;
	
	@FindBy(css="select#Claimants_PartyContactControl_AddressControl1_StateDropDownList")
	private WebElement state;
	
	@FindBy(css="input#Claimants_PartyContactControl_AddressControl1_countyTextBox")
	private WebElement county;
	


	public void clickClaimantTab() throws InterruptedException {
		Wait.modifyWait(driver, claimantTab);
		claimantTab.click();
	}

	public void clickAddClaimantButton() throws InterruptedException {
		Wait.modifyWait(driver, addClaimantButton);
		addClaimantButton.click();
	}

	public void selectClaimantType() throws InterruptedException {
		Wait.modifyWait(driver, claimantDropDown);
		Select s = new Select(claimantDropDown);
		s.selectByIndex(1);
	}

	public void addClaimantName() throws InterruptedException {
		Wait.modifyWait(driver, claimantFirstName);
		claimantFirstName.sendKeys("Automation");
		claimantMiddleName.sendKeys("of");
		claimanLastName.sendKeys("Claimant");
	}
	
	public String getIncidentDateFromHeader(){
		String date=testElement.getText();
		int count=date.lastIndexOf(":");
		String finalDate = date.substring(count+1).trim();
		return finalDate;
	}
	
	public void addClaimantZip() throws InterruptedException {
	
		zip.sendKeys("55555");
	}
	
	public void addClaimantAddress(){
		address.clear();
		address.sendKeys("Park Street");
	}
	
	public String getIncidentDate(){
		String incidentDateValue = incidentDate.getAttribute("value");
		return incidentDateValue;
	}
	public void addReportDateToInsured() throws ParseException  {
		
		reportDateToInsured.click();
		Wait.waitFor(5);
		reportDateToInsured.sendKeys(BaseClass.generateDateMoreThanSpecificDate(getIncidentDateFromHeader()));
	}
	
	
	
	public void clickOnSaveButton(){
		save.click();
	}
	
	public String fetchClaimantType() throws InterruptedException
	{
		Wait.modifyWait(driver, claimantDropDown);
		Select s = new Select(claimantDropDown);
		return s.getFirstSelectedOption().getText();
	}
	
	public String fetchClaimantName() throws InterruptedException
	{
		return claimantFirstName.getAttribute("value")+" "+claimantMiddleName.getAttribute("value")+" "+claimanLastName.getAttribute("value");
		
	}
	
	public String fetchReportDateToInsured() throws InterruptedException {
		return reportDateToInsured.getAttribute("value");
	}
	
	public String fetchValidationMessage() throws InterruptedException {
		return messageArea.getText();
	}
	
	public String fetchAddress(){
		
		return address.getAttribute("value");
		
	}
	
	public String fetchCity() throws InterruptedException{
			
		
		Select s = new Select(city);
		return s.getFirstSelectedOption().getText();
			
		}
	
	public String fetchState(){
		
		Select s= new Select(state);
		return s.getFirstSelectedOption().getText();
		
	}
	
	public String fetchZip(){
		
		return zip.getAttribute("value");
		
	}
	
	public String fetchCounty(){
		
		return county.getAttribute("value");
		
	}	
}
