package commonPageObjects.NonComp.Claims;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Wait;

public class Claims {

	WebDriver driver;
	BaseClass baseclass;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;

	public Claims(WebDriver driver) {
		this.driver = driver;

	}

	/*
	 * WebElements of Buttons to perform action on Non Comp Policy Screen
	 */

	@FindBy(css = "input#Claim_claimNumberTextBox")
	private WebElement claimNumber;

	@FindBy(css = "input#Claim_enteredByTextBox")
	public WebElement enteredBy;

	@FindBy(css = "select#Claim_claimTypeDropDown")
	public WebElement claimType;

	@FindBy(css = "select#Claim_statusDropDown")
	public WebElement status;
	
	@FindBy(css = "select#Claim_adjusterDropDown")
	public WebElement adjuster;
	
	@FindBy(css = "select#Claim_supervisorDropDown")
	public WebElement supervisor;
	
	@FindBy(css = "input#Claim_PolicyYearTextBox")
	public WebElement policyYear;
	
	@FindBy(css = "input#Claim_injuryDateTextBox")
	public WebElement incidentDate;

	@FindBy(css = "input#Claim_saveButton")
	public WebElement saveButton;

	@FindBy(css = "span#Claim_saveMessageLabel")
	public WebElement messageArea;

	@FindBy(css = "a#claimsTreeViewt0")
	public WebElement claimTab;

	@FindBy(css = "input#closeClaimImageButton")
	private WebElement closeClaimImageButton;

	@FindBy(css = "input#openClaimImageButton")
	private WebElement openClaimImageButton;

	public void clickClaimTab() throws InterruptedException {
		Wait.modifyWait(driver, claimTab);
		claimTab.click();
	}

	public String getIncidentDate(){
		String incidentDateValue = incidentDate.getAttribute("value");
		int incidentDateYear = incidentDateValue.lastIndexOf("/");
		String incidentDateYearValue = incidentDateValue.substring(incidentDateYear+1);
		return incidentDateYearValue;
	}
	public void selectClaimType() throws InterruptedException {
		Thread.sleep(2000);
		Select s = new Select(claimType);
		s.selectByIndex(1);
	}

	public void selectClaimStatus() throws InterruptedException {
		Thread.sleep(2000);
		Select s = new Select(status);
		s.selectByIndex(1);
	}
	public void selectAdjuster() throws Exception{
		Thread.sleep(2000);
		Select s = new Select(adjuster);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}
	
	public void selectSupervisor() throws Exception{
		Thread.sleep(2000);
		Select s = new Select(supervisor);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
	}

	public void enterPolicyYear(){
		policyYear.sendKeys(getIncidentDate());
		System.out.println("PolicyYear: "+ getIncidentDate());
	}
	public void clickOnSave() {
		saveButton.click();
	}

	public String fetchValidationMessage() throws InterruptedException {

		return messageArea.getText();
	}

	public String fetchClaimStatus() throws InterruptedException {
		Wait.modifyWait(driver, status);
		Select s = new Select(status);
		return s.getFirstSelectedOption().getText();

	}

	public String fecthClaimType() throws InterruptedException {
		Wait.modifyWait(driver, claimType);
		Select s = new Select(claimType);
		return s.getFirstSelectedOption().getText();
	}
	
	
	
	public void clickCloseClaimIcon() throws InterruptedException{
		Wait.modifyWait(driver, closeClaimImageButton);
		closeClaimImageButton.click();
		try{
		Alert alert= driver.switchTo().alert();
		alert.accept();}
		catch(NoAlertPresentException e)
		{
			e.getMessage();
			
		}
		
	}
	
	
	public void clickOpenClaimIcon() throws InterruptedException{
		Wait.modifyWait(driver, openClaimImageButton);
		openClaimImageButton.click();
		try{
			Alert alert = driver.switchTo().alert();
			alert.accept();
		}
			catch(NoAlertPresentException e)
		{
				e.getMessage();
		}
	}
	
}
