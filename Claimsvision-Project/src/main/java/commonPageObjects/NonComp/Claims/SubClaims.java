package commonPageObjects.NonComp.Claims;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Wait;

public class SubClaims {


	WebDriver driver;
	BaseClass baseclass;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;

	public SubClaims(WebDriver driver) {
		this.driver = driver;

	}

	/*
	 * WebElements of Buttons to perform action on Non Comp Policy Screen
	 */

	@FindBy(css = "a#claimsTreeViewt2")
	private WebElement subClaimTab;
	
	@FindBy(css = "input#SubClaims_addSubClaimButton")
	private WebElement addSubClaimButton;
	
	@FindBy(css = "select#SubClaims_insurableItemDropDown")
	private WebElement insurableItem;
	
	@FindBy(css = "select#SubClaims_coverageDropDown")
	private WebElement coverage;
	
	@FindBy(css = "input#SubClaims_saveButton")
	private WebElement saveButton;
	
	@FindBy(css = "span#SubClaims_saveMessageLabel")
	private WebElement messageArea;
	
	@FindBy(css="a#SubClaims_ClaimantList1_claimantList_ctl01_subClaimLinkButton")
	private WebElement subCLaimLink;
	
	@FindBy(css="input#closeClaimImageButton")
	private WebElement closeClaimImageButton;
	
	@FindBy(css="select#SubClaims_StatusDropDown")
	private WebElement subClaimStatusDropDown;
	
	@FindBy(css="input#openClaimImageButton")
	private WebElement openClaimImageButton;
	
	public void clicksOnAddSubClaimButton() throws InterruptedException{
		Wait.modifyWait(driver, addSubClaimButton);
		addSubClaimButton.click();
	}
	
	public void clickSubClaimTab(){
		subClaimTab.click();
	}
	
	public String fetchValidationMessage(){
		return messageArea.getText();
	}
	
	public String fetchInsurableItem(){
		Select s= new Select(insurableItem);
		return s.getFirstSelectedOption().getText();
		}
	public String  getSubClaimNumber(){
		return subCLaimLink.getText();
		 
	}
	public String  getCoverageName(){
		Select s= new Select(coverage);
		return s.getOptions().get(s.getOptions().size()-1).getText();
		 
	}
	
	public String fetchCoverageItem(){
		Select s= new Select(coverage);
		return s.getFirstSelectedOption().getText();
		}
	
	public void addInsurableItem() throws InterruptedException{
		Wait.modifyWait(driver, insurableItem);
		Select s = new Select(insurableItem);
		s.selectByIndex(1);
	}
	
	public void addCoverageItem() throws InterruptedException{
		Wait.modifyWait(driver, coverage);
		Select s = new Select(coverage);
		s.selectByIndex(1);
	}
	
	public void clickSaveButton() throws InterruptedException
	{
		Wait.modifyWait(driver, saveButton);
		saveButton.click();
	}
	
	public void clickCloseClaimIcon() throws InterruptedException{
		Wait.modifyWait(driver, closeClaimImageButton);
		closeClaimImageButton.click();
		try{
		Alert alert= driver.switchTo().alert();
		alert.accept();}
		catch(NoAlertPresentException e)
		{
			e.getMessage();
		}
		
	}
	
	public String fetchSubClaimStatus() throws InterruptedException
	{
		Wait.modifyWait(driver, subClaimStatusDropDown);
		Select s =new Select(subClaimStatusDropDown);
		 return s.getFirstSelectedOption().getText();
	}
	
	public void clickOpenClaimIcon() throws InterruptedException{
		Wait.modifyWait(driver, openClaimImageButton);
		openClaimImageButton.click();
		try{
			Alert alert = driver.switchTo().alert();
			alert.accept();
		}
			catch(NoAlertPresentException e)
		{
				e.getMessage();
		}
	}
	
	
	
}
