package testScripts.LACO.Login;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.Login.SearchClaim;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class SearchClaimTestScripts {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	SearchClaim searchClaimPageObject;
	Dashboard dashboardPageObjects;
	WebDriver driver;
	BaseClass bc;

	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		searchClaimPageObject = PageFactory.initElements(driver, SearchClaim.class);
		dashboardPageObjects = PageFactory.initElements(driver, Dashboard.class);
		//searchClaimPageObject = loginPageObjects.Login().navigateToClaimSearch();
		dashboardPageObjects.selectAgree();
}
	
	@BeforeMethod
	public void beforeMethod() throws Exception{
		driver.get(Configuration.baseUrl);
	}
	
	@Test
	public void testCase01_SelectClaimNumber() throws Exception{
		
		Wait.implicitWait(driver, 20);
		searchClaimPageObject = loginPageObjects.Login(8).navigateToClaimSearch();
		logger = report.startTest("Verify Add New Policy with Policy User Defined fields");
		String selectedClaim = searchClaimPageObject.clickAndGetClaimNumber();
		logger.log(LogStatus.INFO, "Claim Number is clicked");
		Wait.waitFor(10);
		Assert.assertTrue(driver.getTitle().startsWith("Claim"));
		
		logger.log(LogStatus.PASS, "Claim Number clicked" + selectedClaim);
		
	}
	
	@Test
	public void testCase02_SelectClaimNumberThrough25Claims() throws Exception  {
		
		searchClaimPageObject = loginPageObjects.Login(8).navigateToUserActivity();
		Wait.implicitWait(driver, 20);
		logger = report.startTest("Verify Add New Policy with Policy User Defined fields");
		String selectedClaim = searchClaimPageObject.clickAndGetClaimNumberThrough25Claims();
		logger.log(LogStatus.INFO, "Claim Number is clicked");
		Wait.waitFor(15);
		Assert.assertTrue(driver.getTitle().startsWith("Claim"));
		logger.log(LogStatus.PASS, "Claim Number clicked"+ ""+selectedClaim);
		
	}
	

	

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException
	{
	if(result.getStatus()==ITestResult.FAILURE)
	{
	TakeScreenshot.takeFailScreen(result.getName(), driver);
	String image= logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
	logger.log(LogStatus.FAIL, "Title verification", image);
	
	report.endTest(logger);
	

	 
	}
	}
	
	@AfterClass
	public void afterClass()
	{
		
		
		report.flush();
		driver.quit();
	}
	
	
}
