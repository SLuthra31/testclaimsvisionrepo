package testScripts.LACO.Claims.ClaimantIncident;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.WorkersComp.Claimant.Claimant;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;

public class TestConversionData {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	Claimant claimantObject;
	WebDriver driver;
	
	public final boolean execution=true;
	
	{

		report=ReportManager.getReporter();
	}
	
	@BeforeClass
	public void beforeClass()
	{		
		driver=Browser.getBrowser("ie");
		driver.manage().window().maximize();
		loginPageObjects=PageFactory.initElements(driver,LoginPageObjects.class);;
		claimantObject = PageFactory.initElements(driver, Claimant.class);
		
		
	}
	
	@BeforeMethod
	public void beforeMethod()
	{
	
		
		driver.get(Configuration.baseUrl);	
	}
	
	@Test
	public void testConversionOfData(){
		
		
	}
}
