package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class CheckInvalidInputFields {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	WebDriver driver;
	BaseClass bc;

	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(5);
		Browser.refresh(driver);
		Wait.waitFor(2);

	}

	// Test case is for verify Validation Message after entering invalid
	// Zip(Postal) field
	@Test(priority = 1, enabled = execution)
	public void testCase01_checkMessage_Postal() throws Exception {
		/* Wait.waitFor(5); */
		
		logger = report.startTest("Test - Invalid fields Message - Zip(Postal)");

		// steps used in test cases
		logger.log(LogStatus.INFO, "User clicked on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered incorrect 'Postal' value");
		policySetupPageObject.enterInvalidDataInPostal();
		logger.log(LogStatus.INFO, "User clicks on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		// Assertion for validation
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0),
				"The format of Zip Code is invalid, it should be ##### or #####-####");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	// Test case is for verify Validation Message after entering invalid Phone
	// field
	@Test(priority = 2, enabled = execution)
	public void testCase02_checkMessage_Phone() throws Exception {
		/* Wait.waitFor(5); */
	
		logger = report.startTest("Test - Invalid fields Message - Phone");

		// steps used in test cases
		logger.log(LogStatus.INFO, "User clicked on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered incorrect 'Phone' value");
		policySetupPageObject.enterInvalidDataInPhone();
		logger.log(LogStatus.INFO, "User clicks on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		// Assertion for Validation
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0),
				"The Phone number has to be numeric, the format of the Phone number should be (###)###-#### or ##########");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	// Test case is for verify Validation Message after entering invalid Fax
	// field
	@Test(priority = 3, enabled = execution)
	public void testCase03_checkMessage_Fax() throws Exception {
		/* Wait.waitFor(5); */

		logger = report.startTest("Test - Invalid fields Message - Fax");

		// steps used in test cases
		logger.log(LogStatus.INFO, "User clicked on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered incorrect 'Fax' value");
		policySetupPageObject.enterInvalidDataInFax();
		logger.log(LogStatus.INFO, "User clicked on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		// Assertion for Validation
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0),
				"The Fax number has to be numeric, the format of the Fax number should be (###)###-#### or ##########");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	// Test case is for verify Validation Message after entering invalid Email
	// field
	@Test(priority = 4, enabled = execution)
	public void testCase04_checkMessage_Email() throws Exception {
		/* Wait.waitFor(5); */
		
		logger = report.startTest("Test - Invalid fields Message - Email");

		// steps used in test cases
		logger.log(LogStatus.INFO, "User clicks on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered incorrect 'Email' value");
		policySetupPageObject.enterInvalidDataInEmail();
		logger.log(LogStatus.INFO, "User clicked on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		// Assertion for Validation
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0),
				"The format of Email Address is invalid, it should be xxx@xxx.xxx");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	// Test case is for verify Validation Message after entering invalid Premium
	// field
	@Test(priority = 5, enabled = execution)
	public void testCase05_checkMessage_Premium() throws Exception {
		/* Wait.waitFor(5); */
		
		logger = report.startTest("Test - Invalid fields Message - Premium");

		// steps used in test cases
		logger.log(LogStatus.INFO, "User clicks on Add Button ");
		policySetupPageObject.clickOnAddButton();
		Wait.waitFor(2);
		logger.log(LogStatus.INFO, "User selected 'Company'");
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered incorrect 'Premium' value");
		policySetupPageObject.enterInvalidDataInPremium();
		logger.log(LogStatus.INFO, "User clicked on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		// Assertion for Validation
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0), "Premium is invalid.");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	// Test case is for verify Validation Message after entering invalid
	// Deductible field
	@Test(priority = 6, enabled = execution)
	public void testCase06_checkMessage_Deductible() throws Exception {
		/* Wait.waitFor(5); */
		
		logger = report.startTest("Test - Invalid fields Message - Deductible");

		// steps used in test cases
		logger.log(LogStatus.INFO, "User clicks on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered incorrect 'Deductible' value");
		policySetupPageObject.enterInvalidDataInDeductible();
		logger.log(LogStatus.INFO, "User clicked on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		// Assertion for Validation
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0), "Deductible is invalid.");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	// Test case is for verify Validation Message after entering invalid
	// PolicyYear field
	@Test(priority = 7, enabled = execution)
	public void testCase07_checkMessage_PolicyYear() throws Exception {
		/* Wait.waitFor(5); */
		
		logger = report.startTest("Test - Invalid fields Message - Policy year");

		// steps used in test cases
		logger.log(LogStatus.INFO, "User clicks on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered incorrect 'Policy Year' value");
		policySetupPageObject.enterInvalidDataInPolicyYear();
		logger.log(LogStatus.INFO, "User clicked on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		// Assertion for Validation
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0), "Policy Year is invalid.");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	// Testcase is for verifying validation message for entering Same Expiration
	// Date like Effective Date
	@Test(priority = 8, enabled = execution)
	public void testCase08_checkMessage_ExpDateSameAsEffDate() throws Exception {

		logger = report.startTest("Test - Message - When Expiration Date same as Effective Date");
		Thread.sleep(5000);
		
		logger.log(LogStatus.INFO, "User clicks on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		Thread.sleep(4000);
		logger.log(LogStatus.INFO, "User entered Expiration Date same as Effective Date");
		policySetupPageObject.enterExpDateSameAsEffDate();
		logger.log(LogStatus.INFO, "User entered Short Policy Number");
		policySetupPageObject.enterShortPolicyNumber();
		logger.log(LogStatus.INFO, "User clicked on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0),
				"Policy Expiration Dt should be greater than Effective Dt");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));

	}

	// Testcase is for verifying validation message for entering Expiration Date
	// less than Effective Date
	@Test(priority = 9, enabled = execution)
	public void testCase09_checkMessage_ExpDateLessThanEffDate() throws Exception {

		logger = report.startTest("Test - Message - When Expiration Date less than Effective Date");
		Thread.sleep(5000);
		;
		logger.log(LogStatus.INFO, "User clicks on Add Button ");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.selectPolicyType();
		Thread.sleep(4000);
		logger.log(LogStatus.INFO, "Entering Incorrect Expiration Date");
		policySetupPageObject.enterIncorrectExpirationDate();
		logger.log(LogStatus.INFO, "Entering Short Policy Number");
		policySetupPageObject.enterShortPolicyNumber();
		logger.log(LogStatus.INFO, "Clicking on Save Button");
		policySetupPageObject.clickOnSaveButton();
		bc.BrowserScrollup();
		Wait.waitFor(2);
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0),
				"Policy Expiration Dt should be greater than Effective Dt");
		logger.log(LogStatus.PASS,
				"System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		driver.quit();
	}

}
