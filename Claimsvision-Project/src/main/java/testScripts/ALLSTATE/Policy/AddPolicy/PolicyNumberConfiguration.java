package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class PolicyNumberConfiguration {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	Dashboard dashboardObjects;
	PolicyNumberAssignment policyNumberAssignment;
	WebDriver driver;
	BaseClass bc;
	
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		policyNumberAssignment = loginPageObjects.Login(14).navigateToPolicyNumberAssignment();

	}
	
	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(5);	
		Browser.refresh(driver);
	}
	
	/*
	 * Methods for Non Comp Policy Number Configurations
	 */
	@Test(priority=1,enabled=execution)
	public void testCase01_nonCompPolicyNumber_Ascending(){
		logger = report.startTest("Test Configuration - Non Comp Policy Number - Ascending Order");
		
		//Steps used in test cases
		policyNumberAssignment.nonComp_PolicyNumberAscending();
			logger.log(LogStatus.INFO, "User entered values in ascending order");
		policyNumberAssignment.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
			Wait.waitFor(3);
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "Non Comp Policy Number configured in ascending order");
		
	}
	
	@Test(priority=2,enabled=execution)
	public void testCase02_nonCompPolicyNumber_Descending(){
		logger = report.startTest("Test Configuration - Non Comp Policy Number - Descending Order");
		
		//Steps used in test cases
		policyNumberAssignment.nonComp_PolicyNumberDescending_withHyphens();
			logger.log(LogStatus.INFO, "User entered values in descending order");
		policyNumberAssignment.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "Non Comp Policy Number configured in descending order");
		
	}

	@Test(priority=3,enabled=execution)
	public void testCase03_nonCompPolicyNumber_Clear(){
		logger = report.startTest("Test Clear button - Non Comp Policy Setup");
		
		//Steps used in test cases
		policyNumberAssignment.nonComp_PolicyNumberAscending();
			logger.log(LogStatus.INFO, "User entered values in ascending order");
		policyNumberAssignment.nonCompClear.click();
			logger.log(LogStatus.INFO, "User clicked on clear button");
			Wait.waitFor(2);
		policyNumberAssignment.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "Clear button functionality tested for Non Comp Policy");
			
	}
	
	@Test(priority=4,enabled=execution)
	public void testCase04_nonCompPolicyNumber_InvalidOrder(){
		logger = report.startTest("Test Configuration - Non Comp Policy Number - Invalid Order");
		
		//Steps used in test cases
		policyNumberAssignment.nonComp_PolicyNumber_InvalidOrder();
			logger.log(LogStatus.INFO, "User entered values in invalid order");
		policyNumberAssignment.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Invalid NonComp Policy Order");
			logger.log(LogStatus.PASS, "System displayed correct error message: " + policyNumberAssignment.messageValidation.getText());
		
	}
	
	@Test(priority=5,enabled=execution)
	public void testCase05_nonCompPolicyNumber_NotConfiguredAllFields(){
		logger = report.startTest("Test Configuration - Non Comp Policy Number - Not All fields");
		
		//Steps used in test cases
		policyNumberAssignment.nonComp_PolicyNumber_NotConfiguredAllFields();
			logger.log(LogStatus.INFO, "User entered values in few fields");
		policyNumberAssignment.compSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "System displayed correct message: " + policyNumberAssignment.messageValidation.getText());
		
	}
	
	/*
	 * Methods for Comp Policy Number Configurations
	 */
	@Test(priority=6,enabled=execution)
	public void testCase06_compPolicyNumber_Ascending(){
		logger = report.startTest("Test Configuration - Comp Policy Number - Ascending Order");
		
		//Steps used in test cases
		policyNumberAssignment.comp_PolicyNumberAscending();
			logger.log(LogStatus.INFO, "User entered values in ascending order");
		policyNumberAssignment.compSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "Comp Policy Number configured in ascending order ");
		
	}
	
	@Test(priority=7,enabled=execution)
	public void testCase07_compPolicyNumber_Descending(){
		logger = report.startTest("Test Configuration - Comp Policy Number - Descending Order");
		
		//Steps used in test cases
		policyNumberAssignment.comp_PolicyNumberDescending_withHyphens();
			logger.log(LogStatus.INFO, "User entered values in descending order");
		policyNumberAssignment.compSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "Comp Policy Number configured in descending order ");
		
	}
	
	@Test(priority=8,enabled=execution)
	public void testCase08_compPolicyNumber_Clear(){
		logger = report.startTest("Test Clear button - Comp Policy Setup");
		
		//Steps used in test cases
		policyNumberAssignment.comp_PolicyNumberAscending();
			logger.log(LogStatus.INFO, "User entered values in ascending order");
		policyNumberAssignment.compClear.click();
			logger.log(LogStatus.INFO, "User clicked on clear button");
		
			policyNumberAssignment.compSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "Clear button functionality tested for Comp Policy");
		
	}
	
	@Test(priority=9,enabled=execution)
	public void testCase09_compPolicyNumber_InvalidOrder(){
		logger = report.startTest("Test Configuration - Comp Policy Number - Invalid Order");
		
		//Steps used in test cases
		policyNumberAssignment.comp_PolicyNumber_InvalidOrder();
			logger.log(LogStatus.INFO, "User entered values in invalid order");
		policyNumberAssignment.compSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
			Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Invalid Workers's Comp Policy Number Order");
			logger.log(LogStatus.PASS, "System displayed correct error message: " + policyNumberAssignment.messageValidation.getText());
		
	}
	
	@Test(priority=10,enabled=execution)
	public void testCase10_compPolicyNumber_NotConfiguredAllFields(){
		logger = report.startTest("Test Configuration - Non Comp Policy Number - Not All fields");
		
		//Steps used in test cases
		policyNumberAssignment.comp_PolicyNumber_NotConfiguredAllFields();
			logger.log(LogStatus.INFO, "User entered values in ascending order");
		policyNumberAssignment.compSave.click();
			logger.log(LogStatus.INFO, "User clicked on save button");
		Wait.waitFor(2);
		//Assertion for Validations
		Assert.assertEquals(policyNumberAssignment.messageValidation.getText(), "Saved successfully.");
			logger.log(LogStatus.PASS, "System displayed correct message: " + policyNumberAssignment.messageValidation.getText());
		
	}
	

	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}
	
	@AfterClass
	public void afterClass() {

		report.flush();
		driver.quit();
	}

	
}
