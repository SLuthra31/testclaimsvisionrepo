package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class CheckDataValidations {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	WebDriver driver;
	BaseClass bc;
	String effDate;
	String effectiveDate;
	String expirationDate;

	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);

	}

	// Testcase is for verifying validation message when creating policy when no
	// order configured in policy number assignment screen
	@Test(priority = 1, enabled = execution)
	public void testCase01_checkMessageWhenPolicyNumber_NotConfigured() throws Exception {
		try {
			Browser.refresh(driver);
			Wait.waitFor(3);
			logger = report.startTest("Verify Validation Message if Policy Number not configured");
			dashboardPageObject.navigateToPolicyNumberAssignment();
			logger.log(LogStatus.INFO, "Navigating to Policy number assignment screen");
			policyNumberAssign.setUp_NonComp_PolicyNumberAssignmentConfiguration_AllBlank();
			logger.log(LogStatus.INFO, "Configuring Policy Number order");
			policyNumberAssign.nonCompSave.click();
			logger.log(LogStatus.INFO, "Clicking on save button in policy number assignment screen");
			Browser.refresh(driver);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Navigating to Policy Setup Screen");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "Clicked on Add Button");
			Wait.waitFor(2);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "Selecting company as test");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "Selecting a carrier");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "Selecting a client");
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "Selecting a policy type");
			policySetupPageObject.enterEffectiveDateAndExpirationDate();
			logger.log(LogStatus.INFO, "Entering Effective & Expiration Date");
			policySetupPageObject.enterShortPolicyNumber();
			logger.log(LogStatus.INFO, "Entering Short Policy Number");
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "Clicking on Save button");

			Thread.sleep(3000);
			bc.BrowserScrollup();
			Thread.sleep(3000);
			Assert.assertEquals(policySetupPageObject.fetchMessages(),
					"Please Configure the Policy Number Assignment before creating Policy", "Validation verified");
			logger.log(LogStatus.PASS, "Validaton Message verified if Policy No. not configured");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// This test case is for checking validation message after not entering
	// configured field
	@Test(priority = 2, enabled = execution)
	public void testCase02_checkMessageWhenUserDontEnterConfiguredField() throws Exception {
		try {
			Browser.refresh(driver);
			Wait.waitFor(3);
			logger = report.startTest("Verify Validation message when user donot enter configured field");
			dashboardPageObject.navigateToPolicyNumberAssignment();
			logger.log(LogStatus.INFO, "Navigating to Policy number assignment screen");
			policyNumberAssign.nonComp_PolicyNumberAscending();
			logger.log(LogStatus.INFO, "Configuring Policy Number order");
			policyNumberAssign.nonCompSave.click();
			logger.log(LogStatus.INFO, "Clicking on save button in policy number assignment screen");
			Browser.refresh(driver);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Navigating to Policy Setup Screen");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "Clicked on Add Button");
			Wait.waitFor(2);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "Selecting company as test");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "Selecting a carrier");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "Selecting a client");
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "Selecting a policy type");
			policySetupPageObject.enterEffectiveDateAndExpirationDate();
			logger.log(LogStatus.INFO, "Entering the effective Date and" + "Expiration Date");
			// policySetupPageObject.checkInsuredIsClient();
			logger.log(LogStatus.INFO, "Selecting Insured is Client");
			logger.log(LogStatus.INFO, "Leaving  the Short Policy number field blank");
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "Clicking on save button");
			Wait.waitFor(3);
			bc.BrowserScrollup();
			Wait.waitFor(3);
			Assert.assertEquals(policySetupPageObject.fetchMessages(),
					"ShortPolicyNumber must be populated for a Policy Number to be assigned. Please populate the ShortPolicyNumber or click 'Cancel'",
					"Validation verified");
			logger.log(LogStatus.PASS, "Validaton Message verified if user enters field which is not configured");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// This test case is for checking overlapping functionality
	@Test(priority = 3, enabled = execution)
	public void testCase03_checkOverlappingFeatureOfPolicy() throws Exception {
		try {
			Browser.refresh(driver);
			Wait.waitFor(3);
			logger = report.startTest(
					"Verify is user is able to create policy with overlapped effective date and expiration date");
			dashboardPageObject.navigateToPolicyNumberAssignment();
			logger.log(LogStatus.INFO, "Navigating to Policy number assignment screen");
			policyNumberAssign.nonComp_PolicyNumber_NotConfiguredAllFields();
			logger.log(LogStatus.INFO, "Configuring Policy Number order");
			policyNumberAssign.nonCompSave.click();
			logger.log(LogStatus.INFO, "Clicking on save button in policy number assignment screen");
			Browser.refresh(driver);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Navigating to Policy Setup Screen");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "Clicked on Add Button");
			Wait.waitFor(3);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "Selecting company as test");
			Wait.waitFor(3);
			String company = policySetupPageObject.fetchCompany();
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "Selecting a carrier");
			Wait.waitFor(3);
			String carrier = policySetupPageObject.fetchCarrier();
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "Selecting a client");
			Wait.waitFor(2);
			String client = policySetupPageObject.fetchClient();
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "Selecting a policy type");
			Wait.waitFor(3);
			String policyType = policySetupPageObject.fetchPolicyType();
			policySetupPageObject.enterEffectiveDateAndExpirationDate();
			effectiveDate = policySetupPageObject.fetchEffectiveDate();
			expirationDate = policySetupPageObject.fetchExpirationDate();
			logger.log(LogStatus.INFO, "Entering the effective Date and" + "Expiration Date");
			// policySetupPageObject.checkInsuredIsClient();
			// logger.log(LogStatus.INFO, "Selecting Insured is Client");
			Wait.waitFor(3);
			policySetupPageObject.enterShortPolicyNumber();
			logger.log(LogStatus.INFO, "Entering a Short Policy number");
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "Clicking on save button");
			Wait.waitFor(3);
			Browser.refresh(driver);
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "Clicked on Add Button");
			Wait.waitFor(3);
			policySetupPageObject.selectCompanyByText(company);
			logger.log(LogStatus.INFO, "Selecting company as test");
			Wait.waitFor(5);
			policySetupPageObject.selectCarrierValueThroughText(carrier.trim());
			logger.log(LogStatus.INFO, "Selecting a carrier");
			Wait.waitFor(5);
			policySetupPageObject.selectClientValueThroughText(client.trim());
			logger.log(LogStatus.INFO, "Selecting a client");
			Wait.waitFor(5);
			policySetupPageObject.selectPolicyTypeThroughText(policyType);
			logger.log(LogStatus.INFO, "Selecting a policy type");
			Wait.waitFor(3);
			policySetupPageObject.enterSameEffectiveDate(effectiveDate);
			policySetupPageObject.enterSameExpirationDate(expirationDate);
			logger.log(LogStatus.INFO, "Entering the effective Date and" + "Expiration Date");
			// policySetupPageObject.checkInsuredIsClient();
			// logger.log(LogStatus.INFO, "Selecting Insured is Client");
			Wait.waitFor(4);
			policySetupPageObject.enterShortPolicyNumber();
			logger.log(LogStatus.INFO, "Entering a Short Policy number");
			String shortPolicyNumber = policySetupPageObject.fetchShortPolicyNumber();
			String policyNum = policySetupPageObject.getExpectedPolicyNumber();
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "Clicking on save button");
			bc.BrowserScrollup();
			Wait.waitFor(5);
			if (policySetupPageObject.fetchMessages().startsWith("The Client has the following policies"))

			{
				logger.log(LogStatus.PASS, "Validations appears for overlapping");
				policySetupPageObject.selectContinueButton();
				logger.log(LogStatus.INFO, "Clicking on Continue button");
				Wait.waitFor(4);
				policySetupPageObject.goToLastSearchResultScreen();
				Wait.waitFor(15);
				logger.log(LogStatus.INFO, "Going to latest policy in search result");
			}

			else {
				logger.log(LogStatus.FAIL, "No validation appeared for overlapping functionality");
				Assert.fail("No Validation appeared for overlapping functionality");
			}

			Assert.assertEquals(policySetupPageObject.fetchLatestPolicyNumber(), policyNum);
			logger.log(LogStatus.PASS, "Hurray!!,Policy has been created");

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// This test case is for checking Overlapped Effective Date value after
	// creating
	// policy
	@Test(enabled = true, dependsOnMethods = "testCase03_checkOverlappingFeatureOfPolicy")
	public void testCase03_02_checkEffectiveDateOfOverlappedPolicy1() throws Exception {

		try {
			Assert.assertEquals(policySetupPageObject.fetchLatestEffectiveDate(), effectiveDate);
			logger.log(LogStatus.PASS, "Hurray!!,Effective Date is" + effectiveDate);
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// This test case is for checking Overlapped Expiration Date Date value
	// after creating
	// policy
	@Test(enabled = true, dependsOnMethods = "testCase03_checkOverlappingFeatureOfPolicy")
	public void testCase03_03_checkExpirationDateOfOverlappedPolicy1() throws Exception {

		try {
			Assert.assertEquals(policySetupPageObject.fetchLatestExpirationDate(), expirationDate);
			logger.log(LogStatus.PASS, "Hurray!!,Expiration Date is" + expirationDate);
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		// driver.quit();
	}

}
