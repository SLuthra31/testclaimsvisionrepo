package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class MandatoryFieldsValidations {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	WebDriver driver;
	BaseClass bc;
	
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		
		Wait.waitFor(5);	
		Browser.refresh(driver);
		
		
	}
	
	// Testcase is for verifying validation message for Client Field
	@Test(priority = 1, enabled = execution)
	public void testCase01_checkMessage_Client() throws Exception {
		try{
		logger = report.startTest("Test - Mandatory field Message - Client");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User clicked on Add button");
		policySetupPageObject.selectPolicyType();
		logger.log(LogStatus.INFO, "User selected 'Policy type'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.clickOnSaveButton();
		logger.log(LogStatus.INFO, "User clicked on Save button");
		bc.BrowserScrollup();
		Wait.waitFor(2);
		Assert.assertEquals("Client is required.",policySetupPageObject.fetchAllValidationMessages().get(0),
				"Validation messsage verified for client type");
		logger.log(LogStatus.PASS, "System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
	} catch (Exception e) {
		logger.log(LogStatus.FAIL, e);
		Assert.fail("Test case failed", e);

	}
	}

	// Testcase is for verifying validation message for Policy Type Field
	@Test(priority = 2, enabled = execution)
	public void testCase02_checkMessage_PolicyType() throws Exception {
		try{
		logger = report.startTest("Test - Mandatory field Message - Policy Type");
		policySetupPageObject.clickOnAddButton();
		logger.log(LogStatus.INFO, "User clicked on Add button");
		Wait.waitFor(2);
		policySetupPageObject.selectCompany();
		logger.log(LogStatus.INFO, "User selected 'Company'");
		Wait.waitFor(3);
		policySetupPageObject.selectCarrierValue();
		logger.log(LogStatus.INFO, "User selected 'Carrier'");
		Wait.waitFor(3);
		policySetupPageObject.selectClientValue();
		logger.log(LogStatus.INFO, "User selected 'Client'");
		policySetupPageObject.enterEffectiveDateAndExpirationDate();
		logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
		policySetupPageObject.clickOnSaveButton();
		logger.log(LogStatus.INFO, "User clicked on Save button");
		bc.BrowserScrollup();
		Wait.waitFor(2);
		Assert.assertEquals(policySetupPageObject.fetchAllValidationMessages().get(0), "Policy Type is required.","Validaton Message verified for Policy Type field");
		logger.log(LogStatus.PASS, "System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
		
	} catch (Exception e) {
		logger.log(LogStatus.FAIL, e);
		Assert.fail("Test case failed", e);

	}
		}

	// Testcase is for verifying validation message for Effective Date Field
	@Test(priority = 3, enabled = execution)
	public void testCase03_checkMessage_EffectiveDate() throws Exception {
		try {
			logger = report.startTest("Test - Mandatory field Message - Effective Date");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add button");
			Wait.waitFor(2);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "User selected 'Company'");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "User selected 'Carrier'");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "User selected 'Client'");
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "User selected 'Policy type'");
			Wait.waitFor(2);
			policySetupPageObject.enterExpirationDate();
			logger.log(LogStatus.INFO, "User entered 'Expiration Date'");
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicked on Save button");
			bc.BrowserScrollup();
			Wait.waitFor(2);
			Assert.assertEquals("Effective Dt is required.",
					policySetupPageObject.fetchAllValidationMessages().get(0),"Validaton Message verified for Effective Type field" );
			logger.log(LogStatus.PASS, "System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
	@Test(priority = 4, enabled = execution)
	public void testCase04_checkMessage_ExpirationDate() throws Exception {
		try {
			logger = report.startTest("Test - Mandatory field Message - Expiration Date");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add button");
			Wait.waitFor(2);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "User selected 'Company'");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "User selected 'Carrier'");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "User selected 'Client'");
			Wait.waitFor(3);
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "User selected 'Policy type'");
			Wait.waitFor(2);
			policySetupPageObject.enterEffectiveDate();
			logger.log(LogStatus.INFO, "User entered 'Effective Date'");
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicked on Save button");
			bc.BrowserScrollup();
			Wait.waitFor(2);
			Assert.assertEquals("Expiration Dt is required.",
					policySetupPageObject.fetchAllValidationMessages().get(0),"Validaton Message verified for Expiration Type field" );
			logger.log(LogStatus.PASS, "System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
	
	
	// Testcase is for verifying validation message for Retro Active Date Field
	@Test(priority = 5, enabled = execution)
	public void testCase05_checkMessage_RetroActiveDate() throws Exception {
		try {

			logger = report.startTest("Test - Mandatory field Message - Retro Active Date");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add button");
			Wait.waitFor(2);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "User selected 'Company'");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "User selected 'Carrier'");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "User selected 'Client'");
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "User selected 'Policy type'");
			policySetupPageObject.enterEffectiveDateAndExpirationDate();
			logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
			policySetupPageObject.clickSaveOnClaimMade();
			logger.log(LogStatus.INFO, "User clicked on Save button after clicking Claim Made");
			Wait.waitFor(3);
			policySetupPageObject.clickOnSaveButton();
			bc.BrowserScrollup();
			Wait.waitFor(2);
			Assert.assertEquals("Retro Active Date is required.",
					policySetupPageObject.fetchAllValidationMessages().get(0),"Validaton Message verified for Retro Active Date field" );
			logger.log(LogStatus.PASS, "System displayed correct error message: " + policySetupPageObject.fetchAllValidationMessages().get(0));
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
		}
	

	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}
	
	@AfterClass
	public void afterClass() {

		report.flush();
		driver.quit();
	}
}
