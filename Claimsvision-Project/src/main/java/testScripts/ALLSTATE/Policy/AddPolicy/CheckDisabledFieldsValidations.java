package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class CheckDisabledFieldsValidations {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	WebDriver driver;
	BaseClass bc;

	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(5);
		Browser.refresh(driver);
	}

	@Test(priority = 1, enabled = execution)
	public void testCase01_checkDisability_PolicyField() throws InterruptedException {
		try {
			logger = report.startTest("Test-Policy Field is disabled after clicking on Add button");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			if (policySetupPageObject.checkPolicyFieldIsDisabled()) {
				logger.log(LogStatus.PASS, "Policy Field is disabled");
			} else {
				logger.log(LogStatus.FAIL, "Policy Field is enabled");
				Assert.fail();
			}

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 2, enabled = execution)
	public void testCase02_checkDisability_TransactionVersion() throws InterruptedException {
		try {
			logger = report.startTest("Test-Transaction Version Field is disabled after clicking on Add button ");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			if (policySetupPageObject.checkTransactionVersionFieldIsDisabled()) {
				logger.log(LogStatus.PASS, "Transaction Version Field is disabled");
			} else {
				logger.log(LogStatus.FAIL, "Transaction Version Field is enabled");
				Assert.fail();
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 3, enabled = execution)
	public void testCase03_checkDisability_TransactionType() throws InterruptedException {
		try {
			logger = report.startTest("Test-Transaction Type Field is disabled after clicking on Add button");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			if (policySetupPageObject.checkTransactionTypeFieldIsDisabled()) {
				logger.log(LogStatus.PASS, "Transaction Type Field is disabled");
			} else {
				logger.log(LogStatus.FAIL, "Transaction Type is enabled");
				Assert.fail();
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 4, enabled = execution)
	public void testCase04_checkDisability_RenewalVersion() throws InterruptedException {
		try {
			logger = report.startTest("Test-Renewal Version Field is disabled after clicking on Add button ");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			if (policySetupPageObject.checkRenewalVersionFieldIsDisabled()) {
				logger.log(LogStatus.PASS, "Renewal version fiels is disabled");
			} else {
				logger.log(LogStatus.FAIL, "Renewal version fiels is enabled");
				Assert.fail();
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}

	}

	@Test(priority = 5, enabled = execution)
	public void testCase05_checkDisability_VersionEffDate() throws InterruptedException {
		try {
			logger = report.startTest("Test-Version Eff Dt Field is disabled after clicking on Add button ");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			if (policySetupPageObject.checkVersionEffectiveTypeFieldIsDisabled()) {
				logger.log(LogStatus.PASS, "Version Effective field is disabled");
			} else {
				logger.log(LogStatus.PASS, "Version Effective Date is enabled");
				Assert.fail();
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 6, enabled = execution)
	public void testCase06_checkEnability_AllInsuredFields() throws InterruptedException {
		try {
			logger = report.startTest("Test- Insured Fields enabled when clicked checkbox twice");
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "Clicked on Add Button");
			Wait.waitFor(2);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "User selected company as test");
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "User selected a carrier");
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "User selected a client");
			logger.log(LogStatus.INFO, "User checked insured is Client");
			Wait.waitFor(4);
			policySetupPageObject.checkInsuredIsClient();
			logger.log(LogStatus.INFO, "User checked insured is Client again");
			Wait.waitFor(2);
			policySetupPageObject.insuredIsClient.click();
			Wait.waitFor(2);
			if (policySetupPageObject.checkInsuredNameFieldIsEnabled()) {
				logger.log(LogStatus.PASS, "Insured is Client checkox is enabled");
			} else {
				logger.log(LogStatus.FAIL, "Insured is Client checkbox is not enabled");
				Assert.fail();
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 7, enabled = execution)
	public void testCase07_checkAppearance_AllForeignAddressFields() throws InterruptedException {
		try {
			logger = report.startTest("Test- Availablity of foreign add. fields when clicked on foreign add. checkbox");
			Thread.sleep(5000);
			logger.log(LogStatus.INFO, "User clicked on Add Button");

			policySetupPageObject.clickOnAddButton();
			Thread.sleep(3000);
			policySetupPageObject.clickOnForeignAddressCheckBox();
			Thread.sleep(2000);
			policySetupPageObject.enterValuesInForeignAddressFields();
			logger.log(LogStatus.PASS, "User is able to enter foreign values in foreign fields");

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		driver.quit();
	}
	
}
