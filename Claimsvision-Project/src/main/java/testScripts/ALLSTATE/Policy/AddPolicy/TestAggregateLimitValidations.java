package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyAggregateLimit;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class TestAggregateLimitValidations {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyAggregateLimit policyAggregateLimitPageObject;
	WebDriver driver;
	BaseClass bc;
	public static String effectiveDate;
	public static String expirationDate;
	String limit;
	String effDate;
	String expDate;
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();
		policyAggregateLimitPageObject = PageFactory.initElements(driver, PolicyAggregateLimit.class);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);

	}

	
	// Testcase is for verifying validation message on Item Category DropDown on
		// AggregateLimitScreen

		@Test(priority = 1, enabled = execution)
		public void testCase01_checkUserIsRidirectedToAggregateLimitScreen() throws Exception {
			try {
				Browser.refresh(driver);
				Wait.waitFor(3);
				logger = report.startTest("Test- User is redirected to Aggregate Screen");
				dashboardPageObject.navigateToPolicyNumberAssignment();
				logger.log(LogStatus.INFO, "User Navigated to Policy number assignment screen");
				policyNumberAssign.nonComp_PolicyNumberAscending();
				logger.log(LogStatus.INFO, "Configuring Policy Number order");
				policyNumberAssign.nonCompSave.click();
				logger.log(LogStatus.INFO, "User clicked on save button in policy number assignment screen");
				Browser.refresh(driver);
				dashboardPageObject.navigateToPolicySetup();
				logger.log(LogStatus.INFO, "Navigating to Policy Setup Screen");
				policySetupPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policySetupPageObject.selectCompany();
				logger.log(LogStatus.INFO, "User selected company as test");
				Wait.waitFor(3);
				policySetupPageObject.selectCarrierValue();
				logger.log(LogStatus.INFO, "User selected a carrier");
				Wait.waitFor(3);
				policySetupPageObject.selectClientValue();
				logger.log(LogStatus.INFO, "User Selected a client");
				policySetupPageObject.selectPolicyType();
				Wait.waitFor(3);
				logger.log(LogStatus.INFO, "User selected a policy type");
				policySetupPageObject.enterEffectiveDateAndExpirationDate();
				logger.log(LogStatus.INFO, "User entered Effective & Expiration Date");
				effectiveDate = policySetupPageObject.fetchEffectiveDate();
				expirationDate = policySetupPageObject.fetchExpirationDate();
				policySetupPageObject.enterShortPolicyNumber();
				logger.log(LogStatus.INFO, "User entered Short Policy Number");
				policySetupPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicked on Save button");
				policySetupPageObject.goToLastSearchResultScreen();
				String policyNumber = policySetupPageObject.fetchLatestPolicyNumber();
				logger.log(LogStatus.INFO, "User clicked on new policy number");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.clickOnAggregaeIcon();
				logger.log(LogStatus.INFO, "User clicked on Aggregate Icon");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.switchToAggregateScreen();
				Assert.assertEquals(policyAggregateLimitPageObject.policyNumberTextBox.getAttribute("value"), policyNumber);
				logger.log(LogStatus.PASS, "User is redirected to Aggregate Window");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

		// Testcase is for verifying validation message on Item Category DropDown on
		// AggregateLimitScreen

		@Test(priority = 2, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
		public void testCase02_checkMandatoryValidationOnCategoryDropDown() throws Exception {
			try {
				policyAggregateLimitPageObject.refresh();
				logger = report
						.startTest("Test- Test Mandatory field validation message on Category fields on Aggrgate Screen");
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyAggregateLimitPageObject.enterValueInLimit();
				logger.log(LogStatus.INFO, "User enters value in limit");
				policyAggregateLimitPageObject.enterEffectiveAndExpiraitonDate();
				logger.log(LogStatus.INFO, "User enters value in Effective Date and Expiration Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Assert.assertEquals(policyAggregateLimitPageObject.validationMessageArea.getText(),
						"Category is required.");
				logger.log(LogStatus.PASS, "Mandatory Validation for Category is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}
		// Testcase is for verifying validation message on Item Effective Date on
		// AggregateLimitScreen

		@Test(priority = 3, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
		public void testCase03_checkMandatoryValidationOnEffectiveDateOfAggregateLimtScreen() throws Exception {
			try {
				policyAggregateLimitPageObject.refresh();
				logger = report.startTest(
						"Test- Test mandatory field validation message on Effective Date field on Aggrgate Screen");
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyAggregateLimitPageObject.enterCategory(1);
				logger.log(LogStatus.INFO, "User Selects category");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.enterValueInLimit();
				logger.log(LogStatus.INFO, "User enters value in limit");
				policyAggregateLimitPageObject.enterExpirationDate();
				logger.log(LogStatus.INFO, "User enters value in Expiration Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(2);
				Assert.assertEquals(policyAggregateLimitPageObject.validationMessageArea.getText(),
						"Effective Date is required.");
				logger.log(LogStatus.PASS, "Mandatory Validation for Effective Date is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}
		// Testcase is for verifying validation message on Expiration Date on
		// AggregateLimitScreen

		@Test(priority = 4, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
		public void testCase04_checkMandatoryValidationOnExpirationDateOfAggregateLimtScreen() throws Exception {
			try {
				policyAggregateLimitPageObject.refresh();
				logger = report.startTest(
						"Test- Test mandatory field validation message on Expiration Date field on Aggrgate Screen");
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyAggregateLimitPageObject.enterCategory(1);
				logger.log(LogStatus.INFO, "User Selects category");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.enterValueInLimit();
				logger.log(LogStatus.INFO, "User enters value in limit");
				policyAggregateLimitPageObject.enterEffectiveDate();
				logger.log(LogStatus.INFO, "User enters value in Effective Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(2);
				Assert.assertEquals(policyAggregateLimitPageObject.validationMessageArea.getText(),
						"Expiration Date is required.");
				logger.log(LogStatus.PASS, "Mandatory Validation for Expiration Date is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}
		// Testcase is for verifying validation on entering invalid data on
		// aggregate screen

		@Test(priority = 5, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
		public void testCase05_checkValidationMessageOnEnteringInvalidDataInLimitField() throws Exception {
			try {
				policyAggregateLimitPageObject.refresh();
				logger = report.startTest("Test- Test- Validation message on entering invalid data in limit field");
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyAggregateLimitPageObject.enterCategory(1);
				logger.log(LogStatus.INFO, "User Selects category");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.putInvalidDataInLimitField();
				logger.log(LogStatus.INFO, "User enters invalid data in limit");
				policyAggregateLimitPageObject.enterEffectiveAndExpiraitonDate();
				logger.log(LogStatus.INFO, "User enters value in Effective Date & Expiration Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyAggregateLimitPageObject.validationMessageArea.getText(),
						"Aggregate Limits is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in limit field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

		// Testcase is for verifying validation on entering invalid data on
		// effective date screen

		@Test(priority = 6, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
		public void testCase06_checkValidationMessageOnEnteringInvalidDataInEffectiveDateField() throws Exception {
			try {
				policyAggregateLimitPageObject.refresh();
				logger = report.startTest("Test- Validation message on entering invalid data in effective date field");
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyAggregateLimitPageObject.enterCategory(1);
				logger.log(LogStatus.INFO, "User Selects category");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.enterValueInLimit();
				logger.log(LogStatus.INFO, "User enters value in limit");
				policyAggregateLimitPageObject.putInvalidDataInEffectiveDateField();
				logger.log(LogStatus.INFO, "User enters invalid value in effective date field");
				policyAggregateLimitPageObject.enterExpirationDate();
				logger.log(LogStatus.INFO, "User enters value Expiration Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyAggregateLimitPageObject.validationMessageArea.getText(),
						"Effective Date is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in effective date field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

		// Testcase is for verifying validation on entering invalid data on
		// expiration date date screen

		@Test(priority = 7, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
		public void testCase07_checkValidationMessageOnEnteringInvalidDataInExpirationDateField() throws Exception {
			try {
				policyAggregateLimitPageObject.refresh();
				logger = report.startTest(
						"Test- Validation message on entering invalid data in expiration date field on Aggregate Limit Screen");
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyAggregateLimitPageObject.enterCategory(1);
				logger.log(LogStatus.INFO, "User Selects category");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.enterValueInLimit();
				logger.log(LogStatus.INFO, "User enters  data in limit");
				policyAggregateLimitPageObject.enterEffectiveDate();
				logger.log(LogStatus.INFO, "User enters value Effective Date");
				policyAggregateLimitPageObject.putInvalidDataInExpirationDateField();
				logger.log(LogStatus.INFO, "User enters invalid value Expiration date Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyAggregateLimitPageObject.validationMessageArea.getText(),
						"Expiration Date is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in expiration date field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

		// Testcase is for verifying validation on entering value more than limit in
		// limit fields

		@Test(priority = 8, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
		public void testCase08_checkValidationMessageOnEnteringValueMoreThanLimitInLimitField() throws Exception {
			try {
				policyAggregateLimitPageObject.refresh();
				logger = report.startTest(
						"Test- Validation message on entering value more than limit  field on Aggregate Limit Screen");
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyAggregateLimitPageObject.enterCategory(1);
				logger.log(LogStatus.INFO, "User Selects category");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.enterValueInLimitFieldMoreThanLimit();
				logger.log(LogStatus.INFO, "User enters invalid data in limit");
				policyAggregateLimitPageObject.enterEffectiveAndExpiraitonDate();
				logger.log(LogStatus.INFO, "User enters value Effective Date and Expiration Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyAggregateLimitPageObject.validationMessageArea.getText(),
						"Aggregate Limits should be between $0.00 and $922,337,203,685,477.58.");
				logger.log(LogStatus.PASS, "Validation for value more than limit  in limit  field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}
		
		// Testcase is for verifying cancel button working

			@Test(priority = 9, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
			public void testCase09_checkCancelButtonClearsTheValueOfLimit() throws Exception, NumberFormatException {
				try {
					policyAggregateLimitPageObject.refresh();
					logger = report.startTest("Test- Cancel Button Clears the value in the field");
					policyAggregateLimitPageObject.clickOnAddButton();
					logger.log(LogStatus.INFO, "User clicked on Add Button");
					policyAggregateLimitPageObject.enterCategory(1);
					logger.log(LogStatus.INFO, "User Selects category");
					Wait.waitFor(3);
					policyAggregateLimitPageObject.enterValueInLimit();
					logger.log(LogStatus.INFO, "User enters value in limit");
					policyAggregateLimitPageObject.enterEffectiveAndExpiraitonDate();
					logger.log(LogStatus.INFO, "User enters value Effective Date and Expiration Date");
					policyAggregateLimitPageObject.clickOnCancelButton();
					logger.log(LogStatus.INFO, "User clicks on Cancel Button");
					Wait.waitFor(4);

					Assert.assertEquals(policyAggregateLimitPageObject.getSelectedCategory(), "Select one");
					logger.log(LogStatus.PASS, "Value in Category is now cleared");
					try {
						limit = policyAggregateLimitPageObject.fetchLimitValue();

					} catch (NumberFormatException e) {
						Assert.assertEquals(limit, null);
						logger.log(LogStatus.PASS, "Value in Limit is now cleared");
					}

				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e);
					Assert.fail("Test case failed", e);
				}
			}

			// Testcase is for verifying cancel button working

			@Test(priority = 10, enabled = execution, dependsOnMethods = "testCase09_checkCancelButtonClearsTheValueOfLimit")
			public void testCase10_checkCancelButtonClearsTheValueOfEffectiveDate() throws Exception, NumberFormatException {
				try {

					try {
						effDate = policyAggregateLimitPageObject.getEnteredEffectiveDate();
					} catch (NumberFormatException e) {
						Assert.assertEquals(effDate, null);
						System.out.println("2");
						logger.log(LogStatus.PASS, "Value in Effective Date is now cleared");
					}

				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e);
					Assert.fail("Test case failed", e);
				}
			}

			@Test(priority = 11, enabled = execution, dependsOnMethods = "testCase09_checkCancelButtonClearsTheValueOfLimit")
			public void testCase11_checkCancelButtonClearsTheValueOfExpirationDate()
					throws Exception, NumberFormatException {
				try {

					try {
						expDate = policyAggregateLimitPageObject.getEnteredExpirationDate();
					} catch (NumberFormatException e) {
						Assert.assertEquals(expDate, null);
						logger.log(LogStatus.PASS, "Value in Expiration Date is now cleared");
					}

				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e);
					Assert.fail("Test case failed", e);
				}
			}
			
			// Testcase is for verifying Delete button functionality on Aggregate screen

			@Test(priority = 12, enabled = execution, dependsOnMethods = "testCase01_checkUserIsRidirectedToAggregateLimitScreen")
			public void testCase12_checkUserIsNotAbleToEnterEffectiveDateAndExpirationDateOutsidePolicyEffectiveDate() throws Exception {
				try {
					Wait.waitFor(3);
					policyAggregateLimitPageObject.refresh();
					logger = report.startTest("Test- Test User Is Not Able To Enter Effective Date Outside Policy Effective Date And Expiration Date");
					policyAggregateLimitPageObject.clickOnAddButton();
					logger.log(LogStatus.INFO, "User clicked on Add Button");
					Wait.waitFor(3);
					policyAggregateLimitPageObject.enterCategory(1);
					logger.log(LogStatus.INFO, "User Selects category");
					Wait.waitFor(3);
					policyAggregateLimitPageObject.enterValueInLimit();
					logger.log(LogStatus.INFO, "User enters value in limit");
					policyAggregateLimitPageObject.fetchLimitValue();
					policyAggregateLimitPageObject.enterEffectiveDateAndExpirationDateOutsiePolicyEffectiveDate();
					
					logger.log(LogStatus.INFO, "User enters value in Effective Date & Expiration Date");
					policyAggregateLimitPageObject.clickOnSaveButton();
					logger.log(LogStatus.INFO, "User clicks on Save Button");
					Wait.waitFor(4);
					Assert.assertEquals(policyAggregateLimitPageObject.validationMessage.getText(), "Effective Date and Expiration Date are not in between policy's Effective and Expiration Date");
					logger.log(LogStatus.PASS, "Validation verified");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e);
					Assert.fail("Test case failed", e);
				}
			}

			@AfterMethod
			public void tearDown(ITestResult result) throws IOException {
				if (result.getStatus() == ITestResult.FAILURE) {
					TakeScreenshot.takeFailScreen(result.getName(), driver);
					String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
					logger.log(LogStatus.FAIL, "Title verification", image);

					report.endTest(logger);

				}
			}

			@AfterClass
			public void afterClass() {

				report.flush();
				// driver.quit();
			}
	
}

