package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyAggregateLimit;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class TestPolicyAggregateLimit {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyAggregateLimit policyAggregateLimitPageObject;
	WebDriver driver;
	BaseClass bc;
	public static String effectiveDate;
	public static String expirationDate;
	String limit;
	String effDate;
	String expDate;
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();
		policyAggregateLimitPageObject = PageFactory.initElements(driver, PolicyAggregateLimit.class);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);

	}
	
	// Testcase is for verifying Add button functionality on Aggregate screen

		@Test(priority = 1, enabled = execution)
		public void testCase01_checkAddButtonFunctionality() throws Exception {
			try {
				Browser.refresh(driver);
				Wait.waitFor(3);
				logger = report.startTest("Test- Add button functionality on Aggregate Screen");
				dashboardPageObject.navigateToPolicyNumberAssignment();
				logger.log(LogStatus.INFO, "User Navigated to Policy number assignment screen");
				policyNumberAssign.nonComp_PolicyNumberAscending();
				logger.log(LogStatus.INFO, "Configuring Policy Number order");
				policyNumberAssign.nonCompSave.click();
				logger.log(LogStatus.INFO, "User clicked on save button in policy number assignment screen");
				Browser.refresh(driver);
				dashboardPageObject.navigateToPolicySetup();
				logger.log(LogStatus.INFO, "Navigating to Policy Setup Screen");
				policySetupPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policySetupPageObject.selectCompany();
				logger.log(LogStatus.INFO, "User selected company as test");
				Wait.waitFor(3);
				policySetupPageObject.selectCarrierValue();
				logger.log(LogStatus.INFO, "User selected a carrier");
				Wait.waitFor(3);
				policySetupPageObject.selectClientValue();
				logger.log(LogStatus.INFO, "User Selected a client");
				policySetupPageObject.selectPolicyType();
				Wait.waitFor(3);
				logger.log(LogStatus.INFO, "User selected a policy type");
				policySetupPageObject.enterEffectiveDateAndExpirationDate();
				logger.log(LogStatus.INFO, "User entered Effective & Expiration Date");
				effectiveDate = policySetupPageObject.fetchEffectiveDate();
				expirationDate = policySetupPageObject.fetchExpirationDate();
				policySetupPageObject.enterShortPolicyNumber();
				logger.log(LogStatus.INFO, "User entered Short Policy Number");
				policySetupPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicked on Save button");
				policySetupPageObject.goToLastSearchResultScreen();
				/*policySetupPageObject.fetchLatestPolicyNumber();
				logger.log(LogStatus.INFO, "User clicked on new policy number");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.clickOnAggregaeIcon();
				logger.log(LogStatus.INFO, "User clicked on Aggregate Icon");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.switchToAggregateScreen();*/
				
				Wait.waitFor(11);
				   policySetupPageObject.fetchLatestPolicyNumber();
				   logger.log(LogStatus.INFO, "User clicked on new policy number");
				   Wait.waitFor(20);

				   policyAggregateLimitPageObject.clickOnAggregaeIcon();
					logger.log(LogStatus.INFO, "User clicked on Aggregate Icon");
					Wait.waitFor(10);
					policyAggregateLimitPageObject.switchToAggregateScreen();
					   Wait.waitFor(4);
				policyAggregateLimitPageObject.refresh();
				policyAggregateLimitPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.enterCategory(1);
				logger.log(LogStatus.INFO, "User Selects category");
				Wait.waitFor(3);
				policyAggregateLimitPageObject.enterValueInLimit();
				logger.log(LogStatus.INFO, "User enters value in limit");
				String enteredLimit = policyAggregateLimitPageObject.fetchLimitValue();
				policyAggregateLimitPageObject.enterEffectiveAndExpiraitonDate();
				logger.log(LogStatus.INFO, "User enters value in Effective Date & Expiration Date");
				policyAggregateLimitPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyAggregateLimitPageObject.fetchLatestValueFromTable(1), enteredLimit);
				logger.log(LogStatus.PASS, "New Aggreate Limit has been added");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

	
	// Testcase is for verifying Edit button functionality on Aggregate screen

	@Test(priority = 2, enabled = execution, dependsOnMethods = "testCase01_checkAddButtonFunctionality")
	public void testCase02_checkEditButtonFunctionality() throws Exception {
		try {
			Wait.waitFor(3);
			policyAggregateLimitPageObject.refresh();
			logger = report.startTest("Test- Edit button functionality on Aggregate Screen");
			policyAggregateLimitPageObject.clickOnModifyButton();
			logger.log(LogStatus.INFO, "User clicked on Modify Button");
			Wait.waitFor(3);
			policyAggregateLimitPageObject.enterCategory(2);
			logger.log(LogStatus.INFO, "User Selects category");
			Wait.waitFor(3);
			String category = policyAggregateLimitPageObject.getSelectedCategory();
			policyAggregateLimitPageObject.enterValueInLimit();
			logger.log(LogStatus.INFO, "User enters value in limit");
			String enteredLimit = policyAggregateLimitPageObject.fetchLimitValue();
			policyAggregateLimitPageObject.enterEffectiveDateAgain();
			policyAggregateLimitPageObject.enterExpirationDateAgain();
			logger.log(LogStatus.INFO, "User enters value in Effective Date & Expiration Date");
			String effectiveDate = policyAggregateLimitPageObject.getEnteredEffectiveDate();
			String expirationDate = policyAggregateLimitPageObject.getEnteredExpirationDate();
			policyAggregateLimitPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(4);
			Assert.assertEquals(policyAggregateLimitPageObject.fetchLatestValueFromTable(0), category);
			logger.log(LogStatus.PASS, "Aggreate Category has been updated");
			Assert.assertEquals(policyAggregateLimitPageObject.fetchLatestValueFromTable(1), enteredLimit);
			logger.log(LogStatus.PASS, "Aggreate Limit has been updated");
			Assert.assertEquals(policyAggregateLimitPageObject.fetchLatestValueFromTable(2),
					policyAggregateLimitPageObject.changeTheFormatOfDate(effectiveDate) + " " + "12:00:00 AM");
			logger.log(LogStatus.PASS, "Aggreate Effective Date has been updated");
			Assert.assertEquals(policyAggregateLimitPageObject.fetchLatestValueFromTable(3),
					policyAggregateLimitPageObject.changeTheFormatOfDate(expirationDate) + " " + "12:00:00 AM");
			logger.log(LogStatus.PASS, "Aggreate Expiration Date has been updated");

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// Testcase is for verifying Delete button functionality on Aggregate screen

	@Test(priority = 3, enabled = execution, dependsOnMethods = "testCase01_checkAddButtonFunctionality")
	public void testCase03_checkDeleteButtonFunctionality() throws Exception {
		try {
			Wait.waitFor(3);
			policyAggregateLimitPageObject.refresh();
			logger = report.startTest("Test- Delete button functionality on Aggregate Screen");
			policyAggregateLimitPageObject.clickOnModifyButton();
			logger.log(LogStatus.INFO, "User clicked on Modify Button");
			Wait.waitFor(3);
			policyAggregateLimitPageObject.clickDeleteButton();
			logger.log(LogStatus.INFO, "User clicks on Delete Button");
			Wait.waitFor(4);
			Assert.assertEquals(policyAggregateLimitPageObject.validationMessageAreaForNoSearchResults.getText(),
					"No results returned.");
			logger.log(LogStatus.PASS, "Delete Button functionality is verified");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
				

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		// driver.quit();
	}

	
}
