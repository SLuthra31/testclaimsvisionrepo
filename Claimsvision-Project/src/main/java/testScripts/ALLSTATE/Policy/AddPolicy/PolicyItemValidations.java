package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class PolicyItemValidations {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	WebDriver driver;
	BaseClass bc;
	
	String effDate;
	String expDate;
	
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();
		policyItem=PageFactory.initElements(driver, PolicyItem.class);
		
	}
	
	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);
		

	}

	// Testcase is for verifying validation message on Item Category DropDown on AggregateLimitScreen
	
	@Test(priority = 1, enabled = execution)
	public void testCase01_checkUserGetRedirectedToPolicyItemScreen() throws Exception {
		try {
			Browser.refresh(driver);
			Wait.waitFor(3);
			   logger = report.startTest("Test - Non Comp Policy Creation");
			   dashboardPageObject.navigateToPolicyNumberAssignment();
			   logger.log(LogStatus.INFO, "Test Naviagtion - Policy Number Assignment");
			   policyNumberAssign.nonComp_PolicyNumberAscending();
			   logger.log(LogStatus.INFO, "User configuring the Policy Number");
			   policyNumberAssign.nonCompSave.click();
			   logger.log(LogStatus.INFO, "User clicked on Save button to save configuration");
			   Browser.refresh(driver);
			   dashboardPageObject.navigateToPolicySetup();
			   logger.log(LogStatus.INFO, "Test Navigation - Policy Setup");
			   Wait.waitFor(2);
			   policySetupPageObject.clickOnAddButton();
			   logger.log(LogStatus.INFO, "User clicked on Add button");
			   Wait.waitFor(3);
			   policySetupPageObject.selectCompany();
			   logger.log(LogStatus.INFO, "User selected 'Company'");
			   Wait.waitFor(3);
			   policySetupPageObject.selectCarrierValue();
			   logger.log(LogStatus.INFO, "User selected 'Carrier'");
			   Wait.waitFor(3);
			   policySetupPageObject.selectClientValue();
			   logger.log(LogStatus.INFO, "User selected 'Client'");
			   Wait.waitFor(3);
			   policySetupPageObject.selectPolicyType();
			   logger.log(LogStatus.INFO, "User selected 'Policy type'");
			   Wait.waitFor(3);
			   policySetupPageObject.enterEffectiveDateAndExpirationDate();
			   logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
			      
			Wait.waitFor(3);
			policySetupPageObject.enterShortPolicyNumber();
			logger.log(LogStatus.INFO, "User entered 'Short Policy number'");
			   
//			policySetupPageObject.enterRetroActiveDate();
//			logger.log(LogStatus.INFO, "User entered Retro Active Date");
			 
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicked on Save button");
			policySetupPageObject.goToLastSearchResultScreen();
			   Wait.waitFor(11);
			   String policyNumber = policySetupPageObject.fetchLatestPolicyNumber();
			   logger.log(LogStatus.INFO, "User clicked on new policy number");
			   Wait.waitFor(20);

			   policyItem.clickPolicyItem();
			   logger.log(LogStatus.INFO, "User clicked on Policy Item Icon");
			   Wait.waitFor(10);
			   policyItem.switchToPolicyItemScreen();
			   Wait.waitFor(4);
			   Assert.assertEquals(policyItem.policyNumberTextBox.getAttribute("value"), policyNumber);
			   logger.log(LogStatus.INFO, "User is redirected to Policy Item Window");		
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}		

	@Test(priority = 2, enabled = execution,dependsOnMethods="testCase01_checkUserGetRedirectedToPolicyItemScreen")
	public void testCase02_checkMandatoryValidationOnItemIdField() throws Exception {
		try {
			
			policyItem.refresh();
			logger = report.startTest("Test- Check Validation Message on Item Id field");
			policyItem.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			policyItem.enterItemName();
			logger.log(LogStatus.INFO, "User enters Item Name");
			Wait.waitFor(5);
			policyItem.selectItemCategory();
			logger.log(LogStatus.INFO, "User selected Item Category");
			policyItem.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(2);
			Assert.assertEquals(policyItem.validationMessageArea.getText(), "Item Id is required.");
			logger.log(LogStatus.PASS, "Mandatory Validation for Item Id is verified");
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
	
	@Test(priority = 3, enabled = execution,dependsOnMethods="testCase01_checkUserGetRedirectedToPolicyItemScreen")
	public void testCase03_checkMandatoryValidationOnItemNameField() throws Exception {
		try {
			policyItem.refresh();
			logger = report.startTest("Test- Check Validation Message on Item Name field");
			policyItem.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			policyItem.enterItemId();
			logger.log(LogStatus.INFO, "User enters Item Id");
			policyItem.selectItemCategory();
			logger.log(LogStatus.INFO, "User selected Item Category");
			policyItem.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(2);
			Assert.assertEquals(policyItem.validationMessageArea.getText(), "Item Name is required.");
			logger.log(LogStatus.PASS, "Mandatory Validation for Item Name is verified");
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
	
	@Test(priority = 4, enabled = execution,dependsOnMethods="testCase01_checkUserGetRedirectedToPolicyItemScreen")
	public void testCase04_checkMandatoryValidationOnItemCategoryField() throws Exception {
		try {
			policyItem.refresh();
			logger = report.startTest("Test- Check Validation Message on Item Category field");
		/*	policyItem.clickCancelPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Cancel button");	*/		
			policyItem.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			policyItem.enterItemId();
			logger.log(LogStatus.INFO, "User enters Item Id");
			policyItem.enterItemName();
			logger.log(LogStatus.INFO, "User enters Item Name");
			policyItem.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(2);
			Assert.assertEquals(policyItem.validationMessageArea.getText(), "Item Category is required.");
			logger.log(LogStatus.PASS, "Mandatory Validation for Item Category is verified");
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
	
	
	//-----------------------//

		

		

		@Test(priority = 5, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemScreen")
		public void testCase05_checkValidationMessageOnEnteringInvalidDataInItemYear() throws Exception {
			try {
				policyItem.refresh();
				logger = report.startTest("Test- Validation message on entering invalid data in Item Year field");
				policyItem.clickAddPolicyItem();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				Wait.waitFor(2);
				policyItem.enterItemId();
				logger.log(LogStatus.INFO, "User enters value on Item Id field");
				Wait.waitFor(3);
				policyItem.enterItemName();
				logger.log(LogStatus.INFO, "User enters value in Item Name");
				policyItem.enterInvalidValueInItemYear();
				logger.log(LogStatus.INFO, "User enters invalid value in Item Year");
				policyItem.selectItemCategory();
				logger.log(LogStatus.INFO, "User enters select category");
				policyItem.clickSavePolicyItem();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyItem.validationMessageArea.getText(),
						"Item Year is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in Item Year field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

	

		@Test(priority = 6, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemScreen")
		public void testCase06_checkValidationMessageOnEnteringInvalidDataInItemValueField() throws Exception {
			try {
				policyItem.refresh();
				logger = report.startTest(
						"Test- Validation message on entering invalid data in Item Year field");
				policyItem.clickAddPolicyItem();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				Wait.waitFor(2);
				policyItem.enterItemId();
				logger.log(LogStatus.INFO, "User enters Value in Item Id");
				Wait.waitFor(3);
				policyItem.enterItemName();
				logger.log(LogStatus.INFO, "User enters  data in Item Name");
				policyItem.selectItemCategory();
				logger.log(LogStatus.INFO, "User selects Item Category");
				policyItem.enterInvalidValueInItemValue();
				logger.log(LogStatus.INFO, "User enters invalid value in Item Value field");
				policyItem.clickSavePolicyItem();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyItem.validationMessageArea.getText(),
						"Item Value is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in Item Value field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

		
		
		// Testcase is for verifying cancel button working

			@Test(priority = 7, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemScreen")
			public void testCase07_checkCancelButtonClearsTheValue() throws Exception, NumberFormatException {
				try {
					policyItem.refresh();
					logger = report.startTest("Test- Cancel Button Clears the value in the Item ID field");
					policyItem.clickAddPolicyItem();
					logger.log(LogStatus.INFO, "User clicked on Add Button");
					Wait.waitFor(2);
					policyItem.enterItemId();
					logger.log(LogStatus.INFO, "User enters Value in Item Id");
					Wait.waitFor(3);
					policyItem.clickCancelPolicyItem();
					logger.log(LogStatus.INFO, "User clicks on Cancel Button");
					Wait.waitFor(4);
					Assert.assertTrue(policyItem.itemIdIsDisabled());
					logger.log(LogStatus.PASS, "Value in Item ID is now cleared");
					

				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e);
					Assert.fail("Test case failed", e);
				}
			}

			
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		 driver.quit();
	}
	
}
