package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class PolicyItemCoveragesValidations {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;
	WebDriver driver;
	BaseClass bc;
	public static String effectiveDate;
	public static String expirationDate;
	String limit;
	String effDate;
	String expDate;
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();
		policyItem = PageFactory.initElements(driver, PolicyItem.class);
		policyItemCoverges = PageFactory.initElements(driver, PolicyItemCoverages.class);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);

	}

	// Testcase is for verifying Add button functionality on PolicyItem screen

	@Test(priority = 1, enabled = execution)
	public void testCase01_checkAddButtonFunctionality() throws Exception {
		try {
			Browser.refresh(driver);
			Wait.waitFor(3);
			logger = report.startTest("Test - Non Comp Policy Coverage Creation");
			dashboardPageObject.navigateToPolicyNumberAssignment();
			logger.log(LogStatus.INFO, "Test Naviagtion - Policy Number Assignment");
			policyNumberAssign.nonComp_PolicyNumber_NotConfiguredAllFields();
			logger.log(LogStatus.INFO, "User configuring the Policy Number");
			policyNumberAssign.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on Save button to save configuration");
			Browser.refresh(driver);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Test Navigation - Policy Setup");
			Wait.waitFor(2);
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add button");
			Wait.waitFor(3);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "User selected 'Company'");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "User selected 'Carrier'");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "User selected 'Client'");
			Wait.waitFor(3);
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "User selected 'Policy type'");
			Wait.waitFor(3);
			policySetupPageObject.enterEffectiveDateAndExpirationDate();
			logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
			effDate = policySetupPageObject.fetchEffectiveDate();
			/*
			 * policySetupPageObject.checkInsuredIsClient();
			 * logger.log(LogStatus.INFO,
			 * "User selected 'Insured is Client' checkbox");
			 */
			Wait.waitFor(3);
			policySetupPageObject.enterShortPolicyNumber();
			logger.log(LogStatus.INFO, "User entered 'Short Policy number'");
			policySetupPageObject.enterPremium();
			logger.log(LogStatus.INFO, "User entered 'Premium'");
			policySetupPageObject.enterDeductible();
			logger.log(LogStatus.INFO, "User entered 'Deductible'");
			policySetupPageObject.enterAgency();
			logger.log(LogStatus.INFO, "User entered 'Agency'");
			policySetupPageObject.enterAgent();
			logger.log(LogStatus.INFO, "User selected 'Agent'");
			Wait.waitFor(3);
			policySetupPageObject.enterPolicyYear();
			logger.log(LogStatus.INFO, "User entered 'Policy year'");
			policySetupPageObject.enterAltPolicy1();
			logger.log(LogStatus.INFO, "User entered 'Alt Policy #'");
			policySetupPageObject.enterAltPolicy2();
			logger.log(LogStatus.INFO, "User entered 'Alt policy #2'");
			policySetupPageObject.enterDomicileState();
			logger.log(LogStatus.INFO, "User entered 'Domicile State'");

			/*
			 * policySetupPageObject.enterRetroActiveDate();
			 * logger.log(LogStatus.INFO, "User entered Retro Active Date");
			 */ Wait.waitFor(3);
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicked on Save button");
			bc.BrowserScrollup();
			Wait.waitFor(5);
			String policyNum = policySetupPageObject.getExpectedPolicyNumber();
			System.out.println("Expected: " + policyNum);
			// bc.BrowserScrollup();
			Wait.waitFor(2);
			policySetupPageObject.goToLastSearchResultScreen();
			Wait.waitFor(10);
			logger.log(LogStatus.INFO, "Search for latest policy in search result");
			String actual=policySetupPageObject.fetchLatestPolicyNumber();
			Assert.assertEquals(actual, policyNum);

			System.out.println("Actual: " + actual);
			logger.log(LogStatus.INFO, "User clicked on new policy number");
			Wait.waitFor(15);

			policyItem.clickPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Policy Item Icon");
			Wait.waitFor(3);
			policyItem.switchToPolicyItemScreen();
			Wait.waitFor(2);
			policyItem.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(5);
			policyItem.enterItemName();
			logger.log(LogStatus.INFO, "User enters Item Name");
			Wait.waitFor(5);
			policyItem.enterItemId();
			logger.log(LogStatus.INFO, "User enters Item Id");
			policyItem.selectItemCategory();
			logger.log(LogStatus.INFO, "User selected Item Category");
			policyItem.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(4);
			policyItem.clickOnModifyButton();
			logger.log(LogStatus.INFO, "User clicked on Modify Button");
			Wait.waitFor(3);
			policyItemCoverges.clickOnCoverageIcon();
			logger.log(LogStatus.INFO, "User clicked on Coverage   Icon");
			Wait.waitFor(3);
			policyItemCoverges.switchToCoverageScreen();
			Wait.waitFor(3);
			policyItemCoverges.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(3);
			policyItemCoverges.selectCoverage(1);
			logger.log(LogStatus.INFO, "User enters Coverage Id");
			policyItemCoverges.enterDataInDeductible();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User enters Deductible ");
			policyItemCoverges.enterDataInLimitPerPerson();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User enters Limit Per" + "Person Id");
			policyItemCoverges.enterDataInLimitPerOccurence();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User enters Limit Per Occurence ");
			policyItemCoverges.enterDataInClaimReportingLevel();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User enters Claim Reporting Level ");
			Wait.waitFor(3);
			policyItemCoverges.enterDataInPremium();
			logger.log(LogStatus.INFO, "User enters Premium");
			Wait.waitFor(3);
			policyItemCoverges.clickSavePolicyItem();
			Wait.waitFor(3);
			String coverage = policyItemCoverges.fetchCoverage();
			Assert.assertEquals(policyItemCoverges.fetchCoveragesFromTable(0), coverage);
			logger.log(LogStatus.PASS, "New coverage has been added");

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 2, enabled = execution, dependsOnMethods = "testCase01_checkAddButtonFunctionality")
	public void testCase02_checkEditButtonFunctionality() throws Exception {
		try {
			Wait.waitFor(3);
			policyItemCoverges.refresh();
			logger = report.startTest("Test- Edit button functionality on Coverage  Screen");
			policyItemCoverges.clickModfyCoverage();
			logger.log(LogStatus.INFO, "User clicked on Modify Button");
			Wait.waitFor(3);
			policyItemCoverges.selectCoverage(2);
			logger.log(LogStatus.INFO, "User enters Coverage");
			String coverage = policyItemCoverges.fetchCoverage();
			policyItemCoverges.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(4);
			Assert.assertEquals(policyItemCoverges.fetchCoveragesFromTable(0), coverage);
			logger.log(LogStatus.PASS, "Coverage has been updated");

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 3, enabled = execution, dependsOnMethods = "testCase01_checkAddButtonFunctionality")
	public void testCase03_checkDeleteButtonFunctionality() throws Exception {
		try {
			Wait.waitFor(3);
			policyItemCoverges.refresh();
			logger = report.startTest("Test- Delete button functionality on Coverage Screen");
			policyItemCoverges.clickModfyCoverage();
			logger.log(LogStatus.INFO, "User clicked on Modify Button");
			Wait.waitFor(3);
			policyItemCoverges.clickDeleteButton();
			logger.log(LogStatus.INFO, "User clicks on Delete Button");
			Wait.waitFor(4);
			Assert.assertEquals(policyItem.validationMessageAreaForNoSearchResults.getText(), "No results returned.");
			logger.log(LogStatus.PASS, "Delete Button functionality is verified");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		 driver.quit();
	}

}
