package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;

import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class CreatePolicyTestScripts {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	WebDriver driver;
	BaseClass bc;
	String effDate;
	String effectiveDate;
	String expirationDate;

	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);

	}

	// This test case is for checking add policy validation
	@Test(priority = 1, enabled = execution)
	public void testCase01_checkAddPolicy_PositiveValidation() throws Exception {

		try {
			Browser.refresh(driver);
			Wait.waitFor(3);
			logger = report.startTest("Test - Non Comp Policy Creation");
			dashboardPageObject.navigateToPolicyNumberAssignment();
			logger.log(LogStatus.INFO, "Test Naviagtion - Policy Number Assignment");
			policyNumberAssign.nonComp_PolicyNumber_NotConfiguredAllFields();
			logger.log(LogStatus.INFO, "User configuring the Policy Number");
			policyNumberAssign.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on Save button to save configuration");
			Browser.refresh(driver);
			dashboardPageObject.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Test Navigation - Policy Setup");
			Wait.waitFor(2);
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add button");
			Wait.waitFor(3);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "User selected 'Company'");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "User selected 'Carrier'");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "User selected 'Client'");
			Wait.waitFor(3);
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "User selected 'Policy type'");
			Wait.waitFor(3);
			policySetupPageObject.enterEffectiveDateAndExpirationDate();
			logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
			effDate = policySetupPageObject.fetchEffectiveDate();
			/*
			 * policySetupPageObject.checkInsuredIsClient();
			 * logger.log(LogStatus.INFO,
			 * "User selected 'Insured is Client' checkbox");
			 */
			Wait.waitFor(3);
			policySetupPageObject.enterShortPolicyNumber();
			logger.log(LogStatus.INFO, "User entered 'Short Policy number'");
			policySetupPageObject.enterPremium();
			logger.log(LogStatus.INFO, "User entered 'Premium'");
			policySetupPageObject.enterDeductible();
			logger.log(LogStatus.INFO, "User entered 'Deductible'");
			policySetupPageObject.enterAgency();
			logger.log(LogStatus.INFO, "User entered 'Agency'");
			policySetupPageObject.enterAgent();
			logger.log(LogStatus.INFO, "User selected 'Agent'");
			Wait.waitFor(3);
			policySetupPageObject.enterPolicyYear();
			logger.log(LogStatus.INFO, "User entered 'Policy year'");
			policySetupPageObject.enterAltPolicy1();
			logger.log(LogStatus.INFO, "User entered 'Alt Policy #'");
			policySetupPageObject.enterAltPolicy2();
			logger.log(LogStatus.INFO, "User entered 'Alt policy #2'");
			policySetupPageObject.enterDomicileState();
			logger.log(LogStatus.INFO, "User entered 'Domicile State'");

			/*
			 * policySetupPageObject.enterRetroActiveDate();
			 * logger.log(LogStatus.INFO, "User entered Retro Active Date");
			 */ Wait.waitFor(3);
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicked on Save button");
			bc.BrowserScrollup();
			Wait.waitFor(5);
			String policyNum = policySetupPageObject.getExpectedPolicyNumber();
			System.out.println("Expected: " + policyNum);
			// bc.BrowserScrollup();
			Wait.waitFor(2);
			policySetupPageObject.goToLastSearchResultScreen();
			Wait.waitFor(10);
			logger.log(LogStatus.INFO, "Search for latest policy in search result");
			Assert.assertEquals(policySetupPageObject.fetchLatestPolicyNumber(), policyNum);
			Wait.waitFor(15);

			System.out.println("Actual: " + policySetupPageObject.fetchLatestPolicyNumber());
			logger.log(LogStatus.PASS, "User successfully created Non Comp Policy");

		} catch (Exception e) {

			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);

		}

	}

	// This test case is for checking Renewal Version value after creating
	// policy
	@Test(enabled = execution, dependsOnMethods = "testCase01_checkAddPolicy_PositiveValidation")
	public void testCase01_01_checkRenewalPolicyAfterCreatingPolicy() throws Exception {

		try {
			logger = report.startTest("Test - Verifying Renewal Version");
			logger.log(LogStatus.INFO, "Searching for latest created Policy");
			logger.log(LogStatus.INFO, "Latest Policy selected from Search Results");
			Assert.assertEquals(policySetupPageObject.fetchRenewalVersion(), "00");
			logger.log(LogStatus.PASS, "Renewal Version is verified: 00");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// This test case is for checking transaction type value after creating
	// policy
	@Test( enabled = execution, dependsOnMethods = "testCase01_checkAddPolicy_PositiveValidation")
	public void testCase01_02_checkTransactionTypeAfterCreatingPolicy() throws Exception {

		try {
			logger = report.startTest("Test - Verifying Transaction Type");
			logger.log(LogStatus.INFO, "Searching for latest created Policy");
			logger.log(LogStatus.INFO, "Latest Policy selected from Search Results");
			Assert.assertEquals(policySetupPageObject.fetchTransactionType(), "New");
			logger.log(LogStatus.PASS, "Transaction Type is verified: New");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// This test case is for checking Version Effective Date value after
	// creating policy
	@Test( enabled = execution, dependsOnMethods = "testCase01_checkAddPolicy_PositiveValidation")
	public void testCase01_03_checkVersionEffectiveDateAfterCreatingPolicy() throws Exception {

		try {
			logger = report.startTest("Test - Verifying Version Effective Date");
			logger.log(LogStatus.INFO, "Searching for latest created Policy");
			logger.log(LogStatus.INFO, "Latest Policy selected from Search Results");
			Assert.assertEquals(policySetupPageObject.fetchVersionEffectiveDate(), effDate);
			logger.log(LogStatus.PASS, "Version Effective Date is verified: " + effDate);
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	// This test case is for checking Transaction version value after
	// creating policy
	@Test(enabled = execution, dependsOnMethods = "testCase01_checkAddPolicy_PositiveValidation")
	public void testCase01_04_checktransactionVersionAfterCreatingPolicy() throws Exception {

		try {
			logger = report.startTest("Test - Verifying Transaction Version");
			logger.log(LogStatus.INFO, "Searching for latest created Policy");
			logger.log(LogStatus.INFO, "Latest Policy selected from Search Results");
			Assert.assertEquals(policySetupPageObject.fetchTransactionVersion(), "0");
			logger.log(LogStatus.PASS, "Transaction Version is verified: 0");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		driver.quit();
	}

}
