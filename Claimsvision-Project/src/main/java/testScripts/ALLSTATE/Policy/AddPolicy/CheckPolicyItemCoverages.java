package testScripts.ALLSTATE.Policy.AddPolicy;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class CheckPolicyItemCoverages {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	PolicySetUp policySetupPageObject;
	Dashboard dashboardPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;
	WebDriver driver;
	BaseClass bc;
	String effDate;
	
	public final boolean execution = true;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboardPageObject = PageFactory.initElements(driver, Dashboard.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		policySetupPageObject = loginPageObjects.Login(14).navigateToPolicySetup();
		policyItem=PageFactory.initElements(driver, PolicyItem.class);
		policyItemCoverges = PageFactory.initElements(driver, PolicyItemCoverages.class);
	}
	
	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Wait.waitFor(3);
		

	}


	
	@Test(priority = 1, enabled = execution)
	public void testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen() throws Exception {
		try {
				Browser.refresh(driver);
				Wait.waitFor(3);
				logger = report.startTest("Test - Non Comp Policy Creation");
				dashboardPageObject.navigateToPolicyNumberAssignment();
				logger.log(LogStatus.INFO, "Test Naviagtion - Policy Number Assignment");
				policyNumberAssign.nonComp_PolicyNumber_NotConfiguredAllFields();
				logger.log(LogStatus.INFO, "User configuring the Policy Number");
				policyNumberAssign.nonCompSave.click();
				logger.log(LogStatus.INFO, "User clicked on Save button to save configuration");
				Browser.refresh(driver);
				dashboardPageObject.navigateToPolicySetup();
				logger.log(LogStatus.INFO, "Test Navigation - Policy Setup");
				Wait.waitFor(2);
				policySetupPageObject.clickOnAddButton();
				logger.log(LogStatus.INFO, "User clicked on Add button");
				Wait.waitFor(3);
				policySetupPageObject.selectCompany();
				logger.log(LogStatus.INFO, "User selected 'Company'");
				Wait.waitFor(3);
				policySetupPageObject.selectCarrierValue();
				logger.log(LogStatus.INFO, "User selected 'Carrier'");
				Wait.waitFor(3);
				policySetupPageObject.selectClientValue();
				logger.log(LogStatus.INFO, "User selected 'Client'");
				Wait.waitFor(3);
				policySetupPageObject.selectPolicyType();
				logger.log(LogStatus.INFO, "User selected 'Policy type'");
				Wait.waitFor(3);
				policySetupPageObject.enterEffectiveDateAndExpirationDate();
				logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
				effDate = policySetupPageObject.fetchEffectiveDate();
				/*policySetupPageObject.checkInsuredIsClient();
				logger.log(LogStatus.INFO, "User selected 'Insured is Client' checkbox");
				*/
				Wait.waitFor(3);
				policySetupPageObject.enterShortPolicyNumber();
				logger.log(LogStatus.INFO, "User entered 'Short Policy number'");
				policySetupPageObject.enterPremium();
				logger.log(LogStatus.INFO, "User entered 'Premium'");
				policySetupPageObject.enterDeductible();
				logger.log(LogStatus.INFO, "User entered 'Deductible'");
				policySetupPageObject.enterAgency();
				logger.log(LogStatus.INFO, "User entered 'Agency'");
				policySetupPageObject.enterAgent();
				logger.log(LogStatus.INFO, "User selected 'Agent'");
				Wait.waitFor(3);
				policySetupPageObject.enterPolicyYear();
				logger.log(LogStatus.INFO, "User entered 'Policy year'");
				policySetupPageObject.enterAltPolicy1();
				logger.log(LogStatus.INFO, "User entered 'Alt Policy #'");
				policySetupPageObject.enterAltPolicy2();
				logger.log(LogStatus.INFO, "User entered 'Alt policy #2'");
				policySetupPageObject.enterDomicileState();
				logger.log(LogStatus.INFO, "User entered 'Domicile State'");
				
				/*policySetupPageObject.enterRetroActiveDate();
				logger.log(LogStatus.INFO, "User entered Retro Active Date");
			*/	Wait.waitFor(3);
				policySetupPageObject.clickOnSaveButton();
				logger.log(LogStatus.INFO, "User clicked on Save button");
				bc.BrowserScrollup();
				Wait.waitFor(5);
				String policyNum = policySetupPageObject.getExpectedPolicyNumber();
				System.out.println("Expected: "+ policyNum);
				//bc.BrowserScrollup();
				Wait.waitFor(2);
				policySetupPageObject.goToLastSearchResultScreen();
				Wait.waitFor(10);
				logger.log(LogStatus.INFO, "Search for latest policy in search result");
				Assert.assertEquals(policySetupPageObject.fetchLatestPolicyNumber(),
						policyNum);
				 logger.log(LogStatus.INFO, "User clicked on new policy number");
				Wait.waitFor(15);		
			   policyItem.clickPolicyItem();
			   Wait.waitFor(2);
			   logger.log(LogStatus.INFO, "User clicked on Policy Item Icon");
			   Wait.waitFor(2);
			   policyItem.switchToPolicyItemScreen();
			   Wait.waitFor(4);
			policyItem.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			policyItem.enterItemName();
			logger.log(LogStatus.INFO, "User enters Item Name");
			Wait.waitFor(2);
			String itemName=policyItem.fetchItemName();
			policyItem.selectItemCategory();
			logger.log(LogStatus.INFO, "User selected Item Category");
			policyItem.enterItemId();
			logger.log(LogStatus.INFO, "User enters Item Id");
			policyItem.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			policyItem.clickOnModifyButton();
			logger.log(LogStatus.INFO, "User clicked on Modify Button");
			Wait.waitFor(3);
			policyItemCoverges.clickOnCoverageIcon();
			logger.log(LogStatus.INFO, "User clicked on Coverage   Icon");
			Wait.waitFor(3);
			policyItemCoverges.switchToCoverageScreen();
			Assert.assertEquals(policyItemCoverges.fetchItemName(), itemName);
			logger.log(LogStatus.INFO, "User is redirected to Coverage Window");
			
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}		

	@Test(priority = 2, enabled = execution,dependsOnMethods="testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase02_checkMandatoryValidationOnCoverageField() throws Exception {
		try {
			
			policyItemCoverges.refresh();
			logger = report.startTest("Test- Check Validation Message on Coverage field");
			Wait.waitFor(2);
			policyItemCoverges.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			policyItemCoverges.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Assert.assertEquals(policyItemCoverges.validationMessageArea.getText(), "Coverage is required.");
			logger.log(LogStatus.PASS, "Mandatory Validation for Coverage is verified");
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
	
	@Test(priority = 3, enabled = execution,dependsOnMethods="testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase03_checkInvaliDataValidationOnItemDeductibleField() throws Exception {
		try {
			policyItemCoverges.refresh();
			logger = report.startTest("Test- Check Invalid Data validaiton message on Deductible field");
			Wait.waitFor(2);
			policyItemCoverges.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			policyItemCoverges.selectCoverage(1);
			logger.log(LogStatus.INFO, "User enters Coverage Id");
			policyItemCoverges.enterInvalidDataInDeductible();
			logger.log(LogStatus.INFO, "User enters Invalid Data in Deductible");
			policyItem.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Assert.assertEquals(policyItem.validationMessageArea.getText(), "Deductible is invalid.");
			logger.log(LogStatus.PASS, "Validation for Invalid Data in Deductible is verified");
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}
	
	@Test(priority = 4, enabled = execution,dependsOnMethods="testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase04_checkInvalidDataValidationOnLimitField() throws Exception {
		try {
			policyItemCoverges.refresh();
			logger = report.startTest("Test- Check Invalid Data  Message on Limit Per Person field");
			Wait.waitFor(2);
			policyItemCoverges.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(2);
			policyItemCoverges.selectCoverage(1);
			logger.log(LogStatus.INFO, "User enters Coverage Id");
			policyItemCoverges.enterInvalidDataInLimitPerPerson();
			logger.log(LogStatus.INFO, "User enters Invalid Data in Limit Per Person field");
			policyItem.clickSavePolicyItem();
			Assert.assertEquals(policyItem.validationMessageArea.getText(), "Limit Per Person is invalid."
);
			logger.log(LogStatus.PASS, "Invalid data Validation for Limit Per Person field is verified");
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

		@Test(priority = 5, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase05_checkValidationMessageInvalidDataInLimitPerOccurence() throws Exception {
			try {
				policyItemCoverges.refresh();
				logger = report.startTest("Test- Validation message on entering invalid data in Limit Per Occurence field");
				Wait.waitFor(2);
				policyItemCoverges.clickAddPolicyItem();
				Wait.waitFor(2);
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				policyItemCoverges.selectCoverage(1);
				logger.log(LogStatus.INFO, "User enters Coverage Id");
				policyItemCoverges.enterInvalidDataInLimitPerOccurence();
				logger.log(LogStatus.INFO, "User enters Invalid Data in Limit Per Occurence field");
				policyItemCoverges.clickSavePolicyItem();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Wait.waitFor(4);
				Assert.assertEquals(policyItem.validationMessageArea.getText(),
						"Limit Per Occurrence is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in Limit Per Occurence field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

		@Test(priority = 6, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase06_checkValidationMessageInvalidDataInClaimReportingField() throws Exception {
			try {
				policyItemCoverges.refresh();
				logger = report.startTest(
						"Test- Validation message on entering invalid data in Item Year field");
				Wait.waitFor(2);
				policyItemCoverges.clickAddPolicyItem();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				Wait.waitFor(2);
				policyItemCoverges.selectCoverage(1);
				logger.log(LogStatus.INFO, "User enters Coverage Id");
				policyItemCoverges.enterInvalidDataInClaimReportingLevel();
				logger.log(LogStatus.INFO, "User enters Invalid Data in Cliam Reporting Level field");
				policyItemCoverges.clickSavePolicyItem();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Assert.assertEquals(policyItem.validationMessageArea.getText(),
						"Claim Reporting Level is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in Claim reporting Level field is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}
		
		@Test(priority = 7, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase07_checkValidationMessageInvalidDataInPremiumField() throws Exception {
			try {
				policyItemCoverges.refresh();
				logger = report.startTest(
						"Test- Validation message on entering data more than limit  in Deductible field");
				Wait.waitFor(2);
				policyItemCoverges.clickAddPolicyItem();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				Wait.waitFor(2);
				policyItemCoverges.selectCoverage(1);
				logger.log(LogStatus.INFO, "User enters Coverage Id");
				policyItemCoverges.enterInvalidDataIPremium();
				logger.log(LogStatus.INFO, "User enters Invalid Data in Premium field");
				policyItemCoverges.clickSavePolicyItem();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Assert.assertEquals(policyItem.validationMessageArea.getText(),
						"Premium is invalid.");
				logger.log(LogStatus.PASS, "Validation for invalid data in Premium fields is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}

		@Test(priority = 8, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase08_checkValidationMessageMoreThanLimitInDeductible() throws Exception {
			try {
				policyItemCoverges.refresh();
				logger = report.startTest(
						"Test- Validation message on data more than limit in deductible field");
				Wait.waitFor(2);
				policyItemCoverges.clickAddPolicyItem();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				Wait.waitFor(2);
				policyItemCoverges.selectCoverage(1);
				logger.log(LogStatus.INFO, "User enters Coverage Id");
				Wait.waitFor(2);
				policyItemCoverges.enterDataMoreThanLimitInDeductible();
				logger.log(LogStatus.INFO, "User enters  Data More than Limit in Deductible field");
				policyItemCoverges.clickSavePolicyItem();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Assert.assertEquals(policyItem.validationMessageArea.getText(),
						"Deductible/SIR should be between $0.00 and 9999999.99.");
				logger.log(LogStatus.PASS, "Validation for invalid data in Premium fields is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}
			
		@Test(priority = 9, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase09_checkValidationMessageMoreThanLimitInLimitPerPerson() throws Exception {
			try {
				policyItemCoverges.refresh();
				logger = report.startTest(
						"Test- Validation message on entering data more than limit in LimitPerPerson field");
				Wait.waitFor(2);
				policyItemCoverges.clickAddPolicyItem();
				logger.log(LogStatus.INFO, "User clicked on Add Button");
				Wait.waitFor(2);
				policyItemCoverges.selectCoverage(1);
				logger.log(LogStatus.INFO, "User enters Coverage Id");
				Wait.waitFor(2);
				policyItemCoverges.enterDataMoreThanLimitInLimitPerPerson();
				logger.log(LogStatus.INFO, "User enters  Data More than Limit in LimitPerPerson field");
				policyItemCoverges.clickSavePolicyItem();
				logger.log(LogStatus.INFO, "User clicks on Save Button");
				Assert.assertEquals(policyItem.validationMessageArea.getText(),
						"Limit Per Person should be between $0.00 and 9999999.99.");
				logger.log(LogStatus.PASS, "Validation for invalid data in Premium fields is verified");

			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e);
				Assert.fail("Test case failed", e);
			}
		}
		
			@Test(priority = 10, enabled = execution, dependsOnMethods = "testCase01_checkUserGetRedirectedToPolicyItemCoverageScreen")
	public void testCase10_checkCancelButtonClearsTheValue() throws Exception, NumberFormatException {
				try {
					policyItemCoverges.refresh();
					logger = report.startTest("Test- Cancel Button Clears the value in the Category Field");
					Wait.waitFor(2);
					policyItemCoverges.clickAddPolicyItem();
					logger.log(LogStatus.INFO, "User clicked on Add Button");
					Wait.waitFor(2);
					policyItemCoverges.selectCoverage(1);
					logger.log(LogStatus.INFO, "User enters Coverage Id");
					policyItemCoverges.clickCancelPolicyItem();
					logger.log(LogStatus.INFO, "User clicks on Cancel Button");
					Wait.waitFor(4);
					Assert.assertTrue(!policyItemCoverges.coverageCategoryDropDown.isEnabled());
					logger.log(LogStatus.PASS, "Value in Category is now cleared");
					

				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e);
					Assert.fail("Test case failed", e);
				}
			}

			
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		 driver.quit();
	}
}
	

