package testScripts.ALLSTATE.Login;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


import commonPageObjects.Login.LoginPageObjects;
import utilities.Browser;
import utilities.Configuration;
import utilities.ExcelReader;
import utilities.ReportManager;
import utilities.TakeScreenshot;

public class LoginTestScripts {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	WebDriver driver;
	
	public final boolean execution=true;
	
	{

		report=ReportManager.getReporter();
	}
	
	@BeforeClass
	public void beforeClass()
	{		
		driver=Browser.getBrowser("ie");
		driver.manage().window().maximize();
		loginPageObjects=PageFactory.initElements(driver,LoginPageObjects.class);;
		
		
		
	}
	
	@BeforeMethod
	public void beforeMethod()
	{
	
		
		driver.get(Configuration.baseUrl);	
	}
	
	/*
	 * 
	 * This method will verify error message for Invalid Credentials
	 * 
	 */
	@Test(priority=1,enabled=execution)
	public void testCase01_verifyLoginFunctionality() throws InterruptedException, IOException
	{
		
		logger=report.startTest("Test Valid Login in Claimsvision");
		logger.log(LogStatus.INFO,"Browser Started");
		loginPageObjects.enterCustomerCode(ExcelReader.getCell(0, 14, 2));
		logger.log(LogStatus.INFO, "User entered Customer Code");
		loginPageObjects.enterUserName(ExcelReader.getCell(0, 14, 3));
		logger.log(LogStatus.INFO, "User entered User Name");
		loginPageObjects.enterPassWord(ExcelReader.getCell(0, 14, 4));
		logger.log(LogStatus.INFO, "User entered Password");
		loginPageObjects.clickOnLogin();
		logger.log(LogStatus.INFO, "User clicked on Login button");
		Thread.sleep(10000);
		Assert.assertEquals(driver.getTitle(),"User Activity");
		logger.log(LogStatus.PASS, "User successfully Logged into Claimsvision");
		
		
	}
	
	@Test(priority=2,enabled=execution)
	public void testCase02_verifyInvalidLogin() throws InterruptedException, IOException
	{
		
		logger=report.startTest("Test Invalid Login in Claimsvision");
		logger.log(LogStatus.INFO,"Browser Started");
		loginPageObjects.enterCustomerCode("ABCD");
		logger.log(LogStatus.INFO, "User entered invalid Customer Code");
		loginPageObjects.enterUserName("ABCD");
		logger.log(LogStatus.INFO, "User entered invalid User Name");
		loginPageObjects.enterPassWord("ABCD");
		logger.log(LogStatus.INFO, "User entered invalid Password");
		loginPageObjects.clickOnLogin();
		logger.log(LogStatus.INFO, "User clicked on Login button");
		Thread.sleep(10000);
		Assert.assertEquals(loginPageObjects.testErrorMessage(), "Invalid User Name or Password.");
		logger.log(LogStatus.PASS, "System displayed correct error message: " + loginPageObjects.testErrorMessage());
		
	}
	

	/*
	 * 
	 * This method will verify validation message if Customer is not entered
	 * 
	 */
	@Test(priority=3,enabled=execution)
	public void testCase03_verifyValidation_Customer() throws InterruptedException, IOException
	{
		
		logger=report.startTest("Test if User skips to enter Customer");
		logger.log(LogStatus.INFO,"Browser Started");
		loginPageObjects.enterUserName(ExcelReader.getCell(0, 14, 3));
		logger.log(LogStatus.INFO, "User entered User Name");
		loginPageObjects.enterPassWord(ExcelReader.getCell(0, 14, 4));
		logger.log(LogStatus.INFO, "User entered Password");
		loginPageObjects.clickOnLogin();
		logger.log(LogStatus.INFO, "User clicked on Login button");
		Thread.sleep(10000);
		Assert.assertEquals(loginPageObjects.testValidationMessage(), "Customer is required.");
		logger.log(LogStatus.PASS, "System displayed correct error message: " + loginPageObjects.testValidationMessage());
		
		
		
	}
	
	/*
	 * 
	 * This method will verify validation message if User Name is not entered
	 * 
	 */	
	@Test(priority=4,enabled=execution)
	public void testCase04_verifyValidation_UserName() throws InterruptedException, IOException
	{
		
		logger=report.startTest("Test if User skips to enter User Name");
		logger.log(LogStatus.INFO,"Browser Started");
		loginPageObjects.enterCustomerCode(ExcelReader.getCell(0, 14, 2));
		logger.log(LogStatus.INFO, "User entered CustomerCode");
		loginPageObjects.enterPassWord(ExcelReader.getCell(0, 14, 4));
		logger.log(LogStatus.INFO, "User entered Password");
		loginPageObjects.clickOnLogin();
		logger.log(LogStatus.INFO, "User clicked on Login button");
		Thread.sleep(10000);
		Assert.assertEquals(loginPageObjects.testValidationMessage(), "User Name is required.");
		logger.log(LogStatus.PASS, "System displayed correct error message: " + loginPageObjects.testValidationMessage());
		
		
		
	}
	
	
	/*
	 * 
	 * This method will verify validation message if Password is not entered
	 * 
	 */
	@Test(priority=5,enabled=execution)
	public void testCase05_verifyValidation_Password() throws InterruptedException, IOException
	{
		
		logger=report.startTest("Test if User skips to enter Password");
		logger.log(LogStatus.INFO,"Browser Started");
		loginPageObjects.enterCustomerCode(ExcelReader.getCell(0, 14, 2));
		logger.log(LogStatus.INFO, "User entered CustomerCode");
		loginPageObjects.enterUserName(ExcelReader.getCell(0, 14, 3));
		logger.log(LogStatus.INFO, "User entered Username");
		loginPageObjects.clickOnLogin();
		logger.log(LogStatus.INFO, "User clicked on Login button");
		Thread.sleep(2000);
		Assert.assertEquals(loginPageObjects.testValidationMessage(), "Password is required.");
		logger.log(LogStatus.PASS, "System displayed correct error message: " + loginPageObjects.testValidationMessage());
		
		
		
	}
	
	/*
	 * 
	 * This method will verify title of page after Successful login
	 * 
	 */
	

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException
	{
	if(result.getStatus()==ITestResult.FAILURE)
	{
	TakeScreenshot.takeFailScreen(result.getName(), driver);
	String image= logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
	logger.log(LogStatus.FAIL, "Title verification", image);
	
	report.endTest(logger);
	

	 
	}
	}
	
	@AfterClass
	public void afterClass()
	{
		
		
		report.flush();
		driver.quit();
	}
	
	
	
	
	@AfterSuite()
	public void sendReportViaMail()
	{
	
//	SendMail.sendReportViaMail();
	}
	
}
