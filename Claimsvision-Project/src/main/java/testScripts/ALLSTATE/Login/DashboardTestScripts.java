package testScripts.ALLSTATE.Login;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;

public class DashboardTestScripts {

	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	Dashboard dashboardPageObjects;
	LoginTestScripts loginPageTest;
	WebDriver driver;
	
	{

		report=ReportManager.getReporter();
	}
	
	@BeforeClass
	public void beforeClass() throws Exception
	{		
		driver=Browser.getBrowser("ie");
		driver.manage().window().maximize();
		loginPageObjects=new LoginPageObjects(driver);
		dashboardPageObjects=new Dashboard(driver);
		loginPageObjects=PageFactory.initElements(driver,LoginPageObjects.class);;
		dashboardPageObjects=PageFactory.initElements(driver,Dashboard.class);;
		
					
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception, Exception
	{
		driver.get(Configuration.baseUrl);
		loginPageObjects.Login(14);
		
		
	}
	
	@Test(priority=1)
	public void testNavigationtoPolicy() throws Exception{
		Thread.sleep(10000);
		logger=report.startTest("Test Navigation - Policy Setup");
		logger.log(LogStatus.INFO,"User should be on Dashboard screen");
		logger.log(LogStatus.INFO,"User navigating to Admin > Policy Setup > Non Comp Policy > Policy");
		dashboardPageObjects.navigateToPolicySetup();
		logger.log(LogStatus.PASS,"User should navigate to Non Comp Policy Setup screen");
	}
	
	@Test(priority=2)
	public void testNavigationtoPolicyNumberAssignment() throws Exception{
		Thread.sleep(10000);
		logger=report.startTest("Test Naviagtion - Policy Number Assignment");
		logger.log(LogStatus.INFO, "User should be on Dashboard screen");
		logger.log(LogStatus.INFO,"User navigating to Admin > Customization > Policy Number Assignment");
		dashboardPageObjects.navigateToPolicyNumberAssignment();
		logger.log(LogStatus.PASS, "User should navigate to Policy Number Assignment screen");
	}
	
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException
	{
	if(result.getStatus()==ITestResult.FAILURE)
	{
	TakeScreenshot.takeFailScreen(result.getName(), driver);
	String image= logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
	logger.log(LogStatus.FAIL, "Title verification", image);
	
	report.endTest(logger);
	

	 
	}
	}
	
	@AfterClass
	public void afterClass()
	{
		
		
		report.flush();
		driver.quit();
	}
	
}
