package testScripts.ALLSTATE.IRF;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.Login.SearchClaim;
import commonPageObjects.NonComp.Claims.Claimants;
import commonPageObjects.NonComp.Claims.Claims;
import commonPageObjects.NonComp.Claims.SubClaims;
import commonPageObjects.NonComp.IRF.IRF_ENS;
import commonPageObjects.NonComp.Policy.PolicyItem;
import commonPageObjects.NonComp.Policy.PolicyItemCoverages;
import commonPageObjects.NonComp.Policy.PolicyNumberAssignment;
import commonPageObjects.NonComp.Policy.PolicySetUp;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class IRF_ENSTestScripts {

	private static final boolean execution = true;
	ExtentReports report;
	ExtentTest logger;
	LoginPageObjects loginPageObjects;
	Dashboard dashboardPageObjects;
	IRF_ENS irfPageObjects;
	WebDriver driver;
	BaseClass bc;
	SearchClaim searchClaimPageObjects;
	PolicySetUp policySetupPageObject;
	PolicyNumberAssignment policyNumberAssign;
	PolicyItem policyItem;
	PolicyItemCoverages policyItemCoverges;
	Claims claimScreenPageObjects;
	Claimants claimantScreenPageObjects;
	SubClaims subClaimPageObjects;
	String itemName = null;
	String coverageName = null;
	String claimNumber = null;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		policyNumberAssign = PageFactory.initElements(driver, PolicyNumberAssignment.class);
		irfPageObjects = PageFactory.initElements(driver, IRF_ENS.class);
		policySetupPageObject = PageFactory.initElements(driver, PolicySetUp.class);
		policyItem = PageFactory.initElements(driver, PolicyItem.class);
		policyItemCoverges = PageFactory.initElements(driver, PolicyItemCoverages.class);
		claimantScreenPageObjects = PageFactory.initElements(driver, Claimants.class);
		subClaimPageObjects = PageFactory.initElements(driver, SubClaims.class);
		claimScreenPageObjects = PageFactory.initElements(driver, Claims.class);
		dashboardPageObjects = loginPageObjects.Login(14);

	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@Test(priority = 1, enabled = execution)
	public void testCase01_CreateIRFClaim() throws Exception {
		try {
			Wait.waitFor(3);
			logger = report.startTest("Test - Non Comp Claim Creation");
			dashboardPageObjects.navigateToPolicyNumberAssignment();
			Wait.waitFor(5);
			logger.log(LogStatus.INFO, "Test Naviagtion - Policy Number Assignment");
			policyNumberAssign.nonComp_PolicyNumber_NotConfiguredAllFields();
			logger.log(LogStatus.INFO, "User configuring the Policy Number");
			policyNumberAssign.nonCompSave.click();
			logger.log(LogStatus.INFO, "User clicked on Save button to save configuration");
			Browser.refresh(driver);
			dashboardPageObjects.navigateToPolicySetup();
			logger.log(LogStatus.INFO, "Test Navigation - Policy Setup");
			Wait.waitFor(2);
			policySetupPageObject.clickOnAddButton();
			logger.log(LogStatus.INFO, "User clicked on Add button");
			Wait.waitFor(3);
			policySetupPageObject.selectCompany();
			logger.log(LogStatus.INFO, "User selected 'Company'");
			Wait.waitFor(3);
			policySetupPageObject.selectCarrierValue();
			logger.log(LogStatus.INFO, "User selected 'Carrier'");
			Wait.waitFor(3);
			policySetupPageObject.selectClientValue();
			logger.log(LogStatus.INFO, "User selected 'Client'");
			Wait.waitFor(3);
			policySetupPageObject.selectPolicyType();
			logger.log(LogStatus.INFO, "User selected 'Policy type'");
			Wait.waitFor(3);
			policySetupPageObject.enterEffectiveDateAndExpirationDate();
			logger.log(LogStatus.INFO, "User entered 'Effective Date' and 'Expiration Date'");
			Wait.waitFor(3);
			// policySetupPageObject.checkInsuredIsClient();
			// logger.log(LogStatus.INFO, "User selected 'Insured is Client'
			// checkbox");
			// Wait.waitFor(5);
			policySetupPageObject.enterShortPolicyNumber();
			logger.log(LogStatus.INFO, "User entered 'Short Policy number'");
			Wait.waitFor(3);
			ArrayList<String> list = new ArrayList<String>();
			list.add(policySetupPageObject.fetchEffectiveDate());

			list.add(policySetupPageObject.fetchExpirationDate());

			list.add(policySetupPageObject.fetchCarrier());

			list.add(policySetupPageObject.fetchClient());

			list.add(policySetupPageObject.fetchPolicyType());

			list.add(policySetupPageObject.fetchShortPolicyNumber());

			String carrierCode = policySetupPageObject.getCarrierCode(list.get(2));
			String policyCode = policySetupPageObject.getPolicyCode(list.get(4));
			policySetupPageObject.clickOnSaveButton();
			logger.log(LogStatus.INFO, "User clicked on Save button");
			Wait.waitFor(5);
			String policyNum = policySetupPageObject.getExpectedPolicyNumber();
			Wait.waitFor(6);
			policySetupPageObject.goToLastSearchResultScreen();
			Wait.waitFor(10);
			Assert.assertEquals(policySetupPageObject.fetchLatestPolicyNumber(), policyNum);
			logger.log(LogStatus.INFO, "User clicked on new policy number");
			Wait.waitFor(20);
			policySetupPageObject.getWindowForPolicySetUp();
			policyItem.clickPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Policy Item Icon");
			Wait.waitFor(3);
			policyItem.switchToPolicyItemScreen();
			Wait.waitFor(2);
			policyItem.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicked on Add Button");
			Wait.waitFor(5);
			itemName = policyItem.enterItemName();
			logger.log(LogStatus.INFO, "User enters Item Name");
			Wait.waitFor(5);
			policyItem.enterItemId();
			logger.log(LogStatus.INFO, "User selected Item ID");
			policyItem.selectItemCategory();
			logger.log(LogStatus.INFO, "User selected Item Category");
			policyItem.clickSavePolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			Wait.waitFor(4);
			policyItem.clickOnModifyButton();
			logger.log(LogStatus.INFO, "User clicks on Modify Button");
			Wait.waitFor(3);
			policyItem.getWindowHandleForItem();
			policyItemCoverges.clickOnCoverageIcon();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User clicks on Coverage Icon");
			policyItemCoverges.switchToCoverageScreen();
			Wait.waitFor(2);
			policyItemCoverges.clickAddPolicyItem();
			logger.log(LogStatus.INFO, "User clicks on Add Button");
			Wait.waitFor(3);
			coverageName = policyItemCoverges.selectCoverage(1);
			logger.log(LogStatus.INFO, "User enters Coverage Id");
			policyItemCoverges.enterDataInDeductible();
			logger.log(LogStatus.INFO, "User enters  Data in Deductible");
			Wait.waitFor(3);
			policyItemCoverges.enterDataInLimitPerPerson();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User enters  Data in Limit Per Person field");
			policyItemCoverges.enterDataInLimitPerOccurence();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User enters  Data in Limit Per Occurence field");
			policyItemCoverges.enterDataInClaimReportingLevel();
			logger.log(LogStatus.INFO, "User enters Invalid Data in Cliam Reporting Level field");
			Wait.waitFor(3);
			policyItemCoverges.enterDataInPremium();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User enters  Data in Premium field");
			policyItemCoverges.clickSavePolicyItem();
			Wait.waitFor(3);
			logger.log(LogStatus.INFO, "User clicks on Save Button");
			policyItemCoverges.clickCloseButtonInCoveregeScreen();
			// bc.switchToNextWindow("Policy Set-up");
			policyItem.getBackToPolicyItemScreen();
			policyItem.clickCloseButtonInItemScreen();
			policySetupPageObject.getBackToPolicySetUp();
			Wait.waitFor(5);
			dashboardPageObjects.getWindowForDashboard();
			dashboardPageObjects.navigateToIRF();
			logger.log(LogStatus.INFO, "User navigated to Irf Screen");
			irfPageObjects.clickPolicyIcon();
			logger.log(LogStatus.INFO, "User clicked on Policy Icon");
			Wait.waitFor(2);
			irfPageObjects.selectPolicy(policyNum);
			logger.log(LogStatus.INFO, "User selects the created policy");

			logger.log(LogStatus.INFO, "System fetching 'Effective Date'");

			logger.log(LogStatus.INFO, "System fetching 'Expiration Date'");

			logger.log(LogStatus.INFO, "System fetching 'Carrier'");

			logger.log(LogStatus.INFO, "System fetching 'Client'");

			logger.log(LogStatus.INFO, "System fetching 'Policy Type'");

			String CustomClaimNumber = irfPageObjects.enterCustomClaimNumber();
			logger.log(LogStatus.INFO, "User enters on Custom Claim Number");
			irfPageObjects.enterIncidentDate(list.get(0));
			logger.log(LogStatus.INFO, "User enters the Incident Date");
			irfPageObjects.clickSubmitButton();
			logger.log(LogStatus.INFO, "User clicks on Submit Button");
			irfPageObjects.clickContinueButton();
			irfPageObjects.closeIrfScreen();
			dashboardPageObjects.getBackToParentWindow();
			searchClaimPageObjects = dashboardPageObjects.navigateToClaimSearch();
			logger.log(LogStatus.INFO, "User navigated to Claim Search Screen");
			claimNumber = carrierCode + policyCode + list.get(5) + CustomClaimNumber;
			searchClaimPageObjects.enterClaimNumber(claimNumber);
			logger.log(LogStatus.INFO, "User enters the Claim Number");
			searchClaimPageObjects.selectSearch();
			logger.log(LogStatus.INFO, "User clicks on Search Button");
			Wait.waitFor(5);
			searchClaimPageObjects.selectClaimNumber();
			logger.log(LogStatus.INFO, "User selects the claim number");
			Wait.waitFor(5);
			Assert.assertEquals(driver.getTitle(), "Claim");
			logger.log(LogStatus.PASS, "User redirected to Claim Screen");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	@Test(priority = 2, enabled = execution, dependsOnMethods = "testCase01_CreateIRFClaim")
	public void testCase02_openTheClaim() throws Exception {
		logger = report.startTest("Test - Check User is able to open the claim");
		Wait.waitFor(3);
		claimScreenPageObjects.selectClaimType();
		Wait.waitFor(2);
		logger.log(LogStatus.INFO, "User change the claim Type to Active");
		claimScreenPageObjects.selectClaimStatus();
		logger.log(LogStatus.INFO, "User change the claim status to Open");
		Wait.waitFor(2);
		claimScreenPageObjects.selectAdjuster();
		logger.log(LogStatus.INFO, "User selects the Adjuster");
		claimScreenPageObjects.selectSupervisor();
		logger.log(LogStatus.INFO, "User selects the Supervisor");
		claimScreenPageObjects.enterPolicyYear();
		logger.log(LogStatus.INFO, "User enters the Policy Year");
		claimScreenPageObjects.clickOnSave();
		logger.log(LogStatus.INFO, "User clicks on Save Button");
		Wait.waitFor(7);
		Assert.assertEquals(claimScreenPageObjects.fetchValidationMessage(), "Saved successfully.");
		logger.log(LogStatus.PASS, "Verified the Validation message in Claim Screen after saving");
		Assert.assertEquals(claimScreenPageObjects.fecthClaimType(), "Active");
		logger.log(LogStatus.PASS, "Verified the Claim type value ");
		Assert.assertEquals(claimScreenPageObjects.fetchClaimStatus(), "Open");
		logger.log(LogStatus.PASS, "Verified the Claim status value ");

	}

	@Test(priority = 3, enabled = execution, dependsOnMethods = "testCase02_openTheClaim")
	public void testCase03_createTheClaimant() throws Exception {
		logger = report.startTest("Test -Check User is able to create the Claimant");
		claimantScreenPageObjects.clickClaimantTab();
		logger.log(LogStatus.INFO, "User is directed to claimant Screen");
		Wait.waitFor(8);
		claimantScreenPageObjects.selectClaimantType();
		Wait.waitFor(2);
		logger.log(LogStatus.INFO, "User selects the claimant Type as Claimant");
		claimantScreenPageObjects.clickAddClaimantButton();
		logger.log(LogStatus.INFO, "User clicks on Add Claimant Button");
		Wait.waitFor(2);
		claimantScreenPageObjects.addClaimantName();
		logger.log(LogStatus.INFO, "User enters the claimant Name");
		Wait.waitFor(2);
		claimantScreenPageObjects.addClaimantAddress();
		Wait.waitFor(2);
		logger.log(LogStatus.INFO, "User enters  the claimant Address");
		claimantScreenPageObjects.addClaimantZip();
		Wait.waitFor(5);
		System.out.println(claimantScreenPageObjects.getIncidentDateFromHeader());
		logger.log(LogStatus.INFO, "User enters  the claimant Zip");
		claimantScreenPageObjects.addReportDateToInsured();
		Wait.waitFor(2);
		String expectedReportDate=claimantScreenPageObjects.fetchReportDateToInsured();
		logger.log(LogStatus.INFO, "User enters  the Report Date To Insured Zip");
		claimantScreenPageObjects.clickOnSaveButton();
		logger.log(LogStatus.INFO, "User clicks on Save Button");
		Wait.waitFor(10);
		Assert.assertEquals(claimantScreenPageObjects.fetchValidationMessage(), "Saved successfully.");
		logger.log(LogStatus.PASS, "Verified the Validation message after saving in Claimant Screen ");
		Assert.assertEquals(claimantScreenPageObjects.fetchClaimantType(), "Claimant");
		logger.log(LogStatus.PASS, "Verified the Claimant type value ");
		Assert.assertEquals(claimantScreenPageObjects.fetchReportDateToInsured(), expectedReportDate);
		logger.log(LogStatus.PASS, "Verified the ReportDateToInsured ");
		Assert.assertEquals(claimantScreenPageObjects.fetchClaimantName(), "Automation o Claimant");
		logger.log(LogStatus.PASS, "Verified the Claimant Name");
		Assert.assertEquals(claimantScreenPageObjects.fetchAddress(), "Park Street");
		logger.log(LogStatus.PASS, "Verified the Claimant Address");
		Assert.assertEquals(claimantScreenPageObjects.fetchCity(), "YOUNG AMERICA");
		logger.log(LogStatus.PASS, "Verified the Claimant City");
		Assert.assertEquals(claimantScreenPageObjects.fetchState(), "MN");
		logger.log(LogStatus.PASS, "Verified the Claimant State");
		Assert.assertEquals(claimantScreenPageObjects.fetchZip(), "55555");
		logger.log(LogStatus.PASS, "Verified the Claimant Zip");
		Assert.assertEquals(claimantScreenPageObjects.fetchCounty(), "CARVER");
		logger.log(LogStatus.PASS, "Verified the Claimant County");
//		Assert.assertEquals(claimantScreenPageObjects.fetchValidationMessage(), "Saved successfully.");
//		logger.log(LogStatus.PASS, "Verified the Validation message after saving in Claimant Screen ");

	}

	@Test(priority = 4, enabled = execution, dependsOnMethods = "testCase03_createTheClaimant")
	public void testCase04_createTheSubClaim() throws Exception {
		logger = report.startTest("Test -Check User is able to create the Sub Claim");
		subClaimPageObjects.clickSubClaimTab();
		logger.log(LogStatus.INFO, "User is directed to Sub Claim Screen");
		Wait.waitFor(8);
		subClaimPageObjects.clicksOnAddSubClaimButton();
		Wait.waitFor(4);
		subClaimPageObjects.addInsurableItem();
		logger.log(LogStatus.INFO, "User has added Insurable Item");
		Wait.waitFor(2);
		subClaimPageObjects.addCoverageItem();
		logger.log(LogStatus.INFO, "User has added Covarage ");
		subClaimPageObjects.clickSaveButton();
		logger.log(LogStatus.INFO, "User clicks on Save Button");
		Wait.waitFor(5);
		Assert.assertEquals(subClaimPageObjects.fetchValidationMessage(), "Saved successfully.");
		logger.log(LogStatus.PASS, "Verified the Validation message after saving in Sub Claim Screen ");
		Assert.assertEquals(subClaimPageObjects.fetchInsurableItem(), itemName);
		logger.log(LogStatus.PASS, "Verified the Item Value in Sub Claim Screen ");
		Assert.assertEquals(subClaimPageObjects.fetchCoverageItem(), coverageName);
		logger.log(LogStatus.PASS, "Verified the Coverage value in Sub Claim Screen ");
		Assert.assertEquals(subClaimPageObjects.getSubClaimNumber(), claimNumber + "-001");
		logger.log(LogStatus.PASS, "Verified the Sub Claim  Number in Sub Claim Screen ");

	}

	@Test(priority = 5, enabled = execution, dependsOnMethods = "testCase04_createTheSubClaim")
	public void testCase05_closeTheClaim() throws Exception {
		logger = report.startTest("Test -Check User is able to close the Claim");
		claimScreenPageObjects.clickClaimTab();
		claimScreenPageObjects.clickCloseClaimIcon();
		logger.log(LogStatus.INFO, "User clicks on close claim Icon in Claim Screen");
		Wait.waitFor(5);
		Assert.assertEquals(claimScreenPageObjects.fetchClaimStatus(), "Closed");
		logger.log(LogStatus.PASS, "Verified that  Claim is Closed");

	}

	@Test(priority = 6, enabled = execution, dependsOnMethods = "testCase05_closeTheClaim")
	public void testCase06_reOpenTheClaim() throws Exception {
		logger = report.startTest("Test -Check User is able to Open the Claim");
		claimScreenPageObjects.clickOpenClaimIcon();
		logger.log(LogStatus.INFO, "User clicks on Open claim Icon in Claim Screen");
		Wait.waitFor(5);
		Assert.assertEquals(claimScreenPageObjects.fetchClaimStatus(), "ReOpened");
		logger.log(LogStatus.PASS, "Verified that  Claim is ReOpened");

	}

	@Test(priority = 8, enabled = execution, dependsOnMethods = "testCase06_reOpenTheClaim")
	public void testCase07_closeTheSubClaim() throws Exception {
		logger = report.startTest("Test -Check User is able to close the Sub Claim");
		subClaimPageObjects.clickSubClaimTab();
		subClaimPageObjects.clickCloseClaimIcon();
		logger.log(LogStatus.INFO, "User clicks on close claim Icon");
		Wait.waitFor(5);
		Assert.assertEquals(subClaimPageObjects.fetchSubClaimStatus(), "Closed");
		logger.log(LogStatus.PASS, "Verified that Sub Claim is Closed");

	}

	@Test(priority = 7, enabled = execution, dependsOnMethods = "testCase06_reOpenTheClaim")
	public void testCase08_reopenTheSubClaim() throws Exception {
		logger = report.startTest("Test -Check User is able to reopen the Sub Claim");
		subClaimPageObjects.clickSubClaimTab();
		subClaimPageObjects.clickOpenClaimIcon();
		logger.log(LogStatus.INFO, "User clicks on open claim Icon");
		Wait.waitFor(5);
		Assert.assertEquals(subClaimPageObjects.fetchSubClaimStatus(), "ReOpened");
		logger.log(LogStatus.PASS, "Verified that Sub Claim is ReOpened");

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		//driver.quit();
	}

}
