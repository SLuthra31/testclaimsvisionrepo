package testScripts.NYLAW.IRF;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonPageObjects.Login.Dashboard;
import commonPageObjects.Login.LoginPageObjects;
import commonPageObjects.WorkersComp.iRF.IRF;
import utilities.BaseClass;
import utilities.Browser;
import utilities.Configuration;
import utilities.ExcelReader;
import utilities.ReportManager;
import utilities.TakeScreenshot;
import utilities.Wait;

public class IRF_WCTestScripts {

	
	private static final boolean execution = true;
	ExtentReports report;
	ExtentTest logger;
	WebDriver driver;
	BaseClass bc;
	LoginPageObjects loginPageObjects;
	IRF irfPageObjects;
	Dashboard dashboard_PageObjects;

	{

		report = ReportManager.getReporter();
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.getBrowser("ie");
		bc = new BaseClass(driver);
		driver.get(Configuration.baseUrl);
		Wait.implicitWait(driver, 20);
		loginPageObjects = PageFactory.initElements(driver, LoginPageObjects.class);
		dashboard_PageObjects = loginPageObjects.Login(10);
		//dashboard_PageObjects.selectAgree();
		irfPageObjects = dashboard_PageObjects.navigateToIRFWC();
		ExcelReader.clearCell();

	}

	@Test
	public void enterIRF() {
		try {
			logger = report.startTest("Test-IRF is getting created successfully");
			final String incidentDate = irfPageObjects.enterIncidentDate();
			logger.log(LogStatus.INFO, "User enter Incident Date");
			irfPageObjects.enterTimeOfLoss();
			logger.log(LogStatus.INFO, "User enter Time od Loss");
			irfPageObjects.enterClientForNYLAW();
			logger.log(LogStatus.INFO, "User enters client");
			Wait.waitFor(2);
			irfPageObjects.enterLevelAssignedForNYLAW();
			logger.log(LogStatus.INFO, "User enters level assigned to");
			irfPageObjects.selectPolicyTypeForNYLAW();
			logger.log(LogStatus.INFO, "User selects policy Type");
			irfPageObjects.enterClaimantID();
			logger.log(LogStatus.INFO, "User enters claimant ID");
			irfPageObjects.enterFirstName();
			logger.log(LogStatus.INFO, "User enters first name");
			irfPageObjects.enterMiddleName();
			logger.log(LogStatus.INFO, "User enters middle name");
			irfPageObjects.enterLastName();
			logger.log(LogStatus.INFO, "User enters last name");
			irfPageObjects.enterSuffix();
			logger.log(LogStatus.INFO, "User enters suffix ");
			irfPageObjects.enterIncidentDescription();
			logger.log(LogStatus.INFO, "User enters Incident Description");
			irfPageObjects.selectNext();
			logger.log(LogStatus.INFO, "User selects Next Button");
			irfPageObjects.clickClaimantInformation();
			logger.log(LogStatus.INFO, "User selects Claimant Information ");
			irfPageObjects.selectGender();
			logger.log(LogStatus.INFO, "User selects Gender ");
			String dob=irfPageObjects.enterDateOfBirth(incidentDate);
			logger.log(LogStatus.INFO, "User enter Date of Birth ");
			irfPageObjects.enterAddress1();
			logger.log(LogStatus.INFO, "User enter claimant address 1");
			irfPageObjects.enterAddress2();
			logger.log(LogStatus.INFO, "User enter claimant address 2");
			irfPageObjects.enterPostal();
			logger.log(LogStatus.INFO, "User enter claimant Postal");
			irfPageObjects.enterCity();
			logger.log(LogStatus.INFO, "User enter claimant City");
			irfPageObjects.enterCounty();
			logger.log(LogStatus.INFO, "User enter claimant County");
			irfPageObjects.enterState();
			logger.log(LogStatus.INFO, "User enter claimant State");
			irfPageObjects.enterHomePhone();
			logger.log(LogStatus.INFO, "User enter claimant HomePhone");
			irfPageObjects.enterCellPhone();
			logger.log(LogStatus.INFO, "User enter claimant Cell Phone");
			irfPageObjects.enterEmailAddress();
			logger.log(LogStatus.INFO, "User enter claimant Email Address");
			irfPageObjects.enterPrimaryLanguage();
			logger.log(LogStatus.INFO, "User enter claimant Primary Language");
			irfPageObjects.enterMaritalStatus();
			logger.log(LogStatus.INFO, "User enter claimant Marital Address");
			irfPageObjects.enterNoOfDependents();
			logger.log(LogStatus.INFO, "User enter No of dependents");
			irfPageObjects.enterEmployeeStatus();
			logger.log(LogStatus.INFO, "User enter Employee Status");
			irfPageObjects.enterHireDate(dob);
			logger.log(LogStatus.INFO, "User enter Hire Date");
			irfPageObjects.enterHireState();
			logger.log(LogStatus.INFO, "User enter Hire State");
			irfPageObjects.enterJurisdictionSate();
			logger.log(LogStatus.INFO, "User enter Jurisdiction atate");
			irfPageObjects.enterOccupationCode();
			logger.log(LogStatus.INFO, "User enter Occupation Code");
			irfPageObjects.enterNAICS();
			logger.log(LogStatus.INFO, "User enter NAICS");
			irfPageObjects.enterWageRate();
			logger.log(LogStatus.INFO, "User enter Wage Rate");
			irfPageObjects.enterPer();
			logger.log(LogStatus.INFO, "User enter Per");
			irfPageObjects.enterNoOfDaysWeek();
			logger.log(LogStatus.INFO, "User enter claimant # Days/Week");
			irfPageObjects.selectFullPayOfIncident();
			logger.log(LogStatus.INFO, "User select Full Pay For Day Of Incident");
			irfPageObjects.selectDidSalaryContinue();
			logger.log(LogStatus.INFO, "User select Did Salary Continue");
			irfPageObjects.clickIncidentDetail();
			logger.log(LogStatus.INFO, "User click Incident Detail icon");
			irfPageObjects.enterDateOfKnowledge(incidentDate);
			logger.log(LogStatus.INFO, "User enter Date of Knowledge");
			irfPageObjects.enterDateReportedToAdmin(incidentDate);
			logger.log(LogStatus.INFO, "User enter Date Reported to Claims");
			irfPageObjects.enterIncidentDateReportedToFirstName();
			logger.log(LogStatus.INFO, "User enter Incident Reported to First Name");
			irfPageObjects.enterIncidentDateReportedToMiddleName();
			logger.log(LogStatus.INFO, "User enter Incident Reported to Middle Name");
			irfPageObjects.enterIncidentDateReportedToLastName();
			logger.log(LogStatus.INFO, "User enter Incident Reported to Last Name");
			irfPageObjects.enterPhoneNumberInIncident();
			logger.log(LogStatus.INFO, "User enter Incident Phone number");
			irfPageObjects.enterJobTitle();
			logger.log(LogStatus.INFO, "User enter Incident Job Title");
			irfPageObjects.enterIncidentEmailAddress();
			logger.log(LogStatus.INFO, "User enter Incident Email Address");
			irfPageObjects.enterFatalityRelatedToIncident();
			logger.log(LogStatus.INFO, "User selects value from fatality related to incident drop down");
			Wait.waitFor(3);
			irfPageObjects.enterFatalityDate(incidentDate);
			logger.log(LogStatus.INFO, "User enter Fatality date");
			Wait.waitFor(3);
			irfPageObjects.selectIsPremisesCheckbox();
			logger.log(LogStatus.INFO, "User select Incident Occured on Employer's Premises as No");
			Wait.waitFor(2);
			irfPageObjects.enterAddress1OfEmpoyerPremises();
			logger.log(LogStatus.INFO, "User enters Address 1 of incident Occured");
			irfPageObjects.enterAddress2OfEmpoyerPremises();
			logger.log(LogStatus.INFO, "User enter Address 2 of incident Occured");
			irfPageObjects.enterPostalEmpoyerPremises();
			logger.log(LogStatus.INFO, "User enter Postal of incident Occured");
			irfPageObjects.enterCityEmpoyerPremises();
			logger.log(LogStatus.INFO, "User enter City of incident Occured");
			irfPageObjects.enterStateEmpoyerPremises();
			logger.log(LogStatus.INFO, "User enter State of incident Occured");
			Wait.waitFor(3);
			irfPageObjects.enterAccidentSummary();
			logger.log(LogStatus.INFO, "User enter Accident Summary");
			irfPageObjects.enterCauseOfIncidentMajor();
			logger.log(LogStatus.INFO, "User enter Cause of Incident Major");
			Wait.waitFor(2);
			irfPageObjects.enterCauseOfIncidentMinor();
			logger.log(LogStatus.INFO, "User enter Cause of Incident Minor");
			Wait.waitFor(2);
			irfPageObjects.enterNatureOfIncidentMajor();
			logger.log(LogStatus.INFO, "User enter Nature of Incident Major");
			Wait.waitFor(2);
			irfPageObjects.enterNatureOfIncidentMinor();
			logger.log(LogStatus.INFO, "User enter Nature of Incident Minor");
			Wait.waitFor(2);
			irfPageObjects.enterPartOfIncidentMajor();
			logger.log(LogStatus.INFO, "User enter Part of Incident Major");
			Wait.waitFor(2);
			irfPageObjects.enterPartOfIncidentMinor();
			logger.log(LogStatus.INFO, "User enter Part of Incident Minor");
			Wait.waitFor(2);
			irfPageObjects.enterTypeOfIncidentCode();
			logger.log(LogStatus.INFO, "User enter Type of Incident Code");
			Wait.waitFor(2);
			irfPageObjects.selectIncidentRelated();
			logger.log(LogStatus.INFO, "User select Incident Related");
			irfPageObjects.selectLostTimeAnticipated();
			logger.log(LogStatus.INFO, "User select Lost Time Anticipated");
			final String dateDisability=irfPageObjects.enterDateDisabilityBegin(incidentDate);
			logger.log(LogStatus.INFO, "User enters Date Disability Begin Date");
			irfPageObjects.enterReturnToWork(dateDisability);
			logger.log(LogStatus.INFO, "User enters Return To Work date");
			irfPageObjects.enterLastWorkDate(incidentDate);
			logger.log(LogStatus.INFO, "User enters Last Work Work date");
			irfPageObjects.enterTimeBeganWork();
			logger.log(LogStatus.INFO, "User enter Time began Work");
			irfPageObjects.selecWasThereAWitness();
			logger.log(LogStatus.INFO, "User select Was there a Witness");
			irfPageObjects.enterWitnessFirstName();
			logger.log(LogStatus.INFO, "User select  Witness First Name");
			irfPageObjects.enterWitnessMiddleName();
			logger.log(LogStatus.INFO, "User select Witness Middle Name");
			irfPageObjects.enterWitnessLastName();
			logger.log(LogStatus.INFO, "User select Witness Last Name");
			irfPageObjects.enterWitnessPhoneNumber();
			logger.log(LogStatus.INFO, "User select Witness Phone number");
			irfPageObjects.enterFirstExposureDate(incidentDate);
			logger.log(LogStatus.INFO, "User enter First Exposure Date");
			irfPageObjects.enterLastExposureDate(incidentDate);
			logger.log(LogStatus.INFO, "User enter Last Exposure Date");
			irfPageObjects.enterStateClaimNumber();
			logger.log(LogStatus.INFO, "User enter state claim number");
			irfPageObjects.enterBodyPerCent();
			logger.log(LogStatus.INFO, "User enter Body Part Percent");
			irfPageObjects.selectContinousTrauma();
			logger.log(LogStatus.INFO, "User select continous trauma");
			irfPageObjects.clickTreatmentDetail();
			irfPageObjects.enterTreatmentDetail();
			logger.log(LogStatus.INFO, "User select Treatment Detail");
			irfPageObjects.enterTreatmentType();
			logger.log(LogStatus.INFO, "User select Treatment Type");
			irfPageObjects.clickSpecialAnalysis();
			
			irfPageObjects.selectcivilDefense();
			logger.log(LogStatus.INFO, "User select civil Defense");
			irfPageObjects.selectclassified();
			logger.log(LogStatus.INFO, "User select classified");
			irfPageObjects.selectclosedSymptomatic();
			logger.log(LogStatus.INFO, "User select closed Symptomatic");
			irfPageObjects.selecttext15_8();
			logger.log(LogStatus.INFO, "User select 15-8");
			irfPageObjects.selectfinalAdjustment();
			logger.log(LogStatus.INFO, "User select final Adjustment");
			irfPageObjects.selectlumpsum();
			logger.log(LogStatus.INFO, "User select lumpsum");
			irfPageObjects.selectmaritime();
			logger.log(LogStatus.INFO, "User select maritime");
			
			irfPageObjects.selectrehab();
			logger.log(LogStatus.INFO, "User select Rehab");
			irfPageObjects.selectthridParty();
			logger.log(LogStatus.INFO, "User select 3rd Party");
			irfPageObjects.selectthirdPartyLienCollect();
			logger.log(LogStatus.INFO, "User select 3rd Party Lien Collect");
			irfPageObjects.selectthirdPartyLienEstab();
			logger.log(LogStatus.INFO, "User select 3rd Party Lien Estab");
			irfPageObjects.selectthirdPartyReviewed();
			logger.log(LogStatus.INFO, "User select 3rd Party Reviewed");
			irfPageObjects.selecttext5105Est();
			logger.log(LogStatus.INFO, "User select 5105 Est");
			irfPageObjects.selecttext5105DoesNotApply();
			logger.log(LogStatus.INFO, "User select 5105 does not apply");
			
			irfPageObjects.selecttext5105Reviewed();
			logger.log(LogStatus.INFO, "User select 5105 Reviewed");
		/*	irfPageObjects.selecttext25A();
			logger.log(LogStatus.INFO, "User select 25-A");*/
			irfPageObjects.selectsubrogation();
			logger.log(LogStatus.INFO, "User select Subrogation");
			irfPageObjects.selectmedicareMedication();
			logger.log(LogStatus.INFO, "User select Medicare Medication");
			irfPageObjects.entertimeInAssignment();
			logger.log(LogStatus.INFO, "User select Time in Assignment");
			irfPageObjects.selecttimeInAssignmentUnknown();	
			logger.log(LogStatus.INFO, "User select Time in Assignment Unknown");
			
			
			
			irfPageObjects.selectSubmit();
			logger.log(LogStatus.INFO, "User select Submit Button");
			
			Wait.waitFor(5);
			String claimNumberValue = irfPageObjects.getClaimNumberForNYLAW();
			System.out.println("Test pass: "+driver.getTitle());
			System.out.println("Claim Number:" + claimNumberValue);
			
			Assert.assertEquals(driver.getTitle(), "Submitted");
			logger.log(LogStatus.PASS, "IRF for Worker's comp claim made successfully");
			
		} catch (final Exception e) {
			logger.log(LogStatus.FAIL, e);
			Assert.fail("Test case failed", e);
		}
	}

	/*@Test
	public void testClaimantValues(){
		
	}*/
	
	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			TakeScreenshot.takeFailScreen(result.getName(), driver);
			final String image = logger.addScreenCapture(Configuration.failSceenLocation + result.getName() + ".png");
			logger.log(LogStatus.FAIL, "Title verification", image);

			report.endTest(logger);

		}
	}

	@AfterClass
	public void afterClass() {

		report.flush();
		driver.quit();
	}


}
